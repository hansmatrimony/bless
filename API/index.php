<?php
	if(isset($_GET["p"]))
	{
		if($_GET["p"]=="uploadVideo")
		{
			uploadVideo();
		}else if($_GET["p"]=="uploadVideoMultipart")
		{
			uploadVideoMultipart();
		}
		else
		if($_GET["p"]=="gifupload")
		{
			gifupload();
		}
		
		else
		if($_GET["p"]=="signup")
		{
			signup();
		}
		else
		if($_GET["p"]=="showAllVideos")
		{
			showAllVideos();
		}
		else
		if($_GET["p"]=="getVideosSimilarToAudio")
		{
			getVideosSimilarToAudio();
		}
		else
		if($_GET["p"]=="getAudioOfVideo")
		{
			getAudioOfVideo();
		}
		else
		if($_GET["p"]=="showMyAllVideos")
		{
			showMyAllVideos();
		}
		else
		if($_GET["p"]=="showAllVideosFollowing")
		{
			showAllVideosFollowing();
		}
		else
		if($_GET["p"]=="likeDislikeVideo")
		{
			likeDislikeVideo();
		}
		else
		if($_GET["p"]=="viewVideo")
		{
			viewVideo();
		}
		else
		if($_GET["p"]=="postComment")
		{
			postComment();
		}
		else
		if($_GET["p"]=="showVideoComments")
		{
			showVideoComments();
		}
		else
		if($_GET["p"]=="updateVideoView")
		{
			updateVideoView();
		}
		else
		if($_GET["p"]=="updateVideoScore")
		{
			updateVideoScore();
		}
		else
		if($_GET["p"]=="allSounds")
		{
			allSounds();
		}
		else
		if($_GET["p"]=="fav_sound")
		{
			fav_sound();
		}
		else
		if($_GET["p"]=="my_FavSound")
		{
			my_FavSound();
		}
		else
		if($_GET["p"]=="my_liked_video")
		{
			my_liked_video();
		}
		else
		if($_GET["p"]=="my_notifications")
		{
			my_notifications();
		}
		else
		if($_GET["p"]=="discover")
		{
			GetHashTagsDiscover();
		}
		else
		if($_GET["p"]=="edit_profile")
		{
			edit_profile();
		}
		else
		if($_GET["p"]=="follow_users")
		{
			follow_users();
		}
		else
		if($_GET["p"]=="get_user_data")
		{
			get_user_data();
		}
		else
		if($_GET["p"]=="get_followers")
		{
			get_followers();
		}
		else
		if($_GET["p"]=="get_followings")
		{
			get_followings();
		}
		else
		if($_GET["p"]=="SearchByHashTag")
		{
			SearchByHashTag();
		}
		else
		if($_GET["p"]=="SearchByCategory")
		{
			SearchByCategory();
		}
		else
		if($_GET["p"]=="GetHashTags")
		{
			GetHashTags();
		}
		else
		if($_GET["p"]=="UpdateProfilePhoto")
		{
			UpdateProfilePhoto();
		}
		else
		if($_GET["p"]=="GetLikes")
		{
			GetLikes();
		}

		else
		if($_GET["p"]=="SaveToFavourites")
		{
			SaveToFavourites();
		}
		else
		if($_GET["p"]=="GetDownloadLink")
		{
			GetDownloadLink();
		}
		else
		if($_GET["p"]=="DeleteVideo")
		{
			DeleteVideo();
		}
		else
		if($_GET["p"]=="createLog")
		{
			createLog();
		}
		else
		if($_GET["p"]=="getLogs")
		{
			getLogs();
		}
		else
		if($_GET["p"]=="offer_services")
		{
			offer_services();
		}

   		
		//admin panel functions
		else
		if($_GET["p"]=="Admin_Login")
		{
			Admin_Login();
		}
		else
		if($_GET["p"]=="All_Users")
		{
			All_Users();
		}
		else
		if($_GET["p"]=="admin_all_sounds")
		{
			admin_all_sounds();
		}
		else
		if($_GET["p"]=="admin_uploadSound")
		{
			admin_uploadSound();
		}
		else
		if($_GET["p"]=="admin_getSoundSection")
		{
			admin_getSoundSection();
		}
		else
		if($_GET["p"]=="admin_show_allVideos")
		{
			admin_show_allVideos();
		}
		else
		if($_GET["p"]=="ReportUser")
		{
			ReportUser();
		}
		else
		if($_GET["p"]=="ReportVideo")
		{
			ReportVideo();
		}
		else
		if($_GET["p"]=="ReportComment")
		{
			ReportComment();
		}
		else
		if($_GET["p"]=="updateUserName")
		{
			updateUserName();
		}
		
		
	
	
	}
	else
	{
		echo"Not Found";

	}
	

	function sendPushNotificationToMobileDevice($data)
	{
        require_once("config.php");
        $key=firebase_key;
      
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                "authorization: key=".$key."",
                "cache-control: no-cache",
                "content-type: application/json",
                "postman-token: 85f96364-bf24-d01e-3805-bccf838ef837"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) 
        {
           // print_r($err);
        } 
        else 
        {
            //print_r($response);
        }

    }
	
	
    			
    			
	function signup()
	{
		require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
		
		if(isset($event_json['fb_id']) && isset($event_json['first_name']) && isset($event_json['last_name']))
		{
			$fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
			$first_name=htmlspecialchars(strip_tags($event_json['first_name'] , ENT_QUOTES));
			$last_name=htmlspecialchars(strip_tags($event_json['last_name'] , ENT_QUOTES));
			$gender=htmlspecialchars(strip_tags($event_json['gender'] , ENT_QUOTES));
			$profile_pic=htmlspecialchars_decode(stripslashes($event_json['profile_pic']));
			$version=htmlspecialchars_decode(stripslashes($event_json['version']));
			$device=htmlspecialchars_decode(stripslashes($event_json['device']));
			$signup_type=htmlspecialchars_decode(stripslashes($event_json['signup_type']));

			if(isset($event_json['bio'])){
				$bio=htmlspecialchars_decode(stripslashes($event_json['bio']));
			}else{
				$bio = '';
			}
			if(isset($event_json['dob'])){
				$dob=htmlspecialchars_decode(stripslashes($event_json['dob']));
			}else{
				$dob = '';
			}
		    
		    $first_name = $first_name=='' ? substr($fb_id, 0, 3) : $first_name;
		    $username=$first_name.rand();
		    $profile_pic = $profile_pic=='' ? 'user.jpg' : $profile_pic;
		    
			$log_in="select * from users where fb_id='".$fb_id."' ";
			$log_in_rs=mysqli_query($conn,$log_in);
			
			if(mysqli_num_rows($log_in_rs))
			{   
			    $rd=mysqli_fetch_object($log_in_rs);  
			    @mysqli_query($conn, "UPDATE users set login_time = '".date("Y-m-d H:i:s")."' where id = '".$rd->id."'");
			     
				$array_out = array();
				 $array_out[] = 
					//array("code" => "200");
					array(
						"fb_id" => $rd->fb_id,
						"action" => "login",
						"profile_pic" => modifyPhoto($rd->profile_pic, $MEDIA_path),
						"first_name" => $rd->first_name,
						"last_name" => $rd->last_name,
						"username" => $rd->username,
						"bio" => $rd->bio,
						"gender" => $rd->gender,
						"dob" => $rd->dob
					);
				
				$output=array( "code" => "200", "msg" => $array_out);
				print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
			}	
			else
			{
			    
			    $qrry_1="insert into users(fb_id,username,first_name,last_name,profile_pic,version,device,signup_type,bio,dob,gender, login_time)values(";
    			$qrry_1.="'".$fb_id."',";
    			$qrry_1.="'".$username."',";
    			$qrry_1.="'".$first_name."',";
    			$qrry_1.="'".$last_name."',";
    			$qrry_1.="'".$profile_pic."',";
    			$qrry_1.="'".$version."',";
    			$qrry_1.="'".$device."',";
    			$qrry_1.="'".$signup_type."',";
    			$qrry_1.="'".$bio."',";
    			$qrry_1.="'".$dob."',";
    			$qrry_1.="'".$gender."',";
    			$qrry_1.="'".date("Y-m-d H:i:s")."'";
    			$qrry_1.=")";
    			if(mysqli_query($conn,$qrry_1))
    			{
				     $last_insert_fb_id = mysqli_insert_id($conn);
				     
				     $array_out = array();
    				 $array_out[] = 
    					//array("code" => "200");
    					array(
    						"fb_id" => $fb_id,
    						"username" => $username,
    						"action" => "signup",
    						"profile_pic" => modifyPhoto($profile_pic, $MEDIA_path),
    						"username" => $username,
    						"first_name" => $first_name,
    						"last_name" => $last_name,
    						"signup_type" => $signup_type,
    						"gender" => $gender,
    						"bio" => $bio,
    						"dob" => $dob
    					);
    				
    				$output=array( "code" => "200", "msg" => $array_out);
    				print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    			}
    			else
    			{
    			    //echo mysqli_error();
    			    $array_out = array();
    					
            		 $array_out[] = 
            			array(
            			"response" =>"problem in signup");
            		
            		$output=array( "code" => "201", "msg" => $array_out);
            		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    			}
			}
			
			
			
		}
		else
		{
			$array_out = array();
					
			 $array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}
	
	function updateUserName(){
		require_once("config.php");
		$query="select fb_id,id 
				from users 
				WHERE first_name > 0 
				ORDER BY CAST(first_name as SIGNED INTEGER) ASC
				";
		$usersnameWithMob=mysqli_query($conn,$query);
		$usersnameWithMobArr=mysqli_fetch_assoc($usersnameWithMob);
		$array_out = array();
		$i=1;
		while($row=mysqli_fetch_array($usersnameWithMob))
    		{
    			$user = "user_".$i."".rand();
    			mysqli_query($conn,"update users set username ='".$user."' where id = '".$row['id']."'");
    			$i++;
    		}
    	print_r("username successfully updated");
		
	}
	function gifupload()
	{
	            //giffimage
	            
				require_once("config.php");
        		$input = file_get_contents("php://input");
        	    $event_json = json_decode($input,true);
        	   
        	    //print_r($event_json);
        	    $gif1 = $event_json['giffimage']['file_data'];
        	    
        	    
        	    $event_json['giffimage'];
        	    //print_r($event_json['fb_id']);
        	    
        	    $gif = base64_decode($gif1);
			    
			    
			      $fileName="hamza".rand();
			 
			
		    	//file_put_contents("upload/gif/".$fileName.".gif", $gif);
		    	uploadToS3("gifs/".$fileName.".gif", $gif);

			    
			


	}
	
	function uploadVideo()
	{
	    require_once("config.php");
		$input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		
		if(isset($event_json['fb_id']) && isset($event_json['picbase64'])  && isset($event_json['videobase64']))
		{   
		    $fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
		    $description=htmlspecialchars(strip_tags($event_json['description'] , ENT_QUOTES));
		    $sound_id=htmlspecialchars(strip_tags($event_json['sound_id'] , ENT_QUOTES));
		    $section=htmlspecialchars(strip_tags($event_json['section'] , ENT_QUOTES));
		    $video = $event_json['videobase64']['file_data'];
		    $thum = $event_json['picbase64']['file_data'];
		    $gif = $event_json['gifbase64']['file_data'];
            
            $fileName=rand()."_".rand();
			$video_url= $fileName.".mp4";
			$thum_url= $fileName.".jpg";
			$gif_url= $fileName.".gif";
			
			/*list($type, $data) = explode(',', $data);
			list(, $data)      = explode(',', $data);*/
			$thum = base64_decode($thum);
			
			// file_put_contents("upload/thum/".$fileName.".jpg", $thum);
			uploadToS3("thumbnails/".$fileName.".jpg", $thum);
			
			$video = base64_decode($video);
			
			// file_put_contents("upload/video/".$fileName.".mp4", $video);
			uploadToS3("videos/".$fileName.".mp4", $video);
			
			
			$gif = base64_decode($gif);
			
			// file_put_contents("upload/gif/".$fileName.".gif", $gif);
			uploadToS3("gifs/".$fileName.".gif", $gif);
			
		
			
			$qrry_1="insert into videos(description,video,sound_id,fb_id,gif,thum,original_video,section)values(";
			$qrry_1.="'".$description."',";
			$qrry_1.="'".$video_url."',";
			$qrry_1.="'".$sound_id."',";
			$qrry_1.="'".$fb_id."',";
			$qrry_1.="'".$gif_url."',";
			$qrry_1.="'".$thum_url."',";
			$qrry_1.="'".$video_url."',";
			$qrry_1.="'".$section."'";
			$qrry_1.=")";
			if(mysqli_query($conn,$qrry_1))
			{
			   $array_out = array();
    			$array_out[] = 
    				array(
    					"response" => "file uploaded"
    				);
    			
    			$output=array( "code" => "200", "msg" => $array_out);
    			print_r(json_encode($output, JSON_UNESCAPED_SLASHES)); 
			}
			else
			{
			    $array_out = array();
    			$array_out[] = 
    				array(
    					"response" => "error in uploading files"
    				);
    			
    			$output=array( "code" => "201", "msg" => $array_out);
    			print_r(json_encode($output, JSON_UNESCAPED_SLASHES)); 
			}
			
			
			
			
		}
		else
		{
			$array_out = array();
			$array_out[] = 
				array(
					"response" => "json parem missing"
				);
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	
	}

	function uploadVideoMultipart()
	{
	    require_once("config.php");
		
		try{
		if(isset($_POST['fb_id']) && isset($_FILES['pic'])  && isset($_FILES['video']))
		{   
		    $fb_id=htmlspecialchars(strip_tags($_POST['fb_id'] , ENT_QUOTES));
		    $description=htmlspecialchars(strip_tags($_POST['description'] , ENT_QUOTES));	
		    $sound_id=htmlspecialchars(strip_tags($_POST['sound_id'] , ENT_QUOTES));
		    $section=htmlspecialchars(strip_tags($_POST['section'] , ENT_QUOTES));
		    $tags=htmlspecialchars(strip_tags($_POST['tags'] , ENT_QUOTES));
		    $location=htmlspecialchars(strip_tags($_POST['location'] , ENT_QUOTES));
		    $video_score=htmlspecialchars(strip_tags($_POST['video_score'] , ENT_QUOTES));
		    $video_language=htmlspecialchars(strip_tags($_POST['video_language'] , ENT_QUOTES));
		    $video = file_get_contents($_FILES['video']['tmp_name']);
		    $thum = file_get_contents($_FILES['pic']['tmp_name']);
		    $gif = file_get_contents($_FILES['gif']['tmp_name']);
            
            $fileName=rand()."_".rand();
			$video_url= $fileName.".mp4";
			$thum_url= $fileName.".jpg";
			$gif_url= $fileName.".gif";
			
			/*list($type, $data) = explode(',', $data);
			list(, $data)      = explode(',', $data);*/
			//$thum = base64_decode($thum);
			
			// file_put_contents("upload/thum/".$fileName.".jpg", $thum);
			uploadToS3("thumbnails/".$fileName.".jpg", $thum);
			
			//$video = base64_decode($video);
			
			// file_put_contents("upload/video/".$fileName.".mp4", $video);
			uploadToS3("videos/".$fileName.".mp4", $video);
			
			
			//$gif = base64_decode($gif);
			
			// file_put_contents("upload/gif/".$fileName.".gif", $gif);
			uploadToS3("gifs/".$fileName.".gif", $gif);
		
			
			$qrry_1="insert into videos(description,video,sound_id,fb_id,gif,thum,original_video,location,video_score,tags,video_language,section)values(";
			$qrry_1.="'".$description."',";
			$qrry_1.="'".$video_url."',";
			$qrry_1.="'".$sound_id."',";
			$qrry_1.="'".$fb_id."',";
			$qrry_1.="'".$gif_url."',";
			$qrry_1.="'".$thum_url."',";
			$qrry_1.="'".$video_url."',";
			$qrry_1.="'".$location."',";
			$qrry_1.="'".$video_score."',";
			$qrry_1.="'".$tags."',";
			$qrry_1.="'".$video_language."',";
			$qrry_1.="'".$section."'";
	//		$qrry_1.="'".$tags."'";
			// $qrry_1.="'".$location."'";
			// $qrry_1.="'".$video_score."'";
			// $qrry_1.="'".$video_language."'";
			$qrry_1.=")";
			if(mysqli_query($conn,$qrry_1))
			{
			   $array_out = array();
    			$array_out[] = 
    				array(
    					"response" => "file uploaded"
    				);
    			
    			$output=array( "code" => "200", "msg" => $array_out);
    			print_r(json_encode($output, JSON_UNESCAPED_SLASHES)); 
			}
			else
			{
			    $array_out = array();
    			$array_out[] = 
    				array(
    					"response" => "error in uploading files"
    				);
    			
    			$output=array( "code" => "201", "msg" => $array_out);
    			print_r(json_encode($output, JSON_UNESCAPED_SLASHES)); 
			}
			
			
			
			
		}
		else
		{
			$array_out = array();
			$array_out[] = 
				array(
					"response" => "json parem missing"
				);
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}
	catch(Exception $e){
		$array_out = array();
			$array_out[] = 
				array(
					"response" => $e->getMessage()
				);
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));	
	}
	
	}
	
	function showAllVideos()
	{
	    
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
	
		if(isset($event_json['fb_id']))
		{
			$fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
			$token=$event_json['token'];
			
			@mysqli_query($conn,"update users set tokon='".$token."' where fb_id='".$fb_id."' ");
			
			$query=mysqli_query($conn,"select * from videos where id NOT IN (SELECT video_view.video_id FROM video_view WHERE video_view.fb_id='".$fb_id."') order by video_score desc");
		        
    		$array_out = array();
    		while($row=mysqli_fetch_array($query))
    		{
    		    
    		    $query1=mysqli_query($conn,"select * from users where fb_id='".$row['fb_id']."' ");
		        $rd=mysqli_fetch_object($query1);
		       
		        $query112=mysqli_query($conn,"select * from sound where id='".$row['sound_id']."' ");
		        $rd12=mysqli_fetch_object($query112);
		        
		        $countLikes = mysqli_query($conn,"SELECT count(*) as count from video_like_dislike where video_id='".$row['id']."' ");
                $countLikes_count=mysqli_fetch_assoc($countLikes);
		        
		        $countcomment = mysqli_query($conn,"SELECT count(*) as count from video_comment where video_id='".$row['id']."' ");
                $countcomment_count=mysqli_fetch_assoc($countcomment);
                
                
                $liked = mysqli_query($conn,"SELECT count(*) as count from video_like_dislike where video_id='".$row['id']."' and fb_id='".$fb_id."' ");
                $liked_count=mysqli_fetch_assoc($liked);

                $like_array = likeArray($conn, $MEDIA_path, $liked_count, $row['id'], $fb_id);

                $queryfollow=mysqli_query($conn,"SELECT count(*) as count from follow_users where fb_id='".$event_json['fb_id']."' and followed_fb_id='".$$row['fb_id']."' ");
		        $follow_count=mysqli_fetch_assoc($queryfollow);
		        
		       
                if($follow_count['count']=="0")
                {
                    $follow="0";
                    $follow_button_status="Follow";
                }
                else
                if($follow_count['count']!="0")
                {
                    $follow="1";
                    $follow_button_status="Unfollow";
                }
		        
        	   	$array_out[] = 
        			array(
        			"id" => $row['id'],
        			"fb_id" => $row['fb_id'],
        			"user_info" =>array
            					(
            					    "first_name" => $rd->first_name,
                        			"last_name" => $rd->last_name,
                        			"profile_pic" => modifyPhoto($rd->profile_pic, $MEDIA_path)
            					),
            		"count" =>array
            					(
            					    "like_count" => (string)rand(25,200),//$countLikes_count['count'],
                        			"video_comment_count" => $countcomment_count['count']
            					),
            		"liked"=> $liked_count['count'],
            		"like" => $like_array,			
            	    "video" => $MEDIA_path.'/videos/'.$row['video'],
            	    "original_video" => $MEDIA_path.'/videos/'.$row['original_video'],
        			"thum" => $MEDIA_path.'/thumbnails/'.$row['thum'],
        			"gif" => $MEDIA_path.'/gifs/'.$row['gif'],
        			"description" => $row['description'],
        			"tags" => $row['tags'],
        			"location" => $row['location'],
        			"video_language" => $row['video_language'],
        			"video_score" => $row['video_score'],
        			"sound" =>$rd12 ? array
            					(
            					    "id" => $rd12->id,
            					    "audio_path" => 
                            			array(
                                			"mp3" => $MEDIA_path."/audios/".$rd12->id.".mp3",
                    			            "acc" => $MEDIA_path."/audios/".$rd12->id.".aac"
                                		),
                        			"sound_name" => $rd12->sound_name,
                        			"description" => $rd12->description,
                        			"thum" => $rd12->thum,
                        			"section" => $rd12->section,
                        			"created" => $rd12->created,
                        			"duration" => $rd12->duration 
            					) : array(),
            		"follow_Status" =>array
    					(
    					    "follow" => $follow,
                			"follow_status_button" => $follow_button_status
    					),
        			"created" => $row['created']
        		);
    			
    		}
    		$output=array( "code" => "200", "msg" => $array_out);
    		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    		
		}
		else
		{
			$array_out = array();
					
			 $array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}
	
	function showAllVideosFollowing()
	{
	    
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
	
		if(isset($event_json['fb_id']))
		{
			$fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
			$token=$event_json['token'];
			
			@mysqli_query($conn,"update users set tokon='".$token."' where fb_id='".$fb_id."' ");
			
			$query=mysqli_query($conn,"select * from videos where id NOT IN (SELECT video_view.video_id FROM video_view WHERE video_view.fb_id='".$fb_id."') and fb_id in (SELECT followed_fb_id from follow_users where fb_id = '".$fb_id."') order by video_score desc");
		        
    		$array_out = array();
    		while($row=mysqli_fetch_array($query))
    		{
    		    
    		    $query1=mysqli_query($conn,"select * from users where fb_id='".$row['fb_id']."' ");
		        $rd=mysqli_fetch_object($query1);
		       
		        $query112=mysqli_query($conn,"select * from sound where id='".$row['sound_id']."' ");
		        $rd12=mysqli_fetch_object($query112);
		        
		        $countLikes = mysqli_query($conn,"SELECT count(*) as count from video_like_dislike where video_id='".$row['id']."' ");
                $countLikes_count=mysqli_fetch_assoc($countLikes);
		        
		        $countcomment = mysqli_query($conn,"SELECT count(*) as count from video_comment where video_id='".$row['id']."' ");
                $countcomment_count=mysqli_fetch_assoc($countcomment);
                
                
                $liked = mysqli_query($conn,"SELECT count(*) as count from video_like_dislike where video_id='".$row['id']."' and fb_id='".$fb_id."' ");
                $liked_count=mysqli_fetch_assoc($liked);

                $like_array = likeArray($conn, $MEDIA_path, $liked_count, $row['id'], $fb_id);

                $queryfollow=mysqli_query($conn,"SELECT count(*) as count from follow_users where fb_id='".$event_json['fb_id']."' and followed_fb_id='".$$row['fb_id']."' ");
		        $follow_count=mysqli_fetch_assoc($queryfollow);
		        
		       
                if($follow_count['count']=="0")
                {
                    $follow="0";
                    $follow_button_status="Follow";
                }
                else
                if($follow_count['count']!="0")
                {
                    $follow="1";
                    $follow_button_status="Unfollow";
                }
		        
        	   	$array_out[] = 
        			array(
        			"id" => $row['id'],
        			"fb_id" => $row['fb_id'],
        			"user_info" =>array
            					(
            					    "first_name" => $rd->first_name,
                        			"last_name" => $rd->last_name,
                        			"profile_pic" => modifyPhoto($rd->profile_pic, $MEDIA_path)
            					),
            		"count" =>array
            					(
            					    "like_count" => (string)rand(25,200),//$countLikes_count['count'],
                        			"video_comment_count" => $countcomment_count['count']
            					),
            		"liked"=> $liked_count['count'],
            		"like" => $like_array,			
            	    "video" => $MEDIA_path.'/videos/'.$row['video'],
            	    "original_video" => $MEDIA_path.'/videos/'.$row['original_video'],
        			"thum" => $MEDIA_path.'/thumbnails/'.$row['thum'],
        			"gif" => $MEDIA_path.'/gifs/'.$row['gif'],
        			"description" => $row['description'],
        			"tags" => $row['tags'],
        			"location" => $row['location'],
        			"video_language" => $row['video_language'],
        			"video_score" => $row['video_score'],
        			"sound" =>$rd12 ? array
            					(
            					    "id" => $rd12->id,
            					    "audio_path" => 
                            			array(
                                			"mp3" => $MEDIA_path."/audios/".$rd12->id.".mp3",
                    			            "acc" => $MEDIA_path."/audios/".$rd12->id.".aac"
                                		),
                        			"sound_name" => $rd12->sound_name,
                        			"description" => $rd12->description,
                        			"thum" => $rd12->thum,
                        			"section" => $rd12->section,
                        			"created" => $rd12->created,
                        			"duration" => $rd12->duration 
            					) : array(),
            		"follow_Status" =>array
    					(
    					    "follow" => $follow,
                			"follow_status_button" => $follow_button_status
    					),
        			"created" => $row['created']
        		);
    			
    		}
    		$output=array( "code" => "200", "msg" => $array_out);
    		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    		
		}
		else
		{
			$array_out = array();
					
			 $array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}

	function getVideosSimilarToAudio()
	{
	    
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
	
		if(isset($event_json['fb_id']) && isset($event_json['video_id']))
		{
			$fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
			$token=$event_json['token'];
			$video_id=$event_json['video_id'];
			$sound_id=0;
			
			@mysqli_query($conn,"update users set tokon='".$token."' where fb_id='".$fb_id."' ");

			$existing_video="select * from videos where id='".$video_id."' ";
			$existing_video_rs=mysqli_query($conn,$existing_video);
			
			if(mysqli_num_rows($existing_video_rs))
			{   
	    		$sound_id = 0;
	    		while($row=mysqli_fetch_array($existing_video_rs))
	    		{
	    		    
	    		    $sound_id = $row['sound_id'];
	    		    $query1=mysqli_query($conn,"select * from users where fb_id='".$row['fb_id']."' ");
			        $rd=mysqli_fetch_object($query1);
			       
			        $query112=mysqli_query($conn,"select * from sound where id='".$row['sound_id']."' ");
			        $rd12=mysqli_fetch_object($query112);
			        
			        $countLikes = mysqli_query($conn,"SELECT count(*) as count from video_like_dislike where video_id='".$row['id']."' ");
	                $countLikes_count=mysqli_fetch_assoc($countLikes);
			        
			        $countcomment = mysqli_query($conn,"SELECT count(*) as count from video_comment where video_id='".$row['id']."' ");
	                $countcomment_count=mysqli_fetch_assoc($countcomment);
	                
	                
	                $liked = mysqli_query($conn,"SELECT count(*) as count from video_like_dislike where video_id='".$row['id']."' and fb_id='".$fb_id."' ");
	                $liked_count=mysqli_fetch_assoc($liked);

	                $like_array = likeArray($conn, $MEDIA_path, $liked_count, $row['id'], $fb_id);
			        
	        	   	$temp = 
	        			array(
	        			"id" => $row['id'],
	        			"fb_id" => $row['fb_id'],
	        			"user_info" =>array
	            					(
	            					    "first_name" => $rd->first_name,
	                        			"last_name" => $rd->last_name,
	                        			"profile_pic" => modifyPhoto($rd->profile_pic, $MEDIA_path)
	            					),
	            		"count" =>array
	            					(
	            					    "like_count" => (string)rand(25,200),//$countLikes_count['count'],
	                        			"video_comment_count" => $countcomment_count['count'],
	                        			"view" => (string)rand(25,200),//$row['view'],
	            					),
	            		"liked"=> $liked_count['count'],
	            		"like" => $like_array,			
	            	    "video" => $MEDIA_path.'/videos/'.$row['video'],
	            	    "original_video" => $MEDIA_path.'/videos/'.$row['original_video'],
	        			"thum" => $MEDIA_path.'/thumbnails/'.$row['thum'],
	        			"gif" => $MEDIA_path.'/gifs/'.$row['gif'],
	        			"description" => $row['description'],
	        			"sound" =>array
	            					(
	            					    "id" => $rd12->id,
	            					    "audio_path" => 
	                            			array(
	                                			"mp3" => $MEDIA_path."/audios/".$rd12->id.".mp3",
	                    			            "acc" => $MEDIA_path."/audios/".$rd12->id.".aac"
	                                		),
	                        			"sound_name" => $rd12->sound_name,
	                        			"description" => $rd12->description,
	                        			"thum" => $rd12->thum,
	                        			"section" => $rd12->section,
	                        			"created" => $rd12->created,
	                        			"duration" => $rd12->duration
	            					),
	        			"created" => $row['created']
	        		);
	    			
	    		}

			    if($sound_id==0){
					$array_out = array();
					$array_out = [];
					$output=array( "code" => "200", "msg" => ['original'=>$temp, 'similar'=>$array_out]);
	    			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
				}else{
					$query=mysqli_query($conn,"select * from videos where sound_id='".$sound_id."' and id!='".$video_id."' order by video_score desc ");
			        
		    		$array_out = array();
		    		while($row=mysqli_fetch_array($query))
		    		{
		    		    
		    		    $query1=mysqli_query($conn,"select * from users where fb_id='".$row['fb_id']."' ");
				        $rd=mysqli_fetch_object($query1);
				       
				        $query112=mysqli_query($conn,"select * from sound where id='".$row['sound_id']."' ");
				        $rd12=mysqli_fetch_object($query112);
				        
				        $countLikes = mysqli_query($conn,"SELECT count(*) as count from video_like_dislike where video_id='".$row['id']."' ");
		                $countLikes_count=mysqli_fetch_assoc($countLikes);
				        
				        $countcomment = mysqli_query($conn,"SELECT count(*) as count from video_comment where video_id='".$row['id']."' ");
		                $countcomment_count=mysqli_fetch_assoc($countcomment);
		                
		                
		                $liked = mysqli_query($conn,"SELECT count(*) as count from video_like_dislike where video_id='".$row['id']."' and fb_id='".$fb_id."' ");
		                $liked_count=mysqli_fetch_assoc($liked);

		                $like_array = likeArray($conn, $MEDIA_path, $liked_count, $row['id'], $fb_id);
				        
		        	   	$array_out[] = 
		        			array(
		        			"id" => $row['id'],
		        			"fb_id" => $row['fb_id'],
		        			"user_info" =>array
		            					(
		            					    "first_name" => $rd->first_name,
		                        			"last_name" => $rd->last_name,
		                        			"profile_pic" => modifyPhoto($rd->profile_pic, $MEDIA_path)
		            					),
		            		"count" =>array
		            					(
		            					    "like_count" => (string)rand(25,200),//$countLikes_count['count'],
		                        			"video_comment_count" => $countcomment_count['count'],
		                        			"view" => (string)rand(25,200),//$row['view'],
		            					),
		            		"liked"=> $liked_count['count'],
		            		"like" => $like_array,			
		            	    "video" => $MEDIA_path.'/videos/'.$row['video'],
		            	    "original_video" => $MEDIA_path.'/videos/'.$row['original_video'],
		        			"thum" => $MEDIA_path.'/thumbnails/'.$row['thum'],
		        			"gif" => $MEDIA_path.'/gifs/'.$row['gif'],
		        			"description" => $row['description'],
		        			"sound" =>array
		            					(
		            					    "id" => $rd12->id,
		            					    "audio_path" => 
		                            			array(
		                                			"mp3" => $MEDIA_path."/audios/".$rd12->id.".mp3",
		                    			            "acc" => $MEDIA_path."/audios/".$rd12->id.".aac"
		                                		),
		                        			"sound_name" => $rd12->sound_name,
		                        			"description" => $rd12->description,
		                        			"thum" => $rd12->thum,
		                        			"section" => $rd12->section,
		                        			"created" => $rd12->created,
		                        			"duration" => $rd12->duration
		            					),
		        			"created" => $row['created']
		        		);
		    			
		    		}
		    		$output=array( "code" => "200", "msg" => ['original'=>$temp, 'similar'=>$array_out]);
		    		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
				}
			}else{
				$array_out = array();
				$array_out[] = 
					array(
					"response" =>"This video does not exist anymore");
				
				$output=array( "code" => "201", "msg" => $array_out);
				print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
			}
		}
		else
		{
			$array_out = array();
					
			$array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}

	function getAudioOfVideo(){
		require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
	
		if(isset($event_json['fb_id']) && isset($event_json['video_id']))
		{
			$fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
			$token=$event_json['token'];
			$video_id=$event_json['video_id'];
			$sound_id=0;
			
			@mysqli_query($conn,"update users set tokon='".$token."' where fb_id='".$fb_id."' ");

			$existing_video="select * from videos where id='".$video_id."' ";
			$existing_video_rs=mysqli_query($conn,$existing_video);
			
			if(mysqli_num_rows($existing_video_rs))
			{   
	    		while($row=mysqli_fetch_array($existing_video_rs))
	    		{
	    		    
	    		    $query1=mysqli_query($conn,"select * from users where fb_id='".$row['fb_id']."' ");
			        $rd=mysqli_fetch_object($query1);
			       
			        $query112=mysqli_query($conn,"select * from sound where id='".$row['sound_id']."' ");
			        $rd12=mysqli_fetch_object($query112);
			        
			        $countLikes = mysqli_query($conn,"SELECT count(*) as count from video_like_dislike where video_id='".$row['id']."' ");
	                $countLikes_count=mysqli_fetch_assoc($countLikes);
			        
			        $countcomment = mysqli_query($conn,"SELECT count(*) as count from video_comment where video_id='".$row['id']."' ");
	                $countcomment_count=mysqli_fetch_assoc($countcomment);
	                
	                
	                $liked = mysqli_query($conn,"SELECT count(*) as count from video_like_dislike where video_id='".$row['id']."' and fb_id='".$fb_id."' ");
	                $liked_count=mysqli_fetch_assoc($liked);

	                $like_array = likeArray($conn, $MEDIA_path, $liked_count, $row['id'], $fb_id);
			        
	        	   	$temp = 
	        			array(
	        			"id" => $row['id'],
	        			"fb_id" => $row['fb_id'],
	        			"user_info" =>array
	            					(
	            					    "first_name" => $rd->first_name,
	                        			"last_name" => $rd->last_name,
	                        			"profile_pic" => modifyPhoto($rd->profile_pic, $MEDIA_path)
	            					),
	            		"count" =>array
	            					(
	            					    "like_count" => (string)rand(25,200),//$countLikes_count['count'],
	                        			"video_comment_count" => $countcomment_count['count'],
	                        			"view" => (string)rand(25,200),//$row['view'],
	            					),
	            		"liked"=> $liked_count['count'],
	            		"like" => $like_array,			
	            	    "video" => $MEDIA_path.'/videos/'.$row['video'],
	            	    "original_video" => $MEDIA_path.'/videos/'.$row['original_video'],
	        			"thum" => $MEDIA_path.'/thumbnails/'.$row['thum'],
	        			"gif" => $MEDIA_path.'/gifs/'.$row['gif'],
	        			"description" => $row['description'],
	        			"sound" =>array
	            					(
	            					    "id" => $rd12->id,
	            					    "audio_path" => 
	                            			array(
	                                			"mp3" => $MEDIA_path."/audios/".$rd12->id.".mp3",
	                    			            "acc" => $MEDIA_path."/audios/".$rd12->id.".aac"
	                                		),
	                        			"sound_name" => $rd12->sound_name,
	                        			"description" => $rd12->description,
	                        			"thum" => $rd12->thum,
	                        			"section" => $rd12->section,
	                        			"created" => $rd12->created,
	                        			"duration" => $rd12->duration
	            					),
	        			"created" => $row['created']
	        		);
	    			
	    		}

			    $array_out = array();
				$array_out = [];
				$output=array( "code" => "200", "msg" => $temp);
    			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));

			}else{
				$array_out = array();
				$array_out[] = 
					array(
					"response" =>"This video does not exist anymore");
				
				$output=array( "code" => "201", "msg" => $array_out);
				print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
			}
		}
		else
		{
			$array_out = array();
					
			$array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}
	
	function SearchByHashTag()
	{
	    
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
	
		if(isset($event_json['fb_id']))
		{
			$fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
			$tag=htmlspecialchars(strip_tags($event_json['tag'] , ENT_QUOTES));
			$token=$event_json['token'];
			
			@mysqli_query($conn,"update users set tokon='".$token."' where fb_id='".$fb_id."' ");

			
			$query=mysqli_query($conn,"select * from videos where description like '%".$tag."%' OR tags like '%".$tag."%' order by video_score DESC");
		        
    		$array_out = array();
    		while($row=mysqli_fetch_array($query))
    		{
    		    
    		    $query1=mysqli_query($conn,"select * from users where fb_id='".$row['fb_id']."' ");
		        $rd=mysqli_fetch_object($query1);
		       
		        $query112=mysqli_query($conn,"select * from sound where id='".$row['sound_id']."' ");
		        $rd12=mysqli_fetch_object($query112);
		        
		        $countLikes = mysqli_query($conn,"SELECT count(*) as count from video_like_dislike where video_id='".$row['id']."' ");
                $countLikes_count=mysqli_fetch_assoc($countLikes);
		        
		        $countcomment = mysqli_query($conn,"SELECT count(*) as count from video_comment where video_id='".$row['id']."' ");
                $countcomment_count=mysqli_fetch_assoc($countcomment);
                
                
                $liked = mysqli_query($conn,"SELECT count(*) as count from video_like_dislike where video_id='".$row['id']."' and fb_id='".$fb_id."' ");
                $liked_count=mysqli_fetch_assoc($liked);

                $like_array = likeArray($conn, $MEDIA_path, $liked_count, $row['id'], $fb_id);
		        
        	   	$array_out[] = 
        			array(
        			"id" => $row['id'],
        			"fb_id" => $row['fb_id'],
        			"user_info" =>array
            					(
            					    "first_name" => $rd->first_name,
                        			"last_name" => $rd->last_name,
                        			"profile_pic" => modifyPhoto($rd->profile_pic, $MEDIA_path)
            					),
            			"count" =>array
            					(
            					    "like_count" => (string)rand(25,200),//$countLikes_count['count'],
                        			"video_comment_count" => $countcomment_count['count'],
                        			"view" => (string)rand(25,200),//$row['view'],
            					),
            			"liked"=> $liked_count['count'],	
            			"like" => $like_array,		
            	    		"video" => $MEDIA_path.'/videos/'.$row['video'],
            	    		"original_video" => $MEDIA_path.'/videos/'.$row['original_video'],
        			"thum" => $MEDIA_path.'/thumbnails/'.$row['thum'],
        			"gif" => $MEDIA_path.'/gifs/'.$row['gif'],
        			"tags" => $row['tags'],
        			"description" => $row['description'],
        			"location" => $row['location'],
        			"video_score" => $row['video_score'],
        			"sound" =>array
            					(
            					    "id" => $rd12->id,
            					    "audio_path" => 
                            			array(
                                			"mp3" => $MEDIA_path."/audios/".$rd12->id.".mp3",
                    			            "acc" => $MEDIA_path."/audios/".$rd12->id.".aac"
                                		),
                        			"sound_name" => $rd12->sound_name,
                        			"description" => $rd12->description,
                        			"thum" => $rd12->thum,
                        			"section" => $rd12->section,
                        			"created" => $rd12->created,
                        			"duration" => $rd12->duration
            					),
        			"created" => $row['created']
        		);
    			
    		}
    		$query1=mysqli_query($conn,"select * from tags where tag like '%".$tag."%'");
    		$temp =(object)[];
    		while($row1=mysqli_fetch_array($query1))
			{
				$temp = array('id'=>$row1['id'], 'name'=>$row1['tag'],'image'=>$MEDIA_path.'/tags/'.$row1['image']);
    	   	}
    		$output=array( "code" => "200", "msg" => array("video"=>$array_out,"tag"=>$temp));
    		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    		
		}
		else
		{
			$array_out = array();
					
			 $array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}
	
	function SearchByCategory()
	{
	    
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
//		print_r($event_json);

		if(isset($event_json['fb_id']) && isset($event_json['category']))
		{
			$fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
			$category=htmlspecialchars(strip_tags($event_json['category'] , ENT_QUOTES));
			$token=$event_json['token'];
			
			@mysqli_query($conn,"update users set tokon='".$token."' where fb_id='".$fb_id."' ");

			$q1 = mysqli_query($conn, "SELECT * from categories where name = '".$category."'");
			$category_res = mysqli_fetch_object($q1);

			$q = mysqli_query($conn, "SELECT * from tags where category_id = '".$category_res->id."'");

    		$array_out = array();
    		while ($tag=mysqli_fetch_array($q)) {
    			$query_str = "select * from videos where description like '%".$tag['tag']."%' OR tags like '%".$tag['tag']."%' order by video_score DESC ";
    			$query=mysqli_query($conn, $query_str);

    			while($row=mysqli_fetch_array($query))
    			{

    				$query1=mysqli_query($conn,"select * from users where fb_id='".$row['fb_id']."' ");
    				$rd=mysqli_fetch_object($query1);

    				$query112=mysqli_query($conn,"select * from sound where id='".$row['sound_id']."' ");
    				$rd12=mysqli_fetch_object($query112);

    				$countLikes = mysqli_query($conn,"SELECT count(*) as count from video_like_dislike where video_id='".$row['id']."' ");
    				$countLikes_count=mysqli_fetch_assoc($countLikes);

    				$countcomment = mysqli_query($conn,"SELECT count(*) as count from video_comment where video_id='".$row['id']."' ");
    				$countcomment_count=mysqli_fetch_assoc($countcomment);


    				$liked = mysqli_query($conn,"SELECT count(*) as count from video_like_dislike where video_id='".$row['id']."' and fb_id='".$fb_id."' ");
    				$liked_count=mysqli_fetch_assoc($liked);

    				$like_array = likeArray($conn, $MEDIA_path, $liked_count, $row['id'], $fb_id);

    				$array_out[] = 
    				array(
    					"id" => $row['id'],
    					"fb_id" => $row['fb_id'],
    					"user_info" =>array
    					(
    						"first_name" => $rd->first_name,
    						"last_name" => $rd->last_name,
    						"profile_pic" => modifyPhoto($rd->profile_pic, $MEDIA_path)
    					),
    					"count" =>array
    					(
    						"like_count" => (string)rand(25,200),
    						"video_comment_count" => $countcomment_count['count'],
    						"view" => (string)rand(25,200),
    					),
    					"liked"=> $liked_count['count'],	
    					"like" => $like_array,		
    					"video" => $MEDIA_path.'/videos/'.$row['video'],
    					"original_video" => $MEDIA_path.'/videos/'.$row['original_video'],
    					"thum" => $MEDIA_path.'/thumbnails/'.$row['thum'],
    					"gif" => $MEDIA_path.'/gifs/'.$row['gif'],
    					"tags" => $row['tags'],
    					"description" => $row['description'],
    					"location" => $row['location'],
    					"video_score" => $row['video_score'],
    					"sound" =>array
    					(
    						"id" => $rd12->id,
    						"audio_path" => 
    						array(
    							"mp3" => $MEDIA_path."/audios/".$rd12->id.".mp3",
    							"acc" => $MEDIA_path."/audios/".$rd12->id.".aac"
    						),
    						"sound_name" => $rd12->sound_name,
    						"description" => $rd12->description,
    						"thum" => $rd12->thum,
    						"section" => $rd12->section,
    						"created" => $rd12->created,
    						"duration" => $rd12->duration
    					),
    					"created" => $row['created']
    				);

    			}
    		}
    		$query1=mysqli_query($conn,"select * from tags where tag like '%".$tag."%'");
    		$temp =(object)[];
    		while($row1=mysqli_fetch_array($query1))
    		{
    			$temp = array('id'=>$row1['id'], 'name'=>$row1['tag'],'image'=>$MEDIA_path.'/tags/'.$row1['image']);
    		}
    		$output=array( "code" => "200", "msg" => array("video"=>$array_out,"tag"=>$temp));
    		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    		
		}
		else
		{
			$array_out = array();
					
			 $array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}

	function GetHashTags()
	{
	    
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
	
		if(isset($event_json['fb_id']))
		{
			$fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
			$token=$event_json['token'];
			
			@mysqli_query($conn,"update users set tokon='".$token."' where fb_id='".$fb_id."' ");
			
			$query=mysqli_query($conn,"select * from categories");
		        
    		$array_out = array();
    		while($row=mysqli_fetch_array($query))
    		{
    		    
        	   	$temp = array(
        			"id" => $row['id'],
        			"name" => $row['name'],
        			"image" => $MEDIA_path.'/categories/'.$row['image'],
        			"tags" =>[]
        		);
        		$query1=mysqli_query($conn,"select * from tags where category_id=".$row['id']);
        		while($row1=mysqli_fetch_array($query1))
    			{
    				$temp['tags'][] = array('id'=>$row1['id'], 'name'=>$row1['tag'],'image'=>$MEDIA_path.'/tags/'.$row1['image']);
        	   	}
    			$array_out[] = $temp;
    		}
    		$output=array( "code" => "200", "msg" => $array_out);
    		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    		
		}
		else
		{
			$array_out = array();
					
			 $array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}

	function GetDownloadLink()
	{
	    
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
	
		if(isset($event_json['fb_id']) && isset($event_json['video_id']))
		{
			$fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
			$token=$event_json['token'];
			
			@mysqli_query($conn,"update users set tokon='".$token."' where fb_id='".$fb_id."' ");
			
			$query=mysqli_query($conn,"select * from videos where id=".$event_json['video_id']);
		        
    		$array_out = array();
    		while($row=mysqli_fetch_array($query))
    		{
    		    
        	   	if($row['isWatermark']==2){
        	   		$temp = array(
	        			"id" => $row['id'],
	        			"link" => $MEDIA_path.'/downloads/'.$row['original_video']
	        		);
        	   	}else{
        	   		$temp = array(
	        			"id" => $row['id'],
	        			"link" => $MEDIA_path.'/videos/'.$row['original_video']
	        		);
        	   	}
    			$array_out = $temp;
    		}
    		$output=array( "code" => "200", "msg" => $array_out);
    		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    		
		}
		else
		{
			$array_out = array();
					
			 $array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}

	function DeleteVideo()
	{
	    require_once("config.php");
		$input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
		
		if(isset($event_json['video_id']) && isset($event_json['fb_id']))
		{   
		    $id=htmlspecialchars(strip_tags($event_json['video_id'] , ENT_QUOTES));
		    $fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
		   	
			$qrry_1="select * from videos where id ='".$id."' and fb_id ='".$fb_id."'";
			$log_in_rs=mysqli_query($conn,$qrry_1);
			// @unlink($videoPath);
			// @unlink($thumPath);
			// @unlink($gifPath);
			
			if(mysqli_num_rows($log_in_rs)){
				
				$rd=mysqli_fetch_object($log_in_rs);
				
				$videoPath=$rd->video;
				$thumPath=$rd->thum;
				$gifPath=$rd->gif;
			
				mysqli_query($conn,"Delete from videos where id ='".$id."' ");
				mysqli_query($conn,"Delete from video_like_dislike where video_id ='".$id."' ");
				mysqli_query($conn,"Delete from video_comment where video_id ='".$id."' ");
				$array_out = array();
    					
    			$array_out[] = 
    				array(
    				"response" =>"video deleted");
    	        
	        	$output=array( "code" => "200", "msg" => $array_out);
	    		print_r(json_encode($output, true));
			}else{
				$array_out = array();
				$array_out[] = 
					array(
						"response" => "you are not allowed to delete video"
					);
				
				$output=array( "code" => "201", "msg" => $array_out);
				print_r(json_encode($output, true));
			}
		
		}
		else
		{
			$array_out = array();
			$array_out[] = 
				array(
					"response" => "json parem missing"
				);
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, true));
		} 
	}

	function GetHashTagsDiscover()
	{
	    
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
	
		if(isset($event_json['fb_id']))
		{
			$fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
			$token=$event_json['token'];
			
			@mysqli_query($conn,"update users set tokon='".$token."' where fb_id='".$fb_id."' ");
			
			$query=mysqli_query($conn,"select * from categories");
		        
    		$array_out = array();

    		$temp = array(
    			"id" => 0,
    			"name" => "Trending",
    			"image" => $MEDIA_path.'/categories/trending.png',
    			"tags" =>[]
    		);
    		$query1=mysqli_query($conn,"select * from tags where id in (45,46,47,48,50)");
    		while($row1=mysqli_fetch_array($query1))
			{
				$temp['tags'][] = array('id'=>$row1['id'], 'name'=>$row1['tag'],'image'=>$MEDIA_path.'/tags/'.$row1['image']);
    	   	}
			$array_out[] = $temp;
    		while($row=mysqli_fetch_array($query))
    		{
    		    
        	   	$temp = array(
        			"id" => $row['id'],
        			"name" => $row['name'],
        			"image" => $MEDIA_path.'/categories/'.$row['image'],
        			"tags" =>[]
        		);
        		$query1=mysqli_query($conn,"select * from tags where category_id=".$row['id']);
        		while($row1=mysqli_fetch_array($query1))
    			{
    				$temp['tags'][] = array('id'=>$row1['id'], 'name'=>$row1['tag'],'image'=>$MEDIA_path.'/tags/'.$row1['image']);
        	   	}
    			$array_out[] = $temp;
    		}
    //		$output=array( "code" => "200", "msg" => $array_out);
    		print_r(json_encode(json_decode(file_get_contents('hashtags.json')), JSON_UNESCAPED_SLASHES));
    		
		}
		else
		{
			$array_out = array();
					
			 $array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}

	function GetLikes()
	{
	    
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
	
		if(isset($event_json['fb_id']))
		{
			$fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
			$token=$event_json['token'];
			
			@mysqli_query($conn,"update users set tokon='".$token."' where fb_id='".$fb_id."' ");
			
			$query=mysqli_query($conn,"select * from likes");
		        
    		$array_out = array();
    		while($row=mysqli_fetch_array($query))
    		{
    		    
        	   	$temp = array(
        			"id" => $row['id'],
        			"image" => $MEDIA_path.'/likes/'.$row['image']
        		);
    			$array_out[] = $temp;
    		}
    		$output=array( "code" => "200", "msg" => $array_out);
    		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    		
		}
		else
		{
			$array_out = array();
					
			 $array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}
	
	
	function showMyAllVideos()
	{
	    
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
	
		if(isset($event_json['fb_id']) && isset($event_json['my_fb_id']))
		{
			$fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
			$my_fb_id=htmlspecialchars(strip_tags($event_json['my_fb_id'] , ENT_QUOTES));
			
		    $query1=mysqli_query($conn,"select * from users where fb_id='".$fb_id."' ");
		    $rd=mysqli_fetch_object($query1);
		    if(mysqli_num_rows($query1))
		    {
		        
		        $query=mysqli_query($conn,"select * from videos where fb_id='".$fb_id."' order by video_score DESC");
		        
		        $array_out_video = array();
        		while($row=mysqli_fetch_array($query))
        		{
        		  
        		   $countLikes = mysqli_query($conn,"SELECT count(*) as count from video_like_dislike where video_id='".$row['id']."' ");
                   $countLikes_count=mysqli_fetch_assoc($countLikes);
    		       
    		       $query112=mysqli_query($conn,"select * from sound where id='".$row['sound_id']."' ");
		           $rd12=mysqli_fetch_object($query112);
		        
    		       $countcomment = mysqli_query($conn,"SELECT count(*) as count from video_comment where video_id='".$row['id']."' ");
                   $countcomment_count=mysqli_fetch_assoc($countcomment);
                   
                   $liked = mysqli_query($conn,"SELECT count(*) as count from video_like_dislike where video_id='".$row['id']."' and fb_id='".$fb_id."' ");
                   $liked_count=mysqli_fetch_assoc($liked);

                   $like_array = likeArray($conn, $MEDIA_path, $liked_count, $row['id'], $fb_id);
                
        		   $array_out_video[] = 
            			array(
            			"id" => $row['id'],
            			"video" => $MEDIA_path.'/videos/'.$row['video'],
            			"original_video" => $MEDIA_path.'/videos/'.$row['original_video'],
            			"thum" => $MEDIA_path.'/thumbnails/'.$row['thum'],
            			"gif" => $MEDIA_path.'/gifs/'.$row['gif']."?time=".rand(),
            			"description" => $row['description'],
            			"tags" => $row['tags'],
            			"location" => $row['location'],
            			"video_language" => $row['video_language'],
            			"video_score" => $row['video_score'],
            			"liked" => $liked_count['count'],
            			"like" => $like_array,
            			"count" =>array
            					(
            					    "like_count" => (string)rand(25,200),//$countLikes_count['count'],
                        			"video_comment_count" => $countcomment_count['count'],
                        			"view" => (string)rand(25,200),//$row['view'],
            					),
    					"sound" =>array
            					(
            					    "id" => $rd12->id,
            					    "audio_path" => 
                            			array(
                                			"mp3" => $MEDIA_path."/audios/".$rd12->id.".mp3",
                    			            "acc" => $MEDIA_path."/audios/".$rd12->id.".aac"
                                		),
                        			"sound_name" => $rd12->sound_name,
                        			"description" => $rd12->description,
                        			"thum" => $rd12->thum,
                        			"section" => $rd12->section,
                        			"created" => $rd12->created,
                        			"duration" => $rd12->duration
            					),
            			"created" => $row['created']
            		);
        			
        		}
		        
		        
		        //count total heart
		            $query123=mysqli_query($conn,"select * from videos where fb_id='".$my_fb_id."' ");
		        
    		        $array_out_count_heart ="";
            		while($row123=mysqli_fetch_array($query123))
            		{
            		  	$array_out_count_heart .=$row123['id'].',';
            		}
            		
            		$array_out_count_heart=$array_out_count_heart.'0';
            		
            		$hear_count=mysqli_query($conn,"SELECT count(*) as count from video_like_dislike where video_id IN($array_out_count_heart) ");
		            $hear_count=mysqli_fetch_assoc($hear_count);
		            
		        //count total heart
		        
		        //count total_fans
		        
		            $total_fans=mysqli_query($conn,"SELECT count(*) as count from follow_users where followed_fb_id='".$fb_id."' ");
		            $total_fans=mysqli_fetch_assoc($total_fans);
		            
		        //count total_fans
		        
		        //count total_following
		        
		            $total_following=mysqli_query($conn,"SELECT count(*) as count from follow_users where fb_id='".$fb_id."' ");
		            $total_following=mysqli_fetch_assoc($total_following);
		            
		        //count total_following
		        
		        
		        $count_video_rows=count($array_out_video);
		        if($count_video_rows=="0")
		        {
		            $array_out_video=array(0);
		        }
		        
		        
                $query2=mysqli_query($conn,"SELECT count(*) as count from follow_users where fb_id='".$my_fb_id."' and followed_fb_id='".$fb_id."' ");
		        $follow_count=mysqli_fetch_assoc($query2);
		        
		       
                if($follow_count['count']=="0")
                {
                    $follow="0";
                    $follow_button_status="Follow";
                }
                else
                if($follow_count['count']!="0")
                {
                    $follow="1";
                    $follow_button_status="Unfollow";
                }
                
                
		        $array_out = array();
		        $array_out[] = 
        			array(
        			"fb_id" => $fb_id,
        			"user_info" =>array
            					(   
            					    "fb_id" => $rd->fb_id,
            					    "first_name" => $rd->first_name,
                        			"last_name" => $rd->last_name,
                        			"profile_pic" => modifyPhoto($rd->profile_pic, $MEDIA_path),
                        			"gender" => $rd->gender,
        	                		"created" => $rd->created,
        	                		"bio" => $rd->bio,
        	                		"dob" => $rd->dob
            					),
        			"follow_Status" =>array
            					(
            					    "follow" => $follow,
                        			"follow_status_button" => $follow_button_status
            					),
        			"total_heart" => $hear_count['count'],
        			"total_fans" => $total_fans['count'],
        			"total_following" => $total_following['count'],
        			"user_videos" => $array_out_video
            	    
        		);
		        
		      
		    } 
		    
		    $output=array( "code" => "200", "msg" => $array_out);
    		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    		
		}
		else
		{
			$array_out = array();
					
			 $array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}
	
	
	function updateVideoView()
	{
	    
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
	
		if(isset($event_json['id']))
		{
			$id=htmlspecialchars(strip_tags($event_json['id'] , ENT_QUOTES));
		       
		    mysqli_query($conn,"update videos SET view =view+1 WHERE id ='".$id."' ");
    		
    		$array_out[] = 
				array(
				"response" =>"success");
				
		    $output=array( "code" => "200", "msg" => $array_out);
    		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    		
		}
		else
		{
			$array_out = array();
					
			$array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	    
	}
	

	
	function updateVideoScore()
	{
	    
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
//		print_r($event_json);

		if(isset($event_json['id']) && isset($event_json['video_score']))
		{
			$id=htmlspecialchars(strip_tags($event_json['id'] , ENT_QUOTES));
			$score=htmlspecialchars(strip_tags($event_json['video_score'] , ENT_QUOTES));
		       
		    mysqli_query($conn,"update videos SET video_score='".$score."' WHERE id ='".$id."' ");
    		
    		$array_out[] = 
				array(
				"response" =>"success");
				
		    $output=array( "code" => "200", "msg" => $array_out);
    		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    		
		}
		else
		{
			$array_out = array();
					
			$array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	    
	}	
	
	
	function likeDislikeVideo()
	{
	    require_once("config.php");
		$input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
		
		if(isset($event_json['fb_id']) && isset($event_json['video_id']) && isset($event_json['like_id']) )
		{   
		    $fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
		    $video_id=htmlspecialchars(strip_tags($event_json['video_id'] , ENT_QUOTES));
		    $like_id=htmlspecialchars(strip_tags($event_json['like_id'] , ENT_QUOTES));
		    $action=htmlspecialchars(strip_tags($event_json['action'] , ENT_QUOTES));
		   
		    if($action=="0")
		    {
		        mysqli_query($conn,"Delete from video_like_dislike where video_id ='".$video_id."' ");
	    
        	    $array_out = array();
        					
        			 $array_out[] = 
        				array(
        				"response" =>"video unlike");
        	        
            	$output=array( "code" => "200", "msg" => $array_out);
        		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		    }
		    else
		    {
		        $qrry_1="insert into video_like_dislike(video_id,fb_id,action,like_id)values(";
    			$qrry_1.="'".$video_id."',";
    			$qrry_1.="'".$fb_id."',";
    			$qrry_1.="'".$action."',";
    			$qrry_1.="'".$like_id."'";
    			$qrry_1.=")";
    			if(mysqli_query($conn,$qrry_1))
    			{
    			   $array_out = array();
        			$array_out[] = 
        				array(
        					"response" => "actions success"
        				);
        			
        			$output=array( "code" => "200", "msg" => $array_out);
        			print_r(json_encode($output, JSON_UNESCAPED_SLASHES)); 
        			
        			
        			//push notification
    			        
    			        //fetch user tokon 
    			        
        			        $query1=mysqli_query($conn,"select * from videos where id='".$video_id."' ");
    		                $rd=mysqli_fetch_object($query1);
    		                
    		                $query11=mysqli_query($conn,"select * from users where fb_id='".$rd->fb_id."' ");
    		                $rd1=mysqli_fetch_object($query11);
		                
		                //fetch user tokon 
		                
		                //fetch name of a person who is liking other person video
    		                $query111=mysqli_query($conn,"select * from users where fb_id='".$fb_id."' ");
    		                $rd11=mysqli_fetch_object($query111);
    		                
    		            //fetch name of a person who is liking other person video
		                
        			    $title=$rd11->first_name." Liked Your Video";
        			    $message="You have received 1 more like on your video";
        			    
        			    $notification['to'] = $rd1->tokon;
                        $notification['notification']['title'] = $title;
                        $notification['notification']['body'] = $message;
                        // $notification['notification']['text'] = $sender_details['User']['username'].' has sent you a friend request';
                        $notification['notification']['badge'] = "1";
                        $notification['notification']['sound'] = "default";
                        $notification['notification']['icon'] = "";
                         $notification['notification']['image'] = "";
                        $notification['notification']['type'] = "";
                        $notification['notification']['data'] = "";
        
                        sendPushNotificationToMobileDevice(json_encode($notification));
                        
	                //push notification
    			}
    			else
    			{
    			    $array_out = array();
        			$array_out[] = 
        				array(
        					"response" => "error in liking video"
        				);
        			
        			$output=array( "code" => "201", "msg" => $array_out);
        			print_r(json_encode($output, JSON_UNESCAPED_SLASHES)); 
    			}
    			
    			
    			
    			
		    }
		   	
			
		}
		else
		{
			$array_out = array();
			$array_out[] = 
				array(
					"response" => "json parem missing"
				);
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}
	
    function postComment()
    {
        
        require_once("config.php");
		$input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		
		if(isset($event_json['fb_id']) && isset($event_json['video_id']) )
		{   
		    $fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
		    $video_id=htmlspecialchars(strip_tags($event_json['video_id'] , ENT_QUOTES));
		    $comment=htmlspecialchars(strip_tags($event_json['comment'] , ENT_QUOTES));
		    
		    $query1=mysqli_query($conn,"select * from users where fb_id='".$fb_id."' ");
		    $rd=mysqli_fetch_object($query1);
		        
    	    $qrry_1="insert into video_comment(video_id,fb_id,comments)values(";
			$qrry_1.="'".$video_id."',";
			$qrry_1.="'".$fb_id."',";
			$qrry_1.="'".$comment."'";
			$qrry_1.=")";
			if(mysqli_query($conn,$qrry_1))
			{
			   $array_out = array();
    			$array_out[] = 
    				array(
    					"fb_id" => $fb_id,
    					"video_id" => $video_id,
    					"comments" => $comment,
    					"user_info" =>array
            					(
            					    "first_name" => $rd->first_name,
                        			"last_name" => $rd->last_name,
                        			"profile_pic" => modifyPhoto($rd->profile_pic, $MEDIA_path)
            					),
    				);
    			
    			$output=array( "code" => "200", "msg" => $array_out);
    			print_r(json_encode($output, JSON_UNESCAPED_SLASHES)); 
    			
    			
                //push notification
    			        
			        //fetch user tokon 
			        
    			        $query1=mysqli_query($conn,"select * from videos where id='".$video_id."' ");
		                $rd=mysqli_fetch_object($query1);
		                
		                $query11=mysqli_query($conn,"select * from users where fb_id='".$rd->fb_id."' ");
		                $rd1=mysqli_fetch_object($query11);
	                
	                //fetch user tokon 
	                
	                //fetch name of a person who is liking other person video
		                $query111=mysqli_query($conn,"select * from users where fb_id='".$fb_id."' ");
		                $rd11=mysqli_fetch_object($query111);
		                
		            //fetch name of a person who is liking other person video
	                
    			    $title=$rd11->first_name." Post comment on your video";
    			    $message=$comment;
    			    
    			    $notification['to'] = $rd1->tokon;
                    $notification['notification']['title'] = $title;
                    $notification['notification']['body'] = $message;
                    // $notification['notification']['text'] = $sender_details['User']['username'].' has sent you a friend request';
                    $notification['notification']['badge'] = "1";
                    $notification['notification']['sound'] = "default";
                    $notification['notification']['icon'] = "";
                     $notification['notification']['image'] = "";
                    $notification['notification']['type'] = "";
                    $notification['notification']['data'] = "";
    
                    sendPushNotificationToMobileDevice(json_encode($notification));
                    
                //push notification
			}
			else
			{
			    $array_out = array();
    			$array_out[] = 
    				array(
    					"response" => "error"
    				);
    			
    			$output=array( "code" => "201", "msg" => $array_out);
    			print_r(json_encode($output, JSON_UNESCAPED_SLASHES)); 
			}
			
		}
		else
		{
			$array_out = array();
			$array_out[] = 
				array(
					"response" => "json parem missing"
				);
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
		
    }

    function viewVideo()
	{
	    require_once("config.php");
		$input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
		
		if(isset($event_json['fb_id']) && isset($event_json['video_id']))
		{   
		    $fb_id=$event_json['fb_id'];
		    $video_id=htmlspecialchars(strip_tags($event_json['video_id'] , ENT_QUOTES));

		    $view_in="select * from video_view where fb_id='".$fb_id."' and video_id='".$video_id."'";
			$view_in_rs=mysqli_query($conn,$view_in);
			
			if(mysqli_num_rows($view_in_rs))
			{   
			    $rd=mysqli_fetch_object($view_in_rs);  
			     
				$array_out = array();
				 $array_out[] = 
					//array("code" => "200");
					array(
						"fb_id" => $rd->fb_id,
						"video_id" => $rd->video_id,
					);
				
				$output=array( "code" => "200", "msg" => $array_out);
				print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
			}else
			{
			    
			    $qrry_1="insert into video_view(fb_id,video_id,created)values(";
    			$qrry_1.="'".$fb_id."',";
    			$qrry_1.="'".$video_id."',";
    			$qrry_1.="'".date('Y-m-d H:i:s')."'";
    			$qrry_1.=")";
    			if(mysqli_query($conn,$qrry_1))
    			{
				    $last_insert_fb_id = mysqli_insert_id($conn);
				     
				     $array_out = array();
    				 $array_out[] = 
    					//array("code" => "200");
    					array(
    						"fb_id" => $fb_id,
    						"video_id" => $video_id
    					);
    				
    				$output=array( "code" => "200", "msg" => $array_out);
    				print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    			}
    			else
    			{
    			    //echo mysqli_error();
    			    $array_out = array();
    					
            		 $array_out[] = 
            			array(
            			"response" =>"problem in video view update");
            		
            		$output=array( "code" => "201", "msg" => $array_out);
            		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    			}
			}
		}
		else
		{
			$array_out = array();
			$array_out[] = 
				array(
					"response" => "json parem missing"
				);
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}
    
    function showVideoComments()
	{
	    
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
	
		if(isset($event_json['video_id']))
		{
			$video_id=htmlspecialchars(strip_tags($event_json['video_id'] , ENT_QUOTES));
			
			$query=mysqli_query($conn,"select * from video_comment where video_id='".$video_id."' order by id DESC");
		        
    		$array_out = array();
    		while($row=mysqli_fetch_array($query))
    		{
    		  
    		   $query1=mysqli_query($conn,"select * from users where fb_id='".$row['fb_id']."' ");
		       $rd=mysqli_fetch_object($query1);
		        
    		   $array_out[] = 
        			array(
        			"video_id" => $row['video_id'],
        			"fb_id" => $row['fb_id'],
        			"user_info" =>array
            					(
            					    "first_name" => $rd->first_name,
                        			"last_name" => $rd->last_name,
                        			"profile_pic" => modifyPhoto($rd->profile_pic, $MEDIA_path)
            					),
            	    
        			"comments" => $row['comments'],
        			"created" => $row['created']
        		);
    			
    		}
    		$output=array( "code" => "200", "msg" => $array_out);
    		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    		
		}
		else
		{
			$array_out = array();
					
			 $array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}
	
	
	function allSounds()
	{
	    
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
	
		$query=mysqli_query($conn,"select DISTINCT section from sound ");
	        
		$array_out = array();
		while($row=mysqli_fetch_array($query))
		{
		  
		   //echo $row['section'];
		   //echo "select * from sound where section ='".$row['section']."' ";
		   
		   $query1=mysqli_query($conn,"select * from sound where section ='".$row['section']."' and sound_name!=''");
		   $array_out1 = array();
		   while($row1=mysqli_fetch_array($query1))
		   {
		        $array_out1[] = 
        			array(
            			"id" => $row1['id'],
            			
            			"audio_path" => 
                    			array(
                        			"mp3" => $MEDIA_path."/audios/".$row1['id'].".mp3",
            			            "acc" => $MEDIA_path."/audios/".$row1['id'].".aac"
                        		),
            			"sound_name" => $row1['sound_name'],
            			"description" => $row1['description'],
            			"section" => $row1['section'],
            			"thum" => $row1['thum'],
            			"duration" => $row1['duration'],
            			"created" => $row1['created']
            		);
		    }
		    
		    $array_out2[] = 
    			array(
    			"section_name" => $row['section'],
    			"sections_sounds" => $array_out1
    			
    		);
		   
			
		}
		$output=array( "code" => "200", "msg" => $array_out2);
		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		
	}
	
	
	function fav_sound()
	{
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
		
		if(isset($event_json['fb_id']) && isset($event_json['sound_id']))
		{
			$fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
			$sound_id=htmlspecialchars(strip_tags($event_json['sound_id'] , ENT_QUOTES));
			
			$qrry_1="insert into fav_sound(fb_id,sound_id)values(";
			$qrry_1.="'".$fb_id."',";
			$qrry_1.="'".$sound_id."'";
			$qrry_1.=")";
			if(mysqli_query($conn,$qrry_1))
			{
			 
			
				 $array_out = array();
				 $array_out[] = 
					//array("code" => "200");
					array(
        			"response" =>"successful");
				
				$output=array( "code" => "200", "msg" => $array_out);
				print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
			}
			else
			{
			    //echo mysqli_error();
			    $array_out = array();
					
        		 $array_out[] = 
        			array(
        			"response" =>"problem");
        		
        		$output=array( "code" => "201", "msg" => $array_out);
        		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
			}
			
			
			
		}
		else
		{
			$array_out = array();
					
			 $array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}
	
	function my_FavSound()
	{
	    
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
	    
	    if(isset($event_json['fb_id']))
		{
		    
		   $fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
		    
    	   $query1=mysqli_query($conn,"select * from fav_sound where fb_id ='".$fb_id."' ");
		   $array_out1 = array();
		   while($row1=mysqli_fetch_array($query1))
		   {
		        
		        $qrry="select * from sound WHERE id ='".$row1['sound_id']."' ";
    			$log_in_rs=mysqli_query($conn,$qrry);
    			
    		    $rd=mysqli_fetch_object($log_in_rs);
    			    
    			$array_out1[] = 
        			array(
            			"id" => $row1['id'],
            			
            			"audio_path" => 
                    			array(
                        			"mp3" => $MEDIA_path."/audios/".$row1['sound_id'].".mp3",
            			            "acc" => $MEDIA_path."/audios/".$row1['sound_id'].".aac"
                        		),
            			"sound_name" => $rd->sound_name,
            			"description" => $rd->description,
            			"section" => $rd->section,
            			"thum" => $rd->thum,
            			"duration" => $rd->duration,
            			"created" => $rd->created,
            		);
		    }
		    
		    $output=array( "code" => "200", "msg" => $array_out1);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    		
		}
		else
		{
			$array_out = array();
					
			 $array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
		
	}
	
	
	
	function my_liked_video()
	{
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
	
		if(isset($event_json['fb_id']))
		{
			$fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
			
		    $query1=mysqli_query($conn,"select * from users where fb_id='".$fb_id."' ");
		    $rd=mysqli_fetch_object($query1);
		    if(mysqli_num_rows($query1))
		    {
		        
		        $query=mysqli_query($conn,"select * from video_like_dislike where fb_id='".$fb_id."' order by id DESC");
		        
		        $array_out_video = array();
        		while($row=mysqli_fetch_array($query))
        		{
        		   
        		   $query11=mysqli_query($conn,"select * from videos where id='".$row['video_id']."' ");
		           $rdd=mysqli_fetch_object($query11);
		            
		           $query112=mysqli_query($conn,"select * from sound where id='".$rdd->sound_id."' ");
		           $rd12=mysqli_fetch_object($query112);
		        
        		   $countLikes = mysqli_query($conn,"SELECT count(*) as count from video_like_dislike where video_id='".$row['video_id']."' ");
                   $countLikes_count=mysqli_fetch_assoc($countLikes);
    		        
    		       $countcomment = mysqli_query($conn,"SELECT count(*) as count from video_comment where video_id='".$row['video_id']."' ");
                   $countcomment_count=mysqli_fetch_assoc($countcomment);
                   
                   $liked = mysqli_query($conn,"SELECT count(*) as count, like from video_like_dislike where video_id='".$row['video_id']."' and fb_id='".$fb_id."' ");
                   $liked_count=mysqli_fetch_assoc($liked);

                   $like_array = likeArray($conn, $MEDIA_path, $liked_count, $row['video_id'], $fb_id);
                
        		   $array_out_video[] = 
            			array(
            			"id" => $row['id'],
            			"video" => $MEDIA_path.'/videos/'.$rdd->video,
            			"original_video" => $MEDIA_path.'/videos/'.$rdd->original_video,
            			"thum" => $MEDIA_path.'/thumbnails/'.$rdd->thum,
            			"gif" => $MEDIA_path.'/gifs/'.$rdd->gif,
            			"description" => $rdd->description,
            			"liked" => $liked_count['count'],
            			"like" => $like_array,
            			"count" =>array
            					(
            					    "like_count" => (string)rand(25,200),//$countLikes_count['count'],
                        			"video_comment_count" => $countcomment_count['count'],
                        			"view" => (string)rand(25,200),//$rdd->view,
            					),
    					"sound" =>array
            					(
            					    "id" => $rd12->id,
            					    "audio_path" => 
                            			array(
                                			"mp3" => $MEDIA_path."/audios/".$rd12->id.".mp3",
                    			            "acc" => $MEDIA_path."/audios/".$rd12->id.".aac"
                                		),
                        			"sound_name" => $rd12->sound_name,
                        			"description" => $rd12->description,
                        			"thum" => $rd12->thum,
                        			"section" => $rd12->section,
                        			"created" => $rd12->created,
                        			"duration" => $rd12->duration
            					),
            			"created" => $row['created']
            		);
        			
        		}
		        
		        $count_video_rows=count($array_out_video);
		        if($count_video_rows=="0")
		        {
		            $array_out_video=array(0);
		        }
		        
		        
		        
		        
                
                
		        $array_out = array();
		        $array_out[] = 
        			array(
        			"fb_id" => $fb_id,
        			"user_info" =>array
            					(
            					    "first_name" => $rd->first_name,
                        			"last_name" => $rd->last_name,
                        			"profile_pic" => modifyPhoto($rd->profile_pic, $MEDIA_path),
                        			"gender" => $rd->gender,
                        			"bio" => $rd->bio,
        	                		"created" => $rd->created,
        	                		"gender" => $rd1->gender,
            					),
        			
        			"total_heart" => "100",
        			"total_fans" => "88",
        			"total_following" => "55",
        			"user_videos" => $array_out_video
            	    
        		);
		        
		      
		    } 
		    
		    $output=array( "code" => "200", "msg" => $array_out);
    		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    		
		}
		else
		{
			$array_out = array();
					
			 $array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}


	function my_notifications()
	{
		require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);

		if (isset($event_json['fb_id']))
		{
			$fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
			$array_out = array();

			$q = mysqli_query($conn, "SELECT CONCAT(first_name, ' ', last_name) as name from users where id IN (SELECT fb_id from follow_users where seen = '0' and followed_fb_id = '".$fb_id."')");
			$following_notification = [];
			while ($row = mysqli_fetch_array($q)) {
				$following_notification[] = $row['name'];
			}

			$q = mysqli_query($conn, "SELECT CONCAT(first_name, ' ', last_name) as name from users where id IN (SELECT fb_id from video_comment where seen = '0' and video_id in (SELECT id from videos where fb_id = '".$fb_id."'))");
			$comment_notification = [];
			while ($row = mysqli_fetch_array($q)) {
				$comment_notification[] = $row['name'];
			}

			$q = mysqli_query($conn, "SELECT CONCAT(first_name, ' ', last_name) as name from users where id IN (SELECT fb_id from video_like_dislike where seen = '0' and video_id in (SELECT id from videos where fb_id = '".$fb_id."'))");
			$like_notification = [];
			while ($row = mysqli_fetch_array($q)) {
				$like_notification[] = $row['name'];
			}

			$array_out[] = array(
				"following" => $following_notification,
				"comment" => $comment_notification,
				"like" => $like_notification
			);
	        
		    $output=array( "code" => "200", "msg" => $array_out);
    		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    		
		}
		else
		{
			$array_out = array();
					
			 $array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}	
	
	function get_followers()
	{
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
	
		if(isset($event_json['fb_id']))
		{
			$fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
			
		    $query=mysqli_query($conn,"select * from follow_users where followed_fb_id='".$fb_id."' order by id DESC");
		        
	        $array_out = array();
    		while($row=mysqli_fetch_array($query))
    		{
    		   
    		   $query11=mysqli_query($conn,"select * from users where fb_id='".$row['fb_id']."' ");
		       $rd1=mysqli_fetch_object($query11);
		       
		       
                $query2=mysqli_query($conn,"SELECT count(*) as count from follow_users where followed_fb_id='".$row['fb_id']."' and fb_id='".$fb_id."'  ");
                $follow_count=mysqli_fetch_assoc($query2);
                
                if($follow_count['count']=="0")
                {
                	$follow="0";
                	$follow_button_status="Follow";
                }
                else
                if($follow_count['count']!="0")
                {
                	$follow="1";
                	$follow_button_status="Unfollow";
                }
                

    		   $array_out[] = 
        			array(
        			    "fb_id" => $rd1->fb_id,
					    "username" => $rd1->username,
					    "first_name" => $rd1->first_name,
					    "last_name" => $rd1->last_name,
					    "gender" => $rd1->gender,
					    "bio" => $rd1->bio,
					    "dob" => $rd1->dob,
					    "profile_pic" => modifyPhoto($rd1->profile_pic, $MEDIA_path),
					    "created" => $rd1->created,
					    "follow_Status" =>array
                		(
                			"follow" => $follow,
                			"follow_status_button" => $follow_button_status
                		)
        		);
    			
    		}
	        
	        
		    $output=array( "code" => "200", "msg" => $array_out);
    		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    		
		}
		else
		{
			$array_out = array();
					
			 $array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}
	
	
	function get_followings()
	{
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
	
		if(isset($event_json['fb_id']))
		{
			$fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
			
		    $query=mysqli_query($conn,"select * from follow_users where fb_id='".$fb_id."' order by id DESC");
		        
	        $array_out = array();
    		while($row=mysqli_fetch_array($query))
    		{
    		   
    		   $query11=mysqli_query($conn,"select * from users where fb_id='".$row['followed_fb_id']."' ");
		       $rd1=mysqli_fetch_object($query11);
		       
		       $query1=mysqli_query($conn,"select * from users where fb_id='".$row['fb_id']."' ");
		       $rd=mysqli_fetch_object($query1);
		        
		        
                $query2=mysqli_query($conn,"SELECT count(*) as count from follow_users where fb_id='".$fb_id."' and followed_fb_id='".$row['followed_fb_id']."' ");
                $follow_count=mysqli_fetch_assoc($query2);
                
                if($follow_count['count']=="0")
                {
                	$follow="0";
                	$follow_button_status="Follow";
                }
                else
                if($follow_count['count']!="0")
                {
                	$follow="1";
                	$follow_button_status="Unfollow";
                }
                

    		   $array_out[] = 
        			array(
        			    "fb_id" => $rd1->fb_id,
					    "username" => $rd1->username,
					    "first_name" => $rd1->first_name,
					    "last_name" => $rd1->last_name,
					    "gender" => $rd1->gender,
					    "bio" => $rd1->bio,
					    "profile_pic" => modifyPhoto($rd1->profile_pic, $MEDIA_path),
					    "created" => $rd1->created,
					    "dob" => $rd1->dob,
					    "follow_Status" =>array
                		(
                			"follow" => $follow,
                			"follow_status_button" => $follow_button_status
                		)
        		);
    			
    		}
	        
	        
		    $output=array( "code" => "200", "msg" => $array_out);
    		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    		
		}
		else
		{
			$array_out = array();
					
			 $array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}
	
	
	
	function discover()
	{
	    
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
		
		$fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
	
		$query=mysqli_query($conn,"select DISTINCT section from videos where section!='0' order by video_score DESC ");
	        
		$array_out = array();
		while($row=mysqli_fetch_array($query))
		{
		  
		   //echo $row['section'];
		   //echo "select * from sound where section ='".$row['section']."' ";
		   
		   $query1=mysqli_query($conn,"select * from videos where section ='".$row['section']."' order by video_score DESC ");
		   $array_out1 = array();
		   while($row1=mysqli_fetch_array($query1))
		   {    
		        $query11=mysqli_query($conn,"select * from users where fb_id='".$row1['fb_id']."' ");
		        $rd1=mysqli_fetch_object($query11);
		        
		        $query112=mysqli_query($conn,"select * from sound where id='".$row1['sound_id']."' ");
		        $rd12=mysqli_fetch_object($query112);
		        
		        $countLikes = mysqli_query($conn,"SELECT count(*) as count from video_like_dislike where video_id='".$row1['id']."' ");
                $countLikes_count=mysqli_fetch_assoc($countLikes);
		        
		        $countcomment = mysqli_query($conn,"SELECT count(*) as count from video_comment where video_id='".$row1['id']."' ");
                $countcomment_count=mysqli_fetch_assoc($countcomment);
               
		        $liked = mysqli_query($conn,"SELECT count(*) as count from video_like_dislike where video_id='".$row1['id']."' and fb_id='".$fb_id."' ");
                $liked_count=mysqli_fetch_assoc($liked);

                $like_array = likeArray($conn, $MEDIA_path, $liked_count, $row1['id'], $fb_id);
                   
		        $array_out1[] = 
        			array(
            			"id" => $row1['id'],
            			"video" => $MEDIA_path.'/videos/'.$row1['video'],
            			"original_video" => $MEDIA_path.'/videos/'.$row1['original_video'],
            			"thum" => $MEDIA_path.'/thumbnails/'.$row1['thum'],
            			"gif" => $MEDIA_path.'/gifs/'.$row1['gif'],
            			"description" => $row1['description'],
            			"liked" => $liked_count['count'],
            			"like" => $like_array,
            			"count" =>array
            					(
            					    "like_count" => (string)rand(25,200),//$countLikes_count['count'],
                        			"video_comment_count" => $countcomment_count['count'],
                        			"view" => (string)rand(25,200),//$row1['view'],
            					),
            			"user_info" =>array
            					(
            					    "fb_id" => $rd1->fb_id,
            					    "first_name" => $rd1->first_name,
                        			"last_name" => $rd1->last_name,
                        			"profile_pic" => modifyPhoto($rd1->profile_pic, $MEDIA_path),
                        			"gender" => $rd1->gender,
                        			"bio" => $rd1->bio,
                        			"dob" => $rd1->dob,
        	                		"created" => $rd1->created,
            					),
    					"sound" =>array
            					(
            					    "id" => $rd12->id,
            					    "audio_path" => 
                            			array(
                                			"mp3" => $MEDIA_path."/audios/".$rd12->id.".mp3",
                    			            "acc" => $MEDIA_path."/audios/".$rd12->id.".aac"
                                		),
                        			"sound_name" => $rd12->sound_name,
                        			"description" => $rd12->description,
                        			"thum" => $rd12->thum,
                        			"section" => $rd12->section,
                        			"created" => $rd12->created,
                        			"duration" => $rd12->duration
            					),
            			"created" => $row1['created']
            		);
		    }
		    
		    $array_out2[] = 
    			array(
    			"section_name" => $row['section'],
    			"sections_videos" => $array_out1
    			
    		);
		   
			
		}
		$output=array( "code" => "200", "msg" => $array_out2);
		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		
	}

	function UpdateProfilePhoto()
	{
	    require_once("config.php");
		$input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		
		if(isset($event_json['fb_id']) && isset($event_json['picbase64']))
		{   
		    $fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
		    $thum = $event_json['picbase64']['file_data'];
            
            $fileName=rand()."_".rand();
			$thum_url="photos/".$fileName.".jpg";
			
			/*list($type, $data) = explode(',', $data);
			list(, $data)      = explode(',', $data);*/
			$thum = base64_decode($thum);
			
			// file_put_contents("upload/thum/".$fileName.".jpg", $thum);
			uploadToS3("photos/".$fileName.".jpg", $thum);
			
			/*picture resize*/
				// File and new size
				$filename = 'photos/'.$fileName.'.jpg';
				$newfilename='photos/'.$fileName.'.jpg';
				$percent = 0.4;
				
				// Get new sizes
				list($width, $height) = getimagesize($filename);
				$newwidth = $width * $percent;
				$newheight = $height * $percent;
				// Load
				$thumb = imagecreatetruecolor($newwidth, $newheight);
				$source = imagecreatefromjpeg($filename);
				// Resize
				$res=imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
				// Output
				imagejpeg($thumb,$newfilename);
			/*picture resize*/

			$url = "photos/".$fileName.".jpg";

			$qrry_1="update users SET profile_pic ='".$url."' WHERE fb_id ='".$fb_id."' ";
			if(mysqli_query($conn,$qrry_1))
			{
			    $array_out = array();
				 
				$qrry_1="select * from users WHERE fb_id ='".$fb_id."' ";
    			$log_in_rs=mysqli_query($conn,$qrry_1);
    			
    			if(mysqli_num_rows($log_in_rs))
    			{   
    			    
                    
    			    $rd=mysqli_fetch_object($log_in_rs);
    			    
    			       $array_out = array();
    					
            		 $array_out[] = 
            			array(
            			"first_name" => $rd->first_name,
            			"username" => $rd->username,
            			"last_name" => $rd->last_name,
            			"gender" => $rd->gender,
            			"bio" =>  $rd->bio,
            			"dob" => $rd1->dob,
            			"profile_pic"=> modifyPhoto($rd->profile_pic, $MEDIA_path)
            			);
            		
            		$output=array( "code" => "200", "msg" => $array_out);
            		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    			}
			
        		
			}
			else
			{
			    $array_out = array();
					
        		$array_out[] = 
        			array(
        			"response" =>"Problem in uploading photo");
        		
        		$output=array( "code" => "201", "msg" => $array_out);
        		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
			}		
		}
		else
		{
			$array_out = array();
			$array_out[] = 
				array(
					"response" => "json parem missing"
				);
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	
	}
	
	
	function edit_profile()
	{
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
		//0= owner  1= company 2= ind mechanic
		
		if(isset($event_json['fb_id']) && isset($event_json['first_name']) && isset($event_json['last_name']) && isset($event_json['gender']) && isset($event_json['bio']) && isset($event_json['picbase64']) )
		{
			$fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
			$first_name=htmlspecialchars(strip_tags($event_json['first_name'] , ENT_QUOTES));
			$last_name=htmlspecialchars(strip_tags($event_json['last_name'] , ENT_QUOTES));
			$gender=htmlspecialchars(strip_tags($event_json['gender'] , ENT_QUOTES));
		    $bio=htmlspecialchars(strip_tags($event_json['bio'] , ENT_QUOTES));
			$username=htmlspecialchars(strip_tags($event_json['username'] , ENT_QUOTES));

			$dob=htmlspecialchars(strip_tags($event_json['dob'] , ENT_QUOTES));

			if(isset($event_json['picbase64'])){
				$thum = $event_json['picbase64'];
            
	            $fileName=rand()."_".rand();
				$thum_url="photos/".$fileName.".jpg";
				
				/*list($type, $data) = explode(',', $data);
				list(, $data)      = explode(',', $data);*/
				$thum = base64_decode($thum);
				
				// file_put_contents("upload/thum/".$fileName.".jpg", $thum);
				uploadToS3("photos/".$fileName.".jpg", $thum);
				
				/*picture resize*/
					// File and new size
					$filename = 'photos/'.$fileName.'.jpg';
					$newfilename='photos/'.$fileName.'.jpg';
					$percent = 0.4;
					
					// Get new sizes
					list($width, $height) = getimagesize($filename);
					$newwidth = $width * $percent;
					$newheight = $height * $percent;
					// Load
					$thumb = imagecreatetruecolor($newwidth, $newheight);
					$source = imagecreatefromjpeg($filename);
					// Resize
					$res=imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
					// Output
					imagejpeg($thumb,$newfilename);
				/*picture resize*/

				$url = $fileName.".jpg";
				$qrry_1="update users SET first_name ='".$first_name."' , last_name ='".$last_name."', dob ='".$dob."', gender ='".$gender."' , bio ='".$bio."' , profile_pic ='".$url."'  WHERE fb_id ='".$fb_id."' ";

			}else{
				$qrry_1="update users SET first_name ='".$first_name."' , last_name ='".$last_name."', dob ='".$dob."', gender ='".$gender."' , bio ='".$bio."'  WHERE fb_id ='".$fb_id."' ";
			}
			if(mysqli_query($conn,$qrry_1))
			{
			    $array_out = array();
				 
				$qrry_1="select * from users WHERE fb_id ='".$fb_id."' ";
    			$log_in_rs=mysqli_query($conn,$qrry_1);
    			
    			if(mysqli_num_rows($log_in_rs))
    			{   
    			    
                    
    			    $rd=mysqli_fetch_object($log_in_rs);
    			    $q1 = mysqli_query($conn,"select * from super_user where user_id='".$row['id']."' ");
    			    $super_user = false;
    			    if (mysqli_num_rows($q1)) {
    			    	$super_user = true;
    			    }
    			    
    			       $array_out = array();
    					
            		 $array_out[] = 
            			array(
            			"first_name" => $rd->first_name,
            			"username" => $rd->username,
            			"last_name" => $rd->last_name,
            			"email" => $rd->email,
            			"gender" => $rd->gender,
            			"bio" => $rd->bio,
            			"dob" => $rd->dob,
            			"birth_time" => $rd->birth_time,
            			"location" => $rd->location,
            			"profile_pic"=> modifyPhoto($rd->profile_pic, $MEDIA_path),
            			"superuser" => $super_user
            			);
            		
            		$output=array( "code" => "200", "msg" => $array_out);
            		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    			}
			
        		
			}
			else
			{
			    $array_out = array();
					
        		$array_out[] = 
        			array(
        			"response" =>"problem in updating");
        		
        		$output=array( "code" => "201", "msg" => $array_out);
        		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
			}
			
		}
		else
		{
			$array_out = array();
					
			 $array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	    
	}
	
	
	function follow_users()
	{
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
		//0= owner  1= company 2= ind mechanic
		
		
		if(isset($event_json['fb_id']) && isset($event_json['followed_fb_id']) && isset($event_json['status']))
		{
			$fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
			$followed_fb_id=htmlspecialchars(strip_tags($event_json['followed_fb_id'] , ENT_QUOTES));
			$status=htmlspecialchars(strip_tags($event_json['status'] , ENT_QUOTES));
			
			if($status=="0")
			{
			    mysqli_query($conn,"Delete from follow_users where fb_id ='".$fb_id."' and followed_fb_id='".$followed_fb_id."'  ");
			    
			    $array_out = array();
    				 $array_out[] = 
    					//array("code" => "200");
    					array(
            			"response" =>"unfollow");
    				
    				$output=array( "code" => "200", "msg" => $array_out);
    				print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
			}
			else
			{
			    $qrry_1="insert into follow_users(fb_id,followed_fb_id)values(";
    			$qrry_1.="'".$fb_id."',";
    			$qrry_1.="'".$followed_fb_id."'";
    			$qrry_1.=")";
    			if(mysqli_query($conn,$qrry_1))
    			{
    			 
    			
    				 $array_out = array();
    				 $array_out[] = 
    					//array("code" => "200");
    					array(
            			"response" =>"follow successful");
    				
    				$output=array( "code" => "200", "msg" => $array_out);
    				print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    			}
    			else
    			{
    			    //echo mysqli_error();
    			    $array_out = array();
    					
            		 $array_out[] = 
            			array(
            			"response" =>"problem in signup");
            		
            		$output=array( "code" => "201", "msg" => $array_out);
            		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    			}
			}
			
			
			
		}
		else
		{
			$array_out = array();
					
			 $array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}

	function createLog()
	{
	   	require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
	    $array_out = [];

	    if (isset($event_json['fb_id']) && isset($event_json['called_by']) && isset($event_json['called_to']) && isset($event_json['duration']))
	    {
	    	$q = "SELECT * from call_logs where called_to = '".$event_json['called_to']."' and called_by = '".$event_json['called_by']."' and called_at = '".$event_json['called_at']."'";
	    	$res = mysqli_query($conn, $q);
	    	if (mysqli_num_rows($res) == 0) {
	    		$q = "INSERT into call_logs (called_to, called_by, called_at, duration) values(";
	    		$q .= "'".$event_json['called_to']."',";
	    		$q .= "'".$event_json['called_by']."',";
	    		$q .= "'".$event_json['called_at']."',";
	    		$q .= "'".$event_json['duration']."'";
	    		$q .= ")";

	    		if (mysqli_query($conn, $q)) {
	    			$array_out[] = [
	    				'response' => 'successful'
	    			];
    				$output=array( "code" => "200", "msg" => $array_out);
    				print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
	    		}
	    		else {
	    			$array_out[] = [
	    				'response' => 'some error ocurred'
	    			];
    				$output=array( "code" => "201", "msg" => $array_out);
    				print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
	    		}
	    	}
	    }
	    else {
	    	$array_out = array();

	    	$array_out[] = 
	    	array(
	    		"response" => "Json Parem are missing");

	    	$output=array( "code" => "201", "msg" => $array_out);
	    	print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
	    }
	}
	
	
	function getLogs()
	{
	   	require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
	    $array_out = [];

	    if (isset($event_json['fb_id']) && isset($event_json['called_by']))
	    {
	    	$q = "SELECT * from call_logs where called_by = '".$event_json['called_by']."'";
	    	$res = mysqli_query($conn, $q);
	    	if (mysqli_num_rows($res)) {
	    		$q1 = mysqli_query($conn, "SELECT CONCAT(first_name, ' ', last_name) as name from users where fb_id = '".$event_json['called_by']."'");
	    		$r1 = mysqli_fetch_object($q1);
	    		$called_by_name = $r1->name;

	    		while ($data = mysqli_fetch_array($res)) {
	    			$q1 = mysqli_query($conn, "SELECT CONCAT(first_name, ' ', last_name) as name from users where fb_id = '".$data['called_to']."'");
	    			$r1 = mysqli_fetch_object($q1);
	    			$called_to_name = $r1->name;
	    			$array_out[] = [
	    				"called_by" => $called_by_name,
	    				"called_to" => $called_to_name,
	    				"called_at" => $data['called_at'],
	    				"duration" => $data['duration']
	    			];
	    		}

	    		$output=array( "code" => "200", "msg" => $array_out);
	    		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
	    	}
	    	else {
	    		$array_out[] = [
	    			'response' => 'No logs found'
	    		];
	    		$output=array( "code" => "404", "msg" => $array_out);
	    		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
	    	}
	    }
	    else {
	    	$array_out = array();

	    	$array_out[] = 
	    	array(
	    		"response" => "Json Parem are missing");

	    	$output=array( "code" => "201", "msg" => $array_out);
	    	print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
	    }
	}
	
	
	function offer_services()
	{
	   	require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
	    $array_out = [];

	    if (isset($event_json['fb_id']))
	    {
	    	$q1 = mysqli_query($conn, "SELECT CONCAT(first_name, ' ', last_name) as name, fb_id from users order by id desc");

	    	while ($data = mysqli_fetch_array($q1)) {
	    		$array_out[] = [
	    			"name" => $data['name'],
	    			"fb_id" => $data['fb_id']
	    		];
	    	}

	    	$output=array( "code" => "200", "msg" => $array_out);
	    	print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
	    }
	    else {
	    	$array_out = array();

	    	$array_out[] = 
	    	array(
	    		"response" => "Json Parem are missing");

	    	$output=array( "code" => "201", "msg" => $array_out);
	    	print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
	    }
	}


	//admin panel services
	
	function Admin_Login()
	{   
	   	require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
		//0= owner  1= company 2= ind mechanic
		
		if(isset($event_json['email']) && isset($event_json['password']) )
		{
			$email=htmlspecialchars(strip_tags($event_json['email'] , ENT_QUOTES));
			$password=strip_tags($event_json['password']);
		
			
			$log_in="select * from admin where email='".$email."' and pass='".md5($password)."' ";
			$log_in_rs=mysqli_query($conn,$log_in);
			
			if(mysqli_num_rows($log_in_rs))
			{
				$array_out = array();
				 $array_out[] = 
					//array("code" => "200");
					array(
						"response" => "login success"
					);
				
				$output=array( "code" => "200", "msg" => $array_out);
				print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
			}	
			else
			{
			    
    			$array_out = array();
    					
        		 $array_out[] = 
        			array(
        			"response" =>"Error in login");
        		
        		$output=array( "code" => "201", "msg" => $array_out);
        		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
			}
			
			
			
		}
		else
		{
			$array_out = array();
					
			 $array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}
	
	
	
	function All_Users()
	{
	    
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		
		$query=mysqli_query($conn,"select * from users order by id DESC");
		        
		$array_out = array();
		while($row=mysqli_fetch_array($query))
		{
		     
		   	 $array_out[] = 
				array(
					"fb_id" => $row['fb_id'],
					"username" => $row['username'],
					"first_name" => $row['first_name'],
					"last_name" => $row['last_name'],
					"gender" => $row['gender'],
					"profile_pic" => modifyPhoto($row['profile_pic'], $MEDIA_path),
					"block" => $row['block'],
					"version" => $row['version'],
					"device" => $row['device'],
					"signup_type" => $row['signup_type'],
					"created" => $row['created']
					
				);
			
		}
		$output=array( "code" => "200", "msg" => $array_out);
		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		
	    
	}
	
	
	
	function admin_all_sounds()
	{
	    
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		
		$query=mysqli_query($conn,"select * from sound order by id DESC");
		        
		$array_out = array();
		while($row=mysqli_fetch_array($query))
		{
		     
		   	 $array_out[] = 
				array(
					"id" => $row['id'],
					"sound_name" => $row['sound_name'],
					"description" => $row['description'],
					"thum" => $row['thum'],
					"section" => $row['section'],
					"duration" => $row['duration'],
					"created" => $row['created']
					
				);
			
		}
		$output=array( "code" => "200", "msg" => $array_out);
		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		
	    
	}
	
	function admin_uploadSound()
	{
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
		//0= owner  1= company 2= ind mechanic
		
		if(isset($event_json['fileUrl']))
		{
			$fileUrl=htmlspecialchars(strip_tags($event_json['fileUrl'] , ENT_QUOTES));
			$sound_name=htmlspecialchars(strip_tags($event_json['sound_name'] , ENT_QUOTES));
			$description=htmlspecialchars(strip_tags($event_json['description'] , ENT_QUOTES));
			$section_name=htmlspecialchars(strip_tags($event_json['section_name'] , ENT_QUOTES));
			
			$qrry_1="insert into sound(sound_name,description,section)values(";
			$qrry_1.="'".$sound_name."',";
			$qrry_1.="'".$description."',";
			$qrry_1.="'".$section_name."'";
			$qrry_1.=")";
			if(mysqli_query($conn,$qrry_1))
			{
			     $insert_id=mysqli_insert_id($conn);
			     // @copy($fileUrl,'upload/audio/'.$insert_id.'.aac');
			     uploadToS3('audios/'.$insert_id.'.aac', $fileUrl);

				 $array_out = array();
				 $array_out[] = 
					//array("code" => "200");
					array(
        			"response" =>"successful");
				
				$output=array( "code" => "200", "msg" => $array_out);
				print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
			}
			else
			{
			    //echo mysqli_error();
			    $array_out = array();
					
        		 $array_out[] = 
        			array(
        			"response" =>"problem in signup");
        		
        		$output=array( "code" => "201", "msg" => $array_out);
        		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
			}
			
			
			
			

			
			
			
		}
		else
		{
			$array_out = array();
					
			 $array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	}
	
	
	function admin_getSoundSection()
	{
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		
		$query=mysqli_query($conn,"select * from sound_section");
		        
		$array_out = array();
		while($row=mysqli_fetch_array($query))
		{
		     
		   	 $array_out[] = 
				array(
					"id" => $row['id'],
					"section_name" => $row['section_name'],
					"created" => $row['created']
				);
			
		}
		$output=array( "code" => "200", "msg" => $array_out);
		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
	}
	
	
	function admin_show_allVideos()
	{
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
	
		$query=mysqli_query($conn,"select * from videos order by video_score DESC");

		$array_out = array();
		while($row=mysqli_fetch_array($query))
		{
		    $query1=mysqli_query($conn,"select * from users where fb_id='".$row['fb_id']."' ");
	        $rd=mysqli_fetch_object($query1);
	       
	        $query112=mysqli_query($conn,"select * from sound where id='".$row['sound_id']."' ");
	        $rd12=mysqli_fetch_object($query112);
	        
	        $countLikes = mysqli_query($conn,"SELECT count(*) as count from video_like_dislike where video_id='".$row['id']."' ");
            $countLikes_count=mysqli_fetch_assoc($countLikes);
	        
	        $countcomment = mysqli_query($conn,"SELECT count(*) as count from video_comment where video_id='".$row['id']."' ");
            $countcomment_count=mysqli_fetch_assoc($countcomment);
            
            
            $liked = mysqli_query($conn,"SELECT count(*) as count from video_like_dislike where video_id='".$row['id']."' and fb_id='".$row['fb_id']."' ");
            $liked_count=mysqli_fetch_assoc($liked);

            $like_array = likeArray($conn, $MEDIA_path, $liked_count, $row['id'], $fb_id);
	        
    	   	$array_out[] = 
    			array(
    			"id" => $row['id'],
    			"fb_id" => $row['fb_id'],
    			"user_info" =>array
        					(
        					    "first_name" => $rd->first_name,
        					    "username" => $rd->username,
                    			"last_name" => $rd->last_name,
                    			"profile_pic" => modifyPhoto($rd->profile_pic, $MEDIA_path)
        					),
        		"count" =>array
        					(
        					    "like_count" => (string)rand(25,200),//$countLikes_count['count'],
                    			"video_comment_count" => $countcomment_count['count']
        					),
        		"liked"=> $liked_count['count'],
        		"like" => $like_array,			
        	    "video" => $MEDIA_path.'/videos/'.$row['video'],
        	    "original_video" => $MEDIA_path.'/videos/'.$row['original_video'],
    			"thum" => $MEDIA_path.'/videos/'.$row['thum'],
    			"gif" => $MEDIA_path.'/videos/'.$row['gif'],
    			"section" => $row['section'],
    			"sound" =>array
        					(
        					    "id" => $rd12->id,
        					    "audio_path" => 
                        			array(
                            			"mp3" => $MEDIA_path."/audios/".$rd12->id.".mp3",
                			            "acc" => $MEDIA_path."/audios/".$rd12->id.".aac"
                            		),
                    			"sound_name" => $rd12->sound_name,
                    			"description" => $rd12->description,
                    			"thum" => $rd12->thum,
                    			"section" => $rd12->section,
                    			"created" => $rd12->created,
                    			"duration" => $rd12->duration
        					),
        		"approved" => $row['approved'],
    			"created" => $row['created']
    		);
			
		}
		$output=array( "code" => "200", "msg" => $array_out);
		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
	}
	
	
	function get_user_data()
	{
	   
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		//print_r($event_json);
	    
	    $fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
		$query=mysqli_query($conn,"select * from users where fb_id='".$fb_id."' ");
	   
		$array_out = array();
		while($row=mysqli_fetch_array($query))
		{

		    $q1 = mysqli_query($conn,"select * from super_user where user_id='".$row['id']."' ");
		    $super_user = false;
		    if (mysqli_num_rows($q1)) {
		    	$super_user = true;
		    	$super_user_details = mysqli_fetch_assoc($q1);
		    	
		    	$q1 = mysqli_query($conn, "select skill_id from superuser_skills where super_user_id = '".$super_user_details['id']."'");
		    	$super_user_details['skills'] = [];
		    	while ($r = mysqli_fetch_array($q1)) {
		    		$super_user_details['skills'][] = $r[0];
		    	}

		    	if ($super_user_details['skills']) {
		    		$q = mysqli_query($conn, "SELECT GROUP_CONCAT(`skill`) as skills from `skills` where `id` in (".implode(', ', $super_user_details['skills']).")");
		    		$super_user_details['skills'] = mysqli_fetch_assoc($q)['skills'];
		    	}

		    	$q1 = mysqli_query($conn, "select expertise_id from superuser_expertise where super_user_id = '".$super_user_details['id']."'");
		    	$super_user_details['expertise'] = [];
		    	while ($r = mysqli_fetch_array($q1)) {
		    		$super_user_details['expertise'][] = $r[0];
		    	}

		    	if ($super_user_details['expertise']) {
		    		$q = mysqli_query($conn, "SELECT GROUP_CONCAT(`field`) as fields from `expertise` where `id` in (".implode(', ', $super_user_details['expertise']).")");
		    		$super_user_details['expertise'] = mysqli_fetch_assoc($q)['fields'];
		    	}
		    }
 
		    $array_out[] = 
    			array(
    			"fb_id" => $row['fb_id'],
    			"username" => $row['username'],
    			"first_name"=> $row['first_name'],			
        	    	"last_name" => $row['last_name'],
        	    	"email" => $row['email'],
    			"gender" => $row['gender'],
    			"bio" => $row['bio'],
    			"profile_pic" => modifyPhoto($row['profile_pic'], $MEDIA_path),
    			"created" => $row['created'],
    			"dob" => $row['dob'],
    			"birth_time" => $row['birth_time'],
    			"location" => $row['location'],
    			"superuser" => $super_user,
    			"other_details" => $super_user_details
    		);
			
		}
		$output=array( "code" => "200", "msg" => $array_out);
		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
	}

	function ReportUser()
    {
        
        require_once("config.php");
		$input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		
		if(isset($event_json['reporter_fb_id']) && isset($event_json['reported_fb_id']) )
		{   
		    $qrry_1="insert into report_user(reporter_fb_id,reported_fb_id)values(";
			$qrry_1.="'".$event_json['reporter_fb_id']."',";
			$qrry_1.="'".$event_json['reported_fb_id']."'";
			$qrry_1.=")";
			if(mysqli_query($conn,$qrry_1))
			{
			   	$array_out = array();
    			$output=array( "code" => "200", "msg" => $array_out);
    			print_r(json_encode($output, JSON_UNESCAPED_SLASHES)); 
			}
			else
			{
			    $array_out = array();
    			$array_out[] = 
    				array(
    					"response" => "error"
    				);
    			
    			$output=array( "code" => "201", "msg" => $array_out);
    			print_r(json_encode($output, JSON_UNESCAPED_SLASHES)); 
			}
			
		}
		else
		{
			$array_out = array();
			$array_out[] = 
				array(
					"response" => "json parem missing"
				);
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
		
    }

    function ReportVideo()
    {
        
        require_once("config.php");
		$input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		
		if(isset($event_json['reporter_fb_id']) && isset($event_json['reported_video_id']) )
		{   
		    $qrry_1="insert into report_video(reporter_fb_id,reported_video_id)values(";
			$qrry_1.="'".$event_json['reporter_fb_id']."',";
			$qrry_1.="'".$event_json['reported_video_id']."'";
			$qrry_1.=")";
			if(mysqli_query($conn,$qrry_1))
			{
			   	$array_out = array();
    			$output=array( "code" => "200", "msg" => $array_out);
    			print_r(json_encode($output, JSON_UNESCAPED_SLASHES)); 
			}
			else
			{
			    $array_out = array();
    			$array_out[] = 
    				array(
    					"response" => "error"
    				);
    			
    			$output=array( "code" => "201", "msg" => $array_out);
    			print_r(json_encode($output, JSON_UNESCAPED_SLASHES)); 
			}
			
		}
		else
		{
			$array_out = array();
			$array_out[] = 
				array(
					"response" => "json parem missing"
				);
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
		
    }

    function ReportComment()
    {
        
        require_once("config.php");
		$input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
		
		if(isset($event_json['reporter_fb_id']) && isset($event_json['reported_comment_id']) )
		{   
		    $qrry_1="insert into report_comment(reporter_fb_id,reported_comment_id)values(";
			$qrry_1.="'".$event_json['reporter_fb_id']."',";
			$qrry_1.="'".$event_json['reported_comment_id']."'";
			$qrry_1.=")";
			if(mysqli_query($conn,$qrry_1))
			{
			   	$array_out = array();
    			$output=array( "code" => "200", "msg" => $array_out);
    			print_r(json_encode($output, JSON_UNESCAPED_SLASHES)); 
			}
			else
			{
			    $array_out = array();
    			$array_out[] = 
    				array(
    					"response" => "error"
    				);
    			
    			$output=array( "code" => "201", "msg" => $array_out);
    			print_r(json_encode($output, JSON_UNESCAPED_SLASHES)); 
			}
			
		}
		else
		{
			$array_out = array();
			$array_out[] = 
				array(
					"response" => "json parem missing"
				);
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
		
    }
	
	function changePassword()
	{
	    
	    require_once("config.php");
	    $input = @file_get_contents("php://input");
	    $event_json = json_decode($input,true);
	    
	    //print_r($event_json);
		
		if(isset($event_json['new_password']) && isset($event_json['old_password']))
		{
			$old_password=strip_tags($event_json['old_password']);
			$new_password=strip_tags($event_json['new_password']);
		    
		    die();
		    
		    $qrry_1="select * from admin where pass ='".md5($old_password)."' ";
			$log_in_rs=mysqli_query($conn,$qrry_1);
			$rd=mysqli_fetch_object($log_in_rs);
			
			if($rd->id!="")
			{
			    $qrry_1="update admin SET pass ='".md5($new_password)."' where id='".$rd->id."'  ";
    			if(mysqli_query($conn,$qrry_1))
    			{
    			    $array_out = array();
    					
            		 $array_out[] = 
            			array(
            			"response" =>"success");
            		
            		$output=array( "code" => "200", "msg" => $array_out);
            		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    			}
    			else
    			{
    			    $array_out = array();
    					
            		 $array_out[] = 
            			array(
            			"response" =>"problem in updating");
            		
            		$output=array( "code" => "201", "msg" => $array_out);
            		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
    			}
			}
			else
			{
			    $array_out = array();
					
        		 $array_out[] = 
        			array(
        			"response" =>"problem in updating");
        		
        		$output=array( "code" => "201", "msg" => $array_out);
        		print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
			}
			
			
		}
		else
		{
			$array_out = array();
					
			 $array_out[] = 
				array(
				"response" =>"Json Parem are missing");
			
			$output=array( "code" => "201", "msg" => $array_out);
			print_r(json_encode($output, JSON_UNESCAPED_SLASHES));
		}
	    
	}

	function uploadToS3($location, $file){
		require '../vendor/autoload.php';
		$s3 = new Aws\S3\S3Client([
			'region'  => 'ap-south-1',
			'version' => 'latest',
			'credentials' => [
				'key'    => "AKIAJBD3WHUDMXEPWIBA",
				'secret' => "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",
			]
		]);

		$result = $s3->putObject([
			'Bucket' => 'bless-development',
			'Key'    => $location,
			'Body' => $file,
			'ACL' => 'public-read'		
		]);
	}

	function modifyPhoto($image, $MEDIA_path){
		if(strpos($image, "http") !== false){
			return $image;
		}else{
			return $MEDIA_path.'/photos/'.$image;
		}
	}

	function likeArray($conn, $MEDIA_path, $liked_count, $video_id, $fb_id){
		$like2 = mysqli_query($conn,"SELECT like_id from video_like_dislike where video_id='".$video_id."' and fb_id='".$fb_id."' ");
        $like3=mysqli_fetch_object($like2);
        if($liked_count['count']=="1"){
       		$like1 = mysqli_query($conn,"SELECT id, image from likes where id='".$like3->like_id."'");
       		$like_object=mysqli_fetch_object($like1);
       		$like_array = array(
       			'id'=>$like_object->id,
       			'image'=>$MEDIA_path.'/likes/'.$like_object->image
       		);
        }else{
       		$like_array = (object)array();
        }
        return $like_array;
	}
	
	

?>

