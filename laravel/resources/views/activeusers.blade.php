@include('layout.header')

<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.dataTables.min.css')}}">
<script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
		$('#data1').DataTable();
	});
</script>

<div class="section mini dashboardscreen"><div class="wdth">
	<div class="col15 left">
		@include('layout.leftside')
	</div>
	<div class="col85 right contentside" style="padding:0px 15px;">
<h2 class="title left">All Active Users</h2>
<h2 class="right">
		<a class="button" href="{{route('users')}}" style="text-decoration: none"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
</h2>

@if(sizeof($users) == 0){
<div class="textcenter nothingelse">
	<img src="img/noorder.png" alt="" />
	<h3>No Record Found</h3>
</div>

@else
<table id='data1' class='display' style='width:100%'>
	<thead>
		<tr>
		<th>ID</th>
		<th>Name</th>
		<th>Username</th>
		<th>Videos Watched</th>
		<th>Likes</th>
		<th>Comments</th>
		<th>Created</th>
		</tr>
	</thead>
	<tbody id='myTable_row'>

		@foreach( $users as $user )
		@php
			$videos_watched = App\VideoView::where('fb_id', $user['fb_id'])->where('created', '>=', date("Y-m-d"))->count();
			$likes = App\VideoLike::where('fb_id', $user['fb_id'])->where('created', '>=', date("Y-m-d"))->count();
			$comments = App\VideoComment::where('fb_id', $user['fb_id'])->where('created', '>=', date("Y-m-d"))->count();
		@endphp
		<tr style=" text-align: center;">
			<td>
				{{$user['fb_id']}}
			</td>
			<td style="line-height: 20px;">
				{{$user['first_name']." ".$user['last_name']}}
			</td>
			<td>
				{{$user['username']}}
			</td>
			<td>
				{{$videos_watched}}
			</td>
			<td>
				{{$likes}}
			</td>

			<td>
				{{$comments}}
			</td>
			<td>
				{{$user['created']}}
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
<nav><ul class='pagination pagination-sm' id='myPager'></ul></nav>
@endif

@include('layout.footer')
