@include('layout.header')

<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.dataTables.min.css')}}">
<script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
		$('#data1').DataTable();
	});
</script>

<div class="section mini dashboardscreen"><div class="wdth">
	<div class="col15 left">
		@include('layout.leftside')
	</div>
	<div class="col85 right contentside" style="padding:0px 15px;">
		<h2 class="title left">All Users</h2>
		<h2 class="right">
			<a class="button" href="{{ route('activeusers') }}" style="text-decoration: none"><i class="fa fa-bullseye" aria-hidden="true"></i> Daily Active Users</a>
		</h2>

		@if(sizeof($users) == 0){
		<div class="textcenter nothingelse">
			<img src="img/noorder.png" alt="" />
			<h3>No Record Found</h3>
		</div>

		@else
		<table id='data1' class='display' style='width:100%'>
			<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Username</th>
					<th>Device</th>
					<th>Created</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id='myTable_row'>

				@foreach( $users as $user )
				<tr style=" text-align: center;">
					<td>
						{{$user['fb_id']}}
					</td>
					<td style="line-height: 20px;">
						{{$user['first_name']." ".$user['last_name']}}
					</td>
					<td>
						{{$user['username']}}
					</td>
					<td>
						{{$user['device']}}
					</td>

					<td>
						{{$user['created']}}
					</td>

					<td>
						<i onclick="myFunction('dropdown_<?php echo $user['id']; ?>')" class="fas fa-ellipsis-h" style="cursor: pointer;font-size: 18px;"></i>
						<div id="dropdown_<?php echo $user['id']; ?>" class="w3-dropdown-content w3-bar-block w3-border" style="border: 2px black solid;">
							<span class="w3-bar-item w3-button">
								<form method="get" action="{{route('useractivity')}}">
									<input type="hidden" name="fb_id" value="{{$user['fb_id']}}">
									<button type="submit" style="background-color: #fff; border: none; cursor: pointer;"><b>See Activity</b></button>
								</form>
							</span>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Username</th>
					<th>Device</th>
					<th>Created</th>
					<th>Action</th>
				</tr>
			</tfoot>
		</table>
		<nav><ul class='pagination pagination-sm' id='myPager'></ul></nav>
		@endif
	</div>
</div>

<script>
	function myFunction(data) 
	{
		var x = '#'+data;
		if ($(x).css("display") === "block") {
			$(x).toggle();
		}
		else {
			$('.w3-dropdown-content').hide();
			$(x).toggle();
		}
	}

	$(document).mouseup(function (e) { 
		if ($(e.target).closest(".w3-dropdown-content").length === 0) { 
			$(".w3-dropdown-content").hide(); 
		} 
	});
</script>
@include('layout.footer')
