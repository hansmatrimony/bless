<?php

$sound_baseurl = "http://blessd.in/laravel/upload/";
$sections = App\SoundSection::all();

$secton_form = "";
foreach( $sections as $val ) 
{
    $secton_form.='<option value='.$val['section_name'].'>'.$val['section_name'].'</option>';
}

$categories = App\Category::all();
$hashtags = '';
foreach ($categories as $category) {
    $tags = App\Tag::where('category_id', $category['id'])->get()->toArray();
    $hashtags .= '<optgroup label="'.$category['name'].'">';
    foreach ($tags as $tag) {
        $hashtags .='<option value="#'.$tag['tag'].'">'.$tag['tag'].'</option>';
    }
    $hashtags .= '</optgroup>';
}

?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<style>

    /* Container */
    body{
        margin:0px; 
        padding: 0px;
        font-family: Arial;
    }
    .container {
        display: block;
        width: 600px;
        background: #F9D48A;
        border-radius: 6px;
        line-height: normal;
        margin: 20px auto;
        border: dashed #1F2021;

    }

    .container1{

        display: block;
        width: 600px;
        background: #fafbfd;
        border-radius: 6px;
        line-height: normal;
        margin:20px auto;
        padding: 40px 0px;
    }
    .container2
    {
        display: block;
        width: 600px;
        margin:0px auto;
    }
    .button{
        border: 0px;
        background-color: deepskyblue;
        color: white;
        padding: 5px 15px;
        margin-left: 10px;
    }


    .uploadedBox
    {
        background: #F9D48A;
        border-radius: 6px;
        border: dashed #1F2021;
        padding: 20px;
    }

    .uploadedBox input ,.uploadedBox select
    {
        font-size: 12px; 
        width: 100%; 
        padding: 8px; 
        border: 1px solid #d4d4d4;
        margin-bottom:10px;
    }


</style>

<script>


    function deleteImge(idd)
    {
        document.getElementById(idd).innerHTML = '';
    }
$(document).ready(function(){
        $('#file').change(function(){

            // console.log($('#file')[0].files); 
            var fileCount = $('#file')[0].files.length;

            $("#previewBox").show(); 

            var fd = new FormData();
            var files = $('#file')[0].files[0];
            fd.append('file',files);
            fd.append('_token', '{{csrf_token()}}');

            $.ajax({
                url: '/laravel/tempupload',
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                success: function(response){
                    if(response != 0)
                    {
                        var myarr = response.split(".");
                        $("#previews").append('<div id='+myarr[0]+' class="uploadedBox"><div><audio controls="controls" style="border-radius: 10px; width: 100%;"><source src="upload/'+response+'" type="audio/mp4" /></audio></div><div style="margin-top:20px;"><input type="text" name="title" placeholder="Sound Name" required><input type="text" name="tagss" placeholder="Description" required><input type="text" name="url" value=<?php echo $sound_baseurl; ?>'+response+' required></div><div><select name="section_name" required><option value="">Select Section</option><?php echo $secton_form; ?></select></div><div><select name="hashtags[]" required multiple><option value="">Select Tags</option><?php echo $hashtags; ?></select></div><div style="padding:10px 0; font-size:12px;">'+response+' &nbsp; | &nbsp;<span style="color:red;" onclick=deleteImge("'+myarr[0]+'")>Delete</span></div></div>');

                    }
                    else
                    {
                        alert('error in uploading');
                    }
                },
            });
        });
    });

</script>


<div class="container">

    <label for="file" style="line-height: 75px;">

        <form method="post" action="" enctype="multipart/form-data" id="myform">
            <div class='preview' align="center" style="margin-top: 40px;">
                <img src="/laravel/upload.png" width="100" height="100">
                <h2 style="color: #80808099; font-weight: 300; margin:0px; font-family: Arial;">Upload Images</h2>
            </div>
            <div>
                <input type="file" id="file" name="file" style="display: none;" />
            </div>
        </form>

    </label>    

</div>

<form action="{{route('uploadSound')}}" method="post" id="previewBox" style="display: none;">
    {{csrf_field()}}
    <input type="hidden" name="category" value="<?php echo @$_GET['category'] ?>">
    <div class="container1" id="previews"></div>
    <div class="container2"><input type="submit" value="Submit" style="border: 0;padding: 15px 20px;width: 100%;background:#84D2C0;color: white;font-size: 14px;border-radius: 5px;"></div>
</form>

<br><br><br><br><br><br>

@include('layout.footer')
