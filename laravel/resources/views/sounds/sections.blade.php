@include('layout.header')

<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.dataTables.min.css')}}">
<script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
  $(document).ready(function() {
      $('#data1').DataTable();
  } );
</script>


<div class="section mini dashboardscreen"><div class="wdth">
    <div class="col15 left">
        @include('layout.leftside')
    </div>
    <div class="col85 right contentside" style="padding:0px 15px;">
        <h2 class="title">All Sounds</h2>

        <br>
        <div class="left">
            <a href="/sounds" class="links_sublinks  <?php if($_SERVER['REQUEST_URI'] =="/sounds"){ echo "links_sublinks_active";} ?> ">
                <span>All Sound</span>
            </a>

            <a href="/sections" class="links_sublinks  <?php if($_SERVER['REQUEST_URI'] =="/laravel/sections"){ echo "links_sublinks_active";} ?> " style="margin-left: 22px;">
                <span>All Sections</span>
            </a>

        </div>

        <div class="right" style="padding: 10px 0;">
            <a href="javascript:;" target="_blank">
                <button style="background:  #C82D32; color:  white; padding:  8px 8px; border:  0px; border-radius:  3px;">Add New Section</button>
            </a>
        </div>

        <br><br><br>

        @if(!$sections)
        <div class="textcenter nothingelse">
            <img src="{{asset('assets/img/noorder.png')}}" alt="" />
            <h3>No Record Found</h3>
        </div>

        @else
        <table id='data1' class='display' style='width:100%'>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Section Name</th>
                    <th>Created</th>
                </tr>
            </thead>
            <tbody id='myTable_row'>

                @foreach( $sections as $section )
                <tr style=" text-align: center;">
                    <td>
                        {{$section['id']}}
                    </td>
                    <td style="line-height: 20px;">
                        {{$section['section_name']}}	
                    </td>

                    <td>
                        {{$section['created']}}
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Section Name</th>
                    <th>Created</th>
                </tr>
            </tfoot>
        </table>
        <nav><ul class='pagination pagination-sm' id='myPager'></ul></nav>
        @endif
    </div>
</div>

<script>
    function playsound(data)
    {	
        document.getElementById('preview_play_'+data).innerHTML='<audio controls="controls" style="border-radius: 20px;height: 30px;"><source src="<?php echo '/upload/audio/'; ?>'+data+'.aac" type="audio/mp4" /></audio>';
    }
</script>
