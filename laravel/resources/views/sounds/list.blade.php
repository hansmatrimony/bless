@include('layout.header')

<?php
$sections = App\SoundSection::all();
$section_form = "";
foreach( $sections as $val ) 
{
    $section_form.='<option value="'.$val['section_name'].'">'.$val['section_name'].'</option>';
}

$categories = App\Category::all();
$hashtags = '';
foreach ($categories as $category) {
    $tags = App\Tag::where('category_id', $category['id'])->get()->toArray();
    $hashtags .= '<optgroup label="'.$category['name'].'">';
    foreach ($tags as $tag) {
        $hashtags .='<option value="#'.$tag['tag'].'">'.$tag['tag'].'</option>';
    }
    $hashtags .= '</optgroup>';
}
?>

<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.dataTables.min.css')}}">
<script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
  $(document).ready(function() {
      $('#data1').DataTable();
  } );
</script>


<div class="section mini dashboardscreen"><div class="wdth">
    <div class="col15 left">
        @include('layout.leftside')
    </div>
    <div class="col85 right contentside" style="padding:0px 15px;">
        <h2 class="title">All Sounds

        <div class="right">
            <form id="form">
                <select name="approved" id="approved" onchange="$('#form').submit()">
                    <option value="0">Not Approved</option>
                    <option value="1">Approved</option>
                    <option value="2">Not Good</option>
                </select>
            </form>
        </div>
        </h2>

        <br>
        <div class="left">
            <a href="/laravel/sounds" class="links_sublinks <?php if(preg_match("/sounds/", $_SERVER['REQUEST_URI'])) { echo "links_sublinks_active";} ?> ">
                <span>All Sound</span>
            </a>

            <a href="/laravel/sections" class="links_sublinks <?php if(preg_match("/sections/", $_SERVER['REQUEST_URI'])) { echo "links_sublinks_active";} ?> " style="margin-left: 22px;">
                <span>All Sections</span>
            </a>

        </div>

        <div class="right" style="padding: 10px 0;">
            <a href="/laravel/uploadSound" target="_blank">
                <button style="background:  #C82D32; color:  white; padding:  8px 8px; border:  0px; border-radius:  3px;">Add Sound File</button>
            </a>
        </div>

        <br><br><br>

        @if(!$sounds)
        <div class="textcenter nothingelse">
            <img src="{{asset('assets/img/noorder.png')}}" alt="" />
            <h3>No Record Found</h3>
        </div>

        @else
        <table id='data1' class='display' style='width:100%'>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Sound Preview</th>
                    <th>Sound Name</th>
                    <th>Description</th>
                    <th>Section</th>
                    <th>Tags</th>
                    <th>Duration</th>
                    <th>Created</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody id='myTable_row'>

                @foreach( $sounds as $sound )
                <tr style=" text-align: center;">
                    <td>
                        {{$sound['id']}}
                        <?php
                        if ($sound['approved'] == 1)
                            echo "&nbsp;<i class='far fa-check-circle' id='approve_".$sound['id']."' style='color: green;' title='Approved!' data-approve=1></i>";
                        else
                            echo "&nbsp;<i class='far fa-times-circle' id='approve_".$sound['id']."' style='color: red;' title='Not Approved!' data-approve=0></i>"; 
                        ?>
                    </td>
                    <td id='preview_play_<?php echo $sound['id']; ?>' >
                        <span onclick="playsound('{{$sound['id']}}', '<?php echo $sound['audio_path']['acc']; ?>')"><img src="{{asset('assets/img/play.png')}}" style="width: 30px;"></span>
                    </td>
                    <td style="line-height: 20px;">
                        {{$sound['sound_name']}}	
                    </td>
                    <td>
                        {{$sound['description']}}
                    </td>

                    <td>
                        {{$sound['section']}}
                    </td>

                    <td>
                        {{$sound['tags']}}
                    </td>

                    <td>
                        {{$sound['duration']}}
                    </td>

                    <td>
                        {{$sound['created']}}
                    </td>

                    <td>
                        <i onclick="myFunction('dropdown_<?php echo $sound['id']; ?>')" class="fas fa-ellipsis-h" style="cursor: pointer;font-size: 18px;"></i>
                        <div id="dropdown_<?php echo $sound['id']; ?>" class="w3-dropdown-content w3-bar-block w3-border" style="border: 2px black solid;">
                            <span onclick='approve("<?php echo $sound['id']; ?>")' class="w3-bar-item w3-button" id="approve-menu_<?php echo $sound['id'] ?>">
                                <?php
                        	if ($sound['approved'] == 1)
                                    echo "<i class='fas fa-times'></i>&nbsp;<b>Disapprove</b>";
                                else
                                    echo "<i class='fas fa-check'></i>&nbsp;<b>Approve</b>";
                                ?>
                            </span>
                            <hr>
                            <span onclick='edit("<?php echo $sound['id']; ?>", "{{$sound['sound_name']}}", "{{$sound['description']}}", "{{$sound['section']}}")' class="w3-bar-item w3-button" id="edit-menu_<?php echo $sound['id'] ?>">
                                <i class="fa fa-edit"></i>&nbsp;<b>Edit</b>
                            </span>
                            @if ($sound['good'] == 1)
                            <hr>
                            <span class="w3-bar-item w3-button">
                                <form method="post" action="{{route('markgoodsound')}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" value="{{$sound['id']}}">
                                    <input type="hidden" name="good" value="0">
                                    <button type="submit" style="background-color: #fff; border: none"><b>Not Good</b></button>
                                </form>
                            </span>
                            @endif
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>thum</th>
                    <th>Sound Name</th>
                    <th>Description</th>
                    <th>Section</th>
                    <th>Tags</th>
                    <th>Created</th>
                    <th>Action</th>
                </tr>
            </tfoot>
        </table>
        <nav><ul class='pagination pagination-sm' id='myPager'></ul></nav>
        @endif
    </div>
</div>

<script>
    $(document).ready(function() {
        var val = '<?php if(isset($_GET['approved'])) echo $_GET['approved']; else echo 0; ?>';
        $("#approved").val(val);
    });



    function playsound(data, path)
    {	
        document.getElementById('preview_play_'+data).innerHTML='<audio controls="controls" style="border-radius: 20px;height: 30px;"><source src="'+path+'" type="audio/mp4" /></audio>';
    }

    function myFunction(data) 
    {
        var x = '#'+data;
        if ($(x).css("display") === "block") {
            $(x).toggle();
        }
        else {
            $('.w3-dropdown-content').hide();
            $(x).toggle();
        }
    }

    $(document).mouseup(function (e) { 
        if ($(e.target).closest(".w3-dropdown-content").length === 0) { 
            $(".w3-dropdown-content").hide(); 
        } 
    });

    function edit(id, name, desc, section)
    {
        document.getElementById('PopupParent').style.display = 'block';
        var elem = '<div class="form marginauto "><form method="post" action="{{route('updatesound')}}"><br><br>'+
        '<input type="hidden" name="_token" value="{{csrf_token()}}">'+
        '<p><input type="hidden" name="id" value="'+id+'"></p>'+
        '<p><label>Sound Name</label><input type="text" placeholder="Sound Name" name="sound_name" value="'+name+'"></p><br>'+
        '<p><label>Description</label><input type="text" placeholder="Description" name="description" value="'+desc+'"></p><br>'+
        '<p><label>Section Name</label><select name="section" id="section_'+id+'">'+
        '<option value="">Select Section</option>'+
        '<?php echo $section_form; ?>'+
        '</select></p><br>'+
        '<p><label>Hashtags</label><select name="tag[]" required multiple>'+
        '<option value="" disabled>Select Tags</option>'+
        '<?php echo $hashtags; ?>'+
        '</select></p><br>'+
        '<p><button type="submit" class="buttonColor" style="height: 40px; font-size: 15px;">Update</button></p>'+
        '</form></div>';

        document.getElementById('contentReceived').innerHTML = elem;
        $("#section_"+id).val(section);
    }

    function delet(id)
    {
        document.getElementById('PopupParent').style.display = 'block';
        var elem = '<p style="font-size: 30px; font-weight: 400">Are you sure?</p>'+
        '<form method="post" action="{{route('deletesound')}}">'+
        '{{csrf_field()}}'+
        '<input type="hidden" name="id" value="'+id+'">'+
        '<button class="buttonred" type="submit" style="height: 40px; font-size: 18px;"><i class="fa fa-trash"></i>&nbsp;<b>Delete</b></button>'+
        '</form>';
        document.getElementById('contentReceived').innerHTML = elem;
    }


    function approve(id)
    {
        $('#preloader').show();

        var xmlhttp;
        if(window.XMLHttpRequest)
        {
            xmlhttp=new XMLHttpRequest();
        }
        else
        {
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange = function()
        {
            if(xmlhttp.readyState==4 && xmlhttp.status==200)
            {
                var approved = $('#approve_'+id).attr("data-approve");
                if (approved == 1)
                {
                    $('#approve_'+id).attr('title', 'Not Approved!');
                    $('#approve_'+id).removeClass('fa-check-circle');
                    $('#approve_'+id).addClass('fa-times-circle');
                    $('#approve_'+id).attr("data-approve", 0);
                    $('#approve_'+id).css('color', 'red');
                    $('#approve-menu_'+id).html("<i class='fas fa-check'></i>&nbsp;Approve");
                }
                else
                {
                    $('#approve_'+id).attr('title', 'Approved!');
                    $('#approve_'+id).removeClass('fa-times-circle');
                    $('#approve_'+id).addClass('fa-check-circle');
                    $('#approve_'+id).attr("data-approve", 1);
                    $('#approve_'+id).css('color', 'green');
                    $('#approve-menu_'+id).html("<i class='fas fa-times'></i>&nbsp;Disapprove");
                }

                $('#preloader').hide();
                $('body').append("<div class='notification'><div class='wdth'><div class='alert alert-success'>Successfully Updated!</div></div></div>");
                setTimeout(function() {
                    $('.notification').fadeOut('fast');
                }, 1000);
            }
        }
        xmlhttp.open("POST","/laravel/sound/approve", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("id="+id+"&_token=<?php echo csrf_token(); ?>");
    }

</script>

@include('layout.footer')
