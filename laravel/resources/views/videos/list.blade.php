@include('layout.header')

<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.dataTables.min.css')}}">
<script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#data1').DataTable();
    });
</script>


<div class="section mini dashboardscreen"><div class="wdth">
    <div class="col15 left">
        @include('layout.leftside')
    </div>
    <div class="col85 right contentside" style="padding:0px 15px;">
        <h2 class="title left">All Videos</h2>
        <div class="right" style="padding: 10px 0;">
            <form id="form">
                <select name="approved" id="approved" onchange="$('#form').submit()">
                    <option value="0">Not Approved</option>
                    <option value="1">Approved</option>
                </select>
            </form>
        </div>
    </div>

    @if( !$videos )
    <div class="textcenter nothingelse">
        <img src="img/noorder.png" alt="" />
        <h3>No Record Found</h3>
    </div>

    @else
    <table id='data1' class='display' style='width:100%'>
        <thead>
            <tr>
                <th>ID</th>
                <th>Play Preview</th>
                <th>Username</th>
                <th>Sound Name</th>
                <th>Section</th>
                <th>Score</th>
                <th>Created</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody id='myTable_row'>

            @foreach( $videos as $video )
            <tr style=" text-align: center;">
                <td>
                    <?php 
                    echo $video['id'];
                    if ($video['approved'] == true)
                        echo "&nbsp;<i class='far fa-check-circle' id='approve_".$video['id']."' style='color: green;' title='Approved!' data-approve=1></i>";
                    else
                        echo "&nbsp;<i class='far fa-times-circle' id='approve_".$video['id']."' style='color: red;' title='Not Approved!' data-approve=0></i>"; 
                    ?>

                </td>
                <td>
                    <a href="<?php echo $video['original_video']  ?>" target="_blank"><img src="{{asset('assets/img/play.png')}}" style="width: 30px;"></a>
                </td>

                <td>
                    @if ($video['user_info'])
                    {{$video['user_info']['username']}}	
                    @endif
                </td>

                <td style="line-height: 20px;">
                    @if ($video['sound'])
                    {{$video['sound']['sound_name']}}
                    @endif
                </td>
                <td>
                    {{$video['section']}}
                </td>
                <td>
                    <div id="score_{{$video['id']}}" data-id="{{$video['id']}}">
                        {{$video['video_score']}} &nbsp;
                        <i class="fa fa-edit" style="cursor: pointer;" onclick="editScore('score_{{$video['id']}}', {{$video['video_score']}})"></i>
                    </div>
                </td>

                <td>
                    {{$video['created']}}
                </td>

                <td>
                    <i onclick="myFunction('dropdown_<?php echo $video['id']; ?>')" class="fas fa-ellipsis-h" style="cursor: pointer;font-size: 18px;"></i>
                    <div id="dropdown_<?php echo $video['id']; ?>" class="w3-dropdown-content w3-bar-block w3-border" style="border: 2px black solid;">
                        <span onclick='approve("<?php echo $video['id']; ?>")' class="w3-bar-item w3-button" id="approve-menu_<?php echo $video['id'] ?>">
                            <?php
                            if ($video['approved'] == true) {
                                echo "<i class='fas fa-times'></i>&nbsp;<b>Disapprove</b>";
                            }
                            else
                                echo "<i class='fas fa-check'></i>&nbsp;<b>Approve</b>";
                            ?>
                        </span>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>ID</th>
                <th>Play Preview</th>
                <th>Username</th>
                <th>Sound Name</th>
                <th>Section</th>
                <th>Score</th>
                <th>Created</th>
                <th>Action</th>
            </tr>
        </tfoot>
    </table>
    <nav><ul class='pagination pagination-sm' id='myPager'></ul></nav>
    @endif
</div>
</div>

<script>
    $(document).ready(function() {
        var val = '<?php if(isset($_GET['approved'])) echo $_GET['approved']; else echo 0; ?>';
        $("#approved").val(val);
    });

    function editScore(id, score)
    {
        var x = document.getElementById(id);
        id = $('#'+id).attr('data-id');
        x.innerHTML = '<form method="post" action="{{route('updatescore')}}">'+
        '<input type="hidden" name="_token" value="{{csrf_token()}}">'+
        '<input type="hidden" name="id" value="'+id+'">'+
        '<input type="number" name="score" value="'+score+'"><br><br>'+
        '<button type="submit"><i class="fa fa-check"></i></button>'+
        '</form';
    }

    function myFunction(data) 
    {
        var x = '#'+data;
        if ($(x).css("display") === "block") {
            $(x).toggle();
        }
        else {
            $('.w3-dropdown-content').hide();
            $(x).toggle();
        }
    }

    $(document).mouseup(function (e) { 
        if ($(e.target).closest(".w3-dropdown-content").length === 0) { 
            $(".w3-dropdown-content").hide(); 
        } 
    });

    function addIntoSection(data)
    {
        document.getElementById("PopupParent").style.display="block";
        document.getElementById("contentReceived").innerHTML="<div style='margin-top:150px;' align='center'><img src='img/loader.gif' width='150px'></div>";
        var xmlhttp;
        if(window.XMLHttpRequest)
        {
            xmlhttp=new XMLHttpRequest();
        }
        else
        {
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange=function()
        {
            if(xmlhttp.readyState==4 && xmlhttp.status==200)
            {
                document.getElementById('contentReceived').innerHTML=xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET","ajex-events.php?addIntoSection=ok&id="+data);
        xmlhttp.send();

    }

    function approve(id)
    {
        $('#preloader').show();

        var xmlhttp;
        if(window.XMLHttpRequest)
        {
            xmlhttp=new XMLHttpRequest();
        }
        else
        {
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange = function()
        {
            if(xmlhttp.readyState==4 && xmlhttp.status==200)
            {
                var approved = $('#approve_'+id).attr("data-approve");
                if (approved == 1)
                {
                    $('#approve_'+id).attr('title', 'Not Approved!');
                    $('#approve_'+id).removeClass('fa-check-circle');
                    $('#approve_'+id).addClass('fa-times-circle');
                    $('#approve_'+id).attr("data-approve", 0);
                    $('#approve_'+id).css('color', 'red');
                    $('#approve-menu_'+id).html("<i class='fas fa-check'></i>&nbsp;Approve");
                }
                else
                {
                    $('#approve_'+id).attr('title', 'Approved!');
                    $('#approve_'+id).removeClass('fa-times-circle');
                    $('#approve_'+id).addClass('fa-check-circle');
                    $('#approve_'+id).attr("data-approve", 1);
                    $('#approve_'+id).css('color', 'green');
                    $('#approve-menu_'+id).html("<i class='fas fa-times'></i>&nbsp;Disapprove");
                }

                $('#preloader').hide();
                $('body').append("<div class='notification'><div class='wdth'><div class='alert alert-success'>Successfully Updated!</div></div></div>");
                setTimeout(function() {
                    $('.notification').fadeOut('fast');
                }, 1000);
            }
        }
        xmlhttp.open("POST","/laravel/video/approve", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("id="+id+"&_token=<?php echo csrf_token(); ?>");
    }
</script>

@include('layout.footer')
