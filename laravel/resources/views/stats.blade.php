@include('layout.header')
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="mdb/css/bootstrap.min.css">
<!-- Material Design Bootstrap -->
<link rel="stylesheet" href="mdb/css/mdb.min.css">

<div class="container" style="margin-top: 20px;">
	<center><h3><b>Statistics</b></h3></center>
	<div class="row">
		<button class="accordion-toggle btn btn-outline-danger waves-effect active" id="accordion1" data-toggle="collapse" data-parent="#accordion1" href="#collapse1" onclick="$('#collapse2').removeClass('show');$('#collapse3').removeClass('show');">Videos Watched</button>
		<button class="accordion-toggle btn collapsed btn-outline-danger waves-effect" id="accordion2" data-toggle="collapse" data-parent="#accordion2" href="#collapse2" onclick="$('#collapse1').removeClass('show');$('#collapse3').removeClass('show');">Liked Videos</button>
		<button class="accordion-toggle btn collapsed btn-outline-danger waves-effect" id="accordion3" data-toggle="collapse" data-parent="#accordion3" href="#collapse3" onclick="$('#collapse2').removeClass('show');$('#collapse1').removeClass('show');">Commented Videos</button>
	</div>

	<div class="row" style="margin-top: 20px;">
		<div id="collapse1" class="table-responsive text-nowrap collapse show">
			<table class="table table-sm table-striped table-hover" id="videos_watched" style="text-align: center;">
				<h4>Videos Watched</h4>
				<thead class="white-text" style="background-color: #D4401D;">
					<tr>
						<th scope="col" style="padding-left: 14px;">ID</th>
						<th scope="col">Play Preview </th>
						<th scope="col">Username</th>
						<th scope="col">Sound Name</th>
						<th scope="col">Views</th>
						<th scope="col">Created at</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($videos_watched as $data)
					@php
					$video = App\Video::where('id', $data->video_id)->first();
					@endphp
					@if ($video) 
					<tr>
						<td style="padding-left: 14px;">{{$video['id']}}</td>
						<td>
							<a href="{{$video['original_video']}}" target="_blank"><img src="{{asset('assets/img/play.png')}}" style="width: 30px;"></a>
						</td>
						<td>{{$video['user_info']['username']}}</td>
						<td>{{$video['sound']['sound_name']}}</td>
						<td>{{$data->count}}</td>
						<td>{{$video['created']}}</td>
					</tr>
					@endif
					@endforeach
					<tr>
						<td colspan="6"><center>{{$videos_watched->links()}}</center></td>
					</tr>
				</tbody>
			</table>
		</div>


		<div id="collapse2" class="table-responsive text-nowrap collapse">
			<table class="table table-sm table-striped table-hover" id="liked_videos" style="text-align: center;">
				<h4>Liked/Disliked Videos</h4>
				<thead class="white-text" style="background-color: #D4401D;">
					<tr>
						<th scope="col" style="padding-left: 14px;">ID</th>
						<th scope="col">Play Preview </th>
						<th scope="col">Username</th>
						<th scope="col">Sound Name</th>
						<th scope="col">Count</th>
						<th scope="col">Created at</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($liked_videos as $data)
					@php
					$video = App\Video::where('id', $data->video_id)->first();
					@endphp
					@if ($video)
					<tr>
						<td style="padding-left: 14px;">{{$video['id']}}</td>
						<td>
							<a href="{{$video['original_video']}}" target="_blank"><img src="{{asset('assets/img/play.png')}}" style="width: 30px;"></a>
						</td>
						<td>{{$video['user_info']['username']}}</td>
						<td>{{$video['sound']['sound_name']}}</td>
						<td>{{$data->count}}</td>
						<td>{{$video['created']}}</td>
					</tr>
					@endif
					@endforeach
					<tr>
						<td colspan="6"><center>{{$liked_videos->appends(request()->query())->render()}}</center></td>
					</tr>
				</tbody>
			</table>
		</div>


		<div id="collapse3" class="table-responsive text-nowrap collapse">
			<div class="table-responsive text-nowrap">
				<table class="table table-sm table-striped table-hover" id="commented_videos" style="text-align: center;">
					<h4>Commented on Videos</h4>
					<thead class="white-text" style="background-color: #D4401D;">
						<tr>
							<th scope="col" style="padding-left: 14px;">ID</th>
							<th scope="col">Play Preview </th>
							<th scope="col">Username</th>
							<th scope="col">Sound Name</th>
							<th scope="col">Comment Count</th>
							<th scope="col">Created at</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($commented_videos as $data)
						@php
						$video = App\Video::where('id', $data->video_id)->first();
						@endphp
						@if ($video)
						<tr>
							<td style="padding-left: 14px;">{{$video['id']}}</td>
							<td>
								<a href="{{$video['original_video']}}" target="_blank"><img src="{{asset('assets/img/play.png')}}" style="width: 30px;"></a>
							</td>
							<td>{{$video['user_info']['username']}}</td>
							<td>{{$video['sound']['sound_name']}}</td>
							<td>{{$data->count}}</td>
							<td>{{$video['created']}}</td>
						</tr>
						@endif
						@endforeach
						<tr>
							<td colspan="6"><center>{{$commented_videos->appends(request()->query())->render()}}</center></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- jQuery -->
<script type="text/javascript" src="mdb/js/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="mdb/js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="mdb/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="mdb/js/mdb.min.js"></script>
