    <?php 
    if( Session::has('msg') ) {
        echo "<div class='notification'><div class='wdth'><div class='alert alert-success'>".Session::get('msg')."</div></div></div>";
        ?>
        <script>
            setTimeout(function() {
                $('.notification').fadeOut('fast');
            }, 1000)
        </script>
        <?php
    }
    ?>

    <!-- Modal 7 (Ajax Modal)-->
    <div class="PopupParent" id="PopupParent"  style="background: rgba(0, 0, 0, 0.3); display: none;  position:  fixed; top: 0;width:  100%;height:  100%;z-index:  99999;">
        <div class="wdth" style="width:40%; margin-top:60px;background:  white;padding: 20px 20px; max-height: 650px;  border-radius:  5px; overflow: scroll;">

            <button type="button" onclick="ClosePopup();" style="position: relative; float: right; border: 0;border-radius: 50%;font-size: 21px;width: 30px;height: 30px;" class='buttonColor' >&times;</button>
            <div class="modal-content" style="" align="center">
                <div class="modal-body" id="contentReceived">
                    Loading... 
                </div>
            </div>
            
        </div>
    </div>

    <script>
        function ClosePopup()
        {

            document.getElementById("PopupParent").style.display="none";
            document.getElementById("contentReceived").innerHTML="";

        }   
    </script>
</body>
</html>
