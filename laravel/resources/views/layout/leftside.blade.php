<ul class="login_leftsidebar"> 

	<li <?php if( Route::currentRouteName() == "users" ) {
		echo 'class="active"';
	} ?> ><a href="{{route('users')}}">All Users</a></li>

	<li <?php if( Route::currentRouteName() == "superusers" ) {
		echo 'class="active"';
	} ?> ><a href="{{route('superusers')}}">All Super Users</a></li>

	<li <?php if( preg_match("/sounds|sections/", $_SERVER['REQUEST_URI']) ) {
		echo 'class="active"';
	} ?> ><a href="/laravel/sounds">All Sounds</a></li>

	<li <?php if(preg_match("/videos/", $_SERVER['REQUEST_URI']) ) {
		echo 'class="active"';
	} ?> ><a href="/laravel/videos">All Videos</a></li>

	<li><a href="{{route('stats')}}">Statistics</a></li>

	<li <?php if(isset($_GET['p'])) { if( $_GET['p'] == "change_password" ) {
		echo 'class="active"';
	} } ?> ><a href="dashboard.php?p=change_password">Chanage Password</a></li>

	<li <?php if(isset($_GET['log'])) { if( $_GET['log'] == "out" ) {
		echo 'class="active"';
	} } ?> ><a href="dashboard.php?log=out">Logout</a></li>

</ul>
