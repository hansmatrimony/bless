@include('layout.header')
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="../mdb/css/bootstrap.min.css">
<!-- Material Design Bootstrap -->
<link rel="stylesheet" href="../mdb/css/mdb.min.css">

<div class="container" style="margin-top: 20px;">

	<div class="card">
		<div class="card-body">
			<b>Id :</b> {{$user['fb_id']}}<br>
			<b>Username :</b> {{$user['username']}}<br>
			<b>Name :</b> {{$user['first_name']}} {{$user['last_name']}}
		</div>
	</div>

	<div class="row">
		<button class="accordion-toggle btn btn-outline-danger waves-effect active" id="accordion1" data-toggle="collapse" data-parent="#accordion1" href="#collapse1" onclick="$('#collapse2').removeClass('show');$('#collapse3').removeClass('show');">Videos Watched</button>
		<button class="accordion-toggle btn collapsed btn-outline-danger waves-effect" id="accordion2" data-toggle="collapse" data-parent="#accordion2" href="#collapse2" onclick="$('#collapse1').removeClass('show');$('#collapse3').removeClass('show');">Liked Videos</button>
		<button class="accordion-toggle btn collapsed btn-outline-danger waves-effect" id="accordion3" data-toggle="collapse" data-parent="#accordion3" href="#collapse3" onclick="$('#collapse2').removeClass('show');$('#collapse1').removeClass('show');">Commented Videos</button>
	</div>
	<div class="row" style="margin-top: 20px;">
		<div id="collapse1" class="table-responsive text-nowrap collapse show">
			<table class="table table-sm table-striped table-hover" id="videos_watched">
				<h4>Videos Watched</h4>
				<thead class="white-text" style="background-color: #D4401D;">
					<tr>
						<th scope="col" style="padding-left: 14px;">ID</th>
						<th scope="col">Play Preview </th>
						<th scope="col">Username</th>
						<th scope="col">Sound Name</th>
						<th scope="col">Viewed at</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($videos_watched as $video)
					<tr>
						<td style="padding-left: 14px;">{{$video['video']['id']}}</td>
						<td>
							<a href="{{$video['video']['original_video']}}" target="_blank"><img src="{{asset('assets/img/play.png')}}" style="width: 30px;"></a>
						</td>
						<td>{{$video['video']['user_info']['username']}}</td>
						<td>{{$video['video']['sound']['sound_name']}}</td>
						<td>{{$video['created']}}</td>
					</tr>
					@endforeach
					<tr>
						<td colspan="5"><center>{{$videos_watched->appends(request()->query())->render()}}</center></td>
					</tr>
				</tbody>
			</table>
		</div>


		<div id="collapse2" class="table-responsive text-nowrap collapse">
			<table class="table table-sm table-striped table-hover" id="liked_videos">
				<h4>Liked/Disliked Videos</h4>
				<thead class="white-text" style="background-color: #D4401D;">
					<tr>
						<th scope="col" style="padding-left: 14px;">ID</th>
						<th scope="col">Play Preview </th>
						<th scope="col">Username</th>
						<th scope="col">Sound Name</th>
						<th scope="col">Reaction</th>
						<th scope="col">Liked at</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($liked_videos as $video)
					<tr>
						<td style="padding-left: 14px;">{{$video['video']['id']}}</td>
						<td>
							<a href="{{$video['video']['original_video']}}" target="_blank"><img src="{{asset('assets/img/play.png')}}" style="width: 30px;"></a>
						</td>
						<td>{{$video['video']['user_info']['username']}}</td>
						<td>{{$video['video']['sound']['sound_name']}}</td>
						<td><img src="{{$video['like']}}" width="20" height="20"></td>
						<td>{{$video['created']}}</td>
					</tr>
					@endforeach
					<tr>
						<td colspan="6"><center>{{$liked_videos->appends(request()->query())->render()}}</center></td>
					</tr>
				</tbody>
			</table>
		</div>


		<div id="collapse3" class="table-responsive text-nowrap collapse">
			<div class="table-responsive text-nowrap">
				<table class="table table-sm table-striped table-hover" id="commented_videos">
					<h4>Commented on Videos</h4>
					<thead class="white-text" style="background-color: #D4401D;">
						<tr>
							<th scope="col" style="padding-left: 14px;">ID</th>
							<th scope="col">Play Preview </th>
							<th scope="col">Username</th>
							<th scope="col">Sound Name</th>
							<th scope="col">Comment</th>
							<th scope="col">Commented at</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($commented_videos as $video)
						<tr>
							<td style="padding-left: 14px;">{{$video['video']['id']}}</td>
							<td>
								<a href="{{$video['video']['original_video']}}" target="_blank"><img src="{{asset('assets/img/play.png')}}" style="width: 30px;"></a>
							</td>
							<td>{{$video['video']['user_info']['username']}}</td>
							<td>{{$video['video']['sound']['sound_name']}}</td>
							<td>{{$video['comments']}}</td>
							<td>{{$video['created']}}</td>
						</tr>
						@endforeach
						<tr>
							<td colspan="6"><center>{{$commented_videos->appends(request()->query())->render()}}</center></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

	<!-- jQuery -->
	<script type="text/javascript" src="../mdb/js/jquery.min.js"></script>
	<!-- Bootstrap tooltips -->
	<script type="text/javascript" src="../mdb/js/popper.min.js"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript" src="../mdb/js/bootstrap.min.js"></script>
	<!-- MDB core JavaScript -->
	<script type="text/javascript" src="../mdb/js/mdb.min.js"></script>
