@include('layout.header')

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.multiselect.css')}}" />

	
{{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" rel="stylesheet"/> --}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script> --}}



<style>

	body{
		margin:0px; 
		padding: 0px;
		font-family: Arial;
	}

	.container{

		display: block;
		width: 90%;
		background: #fafbfd;
		border-radius: 6px;
		line-height: normal;
		margin:20px auto;
		padding: 40px 20px;
	}

	.button{
		border: 0px;
		background-color: deepskyblue;
		color: white;
		padding: 5px 15px;
		margin-left: 10px;
	}
</style>


<div class="container dashboard">
	<h2 class="title" style="text-align: center;">Add Super User</h2>

	<div class="form">
		<form action="{{route('createsuperuser')}}" enctype="multipart/form-data" method="post" >
			{{csrf_field()}}
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label for="first_name"><b>First Name</b></label>
						<input id="first_name" class="form-control" name="first_name" required type="text" value="<?php echo @$user['first_name']; ?>">
					</div>
				</div>

				<div class="col-sm-4">
					<div class="form-group">
						<label for="last_name"><b>Last Name</b></label>
						<input id="last_name" class="form-control" name="last_name" required type="text" value="<?php echo @$user['last_name']; ?>">
					</div>
				</div>

				<div class="col-sm-4">
					<div class="form-group">
						<label for="phone"><b>Phone Number</b></label>
						<input id="phone" name="phone" class="form-control" required type="text" maxlength="10" value="<?php echo @$user['fb_id']; ?>">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label for="gender"><b>Gender</b></label>
						<select id="gender" name="gender" class="form-control" required type="text" value="<?php echo @$user['gender']; ?>" style="height: 51px;">
							<option value="">-- Choose --</option>
							<option value="Male">Male</option>
							<option value="Female">Female</option>
						</select>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="form-group">
						<label for="dob"><b>Date of Birth</b></label>
						<input id="dob" name="dob" class="form-control" required type="date" value="<?php echo @$user['dob']; ?>">
					</div>
				</div>

				<div class="col-sm-4">
					<div class="form-group">
						<label for="language" placeholder="Last Name"><b>Language</b></label>
						<select id="language" name="language[]" class="form-control"  type="text" style="height: 51px;" multiple>
							<option value="">-- Choose --</option>
							<option value="English">English</option>
							<option value="Hindi">Hindi</option>
						</select>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label for="category" placeholder="Category"><b>Category</b></label>
						<select id="category" name="category" class="form-control" required type="text" style="height: 51px;">
							<option value="1">Astrologers</option>
						</select>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="form-group">
						<label for="experience"><b>Experience (Yrs.)</b></label>
						<input id="experience" class="form-control" name="experience" type="number" min="0" required>
					</div>
				</div>
			</div>

			<hr>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="bio"><b>Bio</b></label>
						<textarea id="bio" class="form-control" name="bio" required></textarea>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="form-group">
						<label for="skills"><b>Skills</b></label>
						<select id="skills" name="skills[]" class="form-control" multiple required data-live-search="true">
							<option value="" disabled ><b>Choose Skill(s)</b></option>
							<?php 
							$skills = App\Skill::all();
							foreach ($skills as $skill) {
								echo '<option value="'.$skill['id'].'">'.$skill['skill'].'</option>';
							}
							?>
						</select>
					</div>
				</div>
			</div>

			<hr>
			<div class="row">

				<div class="col-sm-6">
					<div class="form-group">
						<label for="expert"><b>Expertise</b></label>
						<select id="expert" name="expertise[]" class="form-control" multiple required data-live-search="true">
							<option value="" disabled ><b>Choose Field(s)</b></option>
							<?php 
							$q = App\Expertise::all();
							foreach ($q as $row) {
								echo '<option value="'.$row['id'].'">'.$row['field'].'</option>';
							}
							?>
						</select>
					</div>
				</div>
			</div>

			<br><hr>
			<div class="row">
				<div class="col-sm-12" align="center">
					<h2 style="text-align: center;">Profile Picture</h2>
					<label for="pic" style="cursor: pointer;" class="uploadbtn">
						<div class='preview' align="center" style="margin-top: 40px;">
							<img id="preview" src="{{asset('/upload.png')}}" width="100" >
							<h2 style="color: #80808099; font-weight: 200; margin:10px; font-family: Arial;">Select Image</h2>
						</div>
						<input type="file" id="pic" name="pic" onchange="return Upload_image(this)" style="display: none;" accept="image/png, image/jpeg, image/jpg" />
					</label>
				</div>
			</div>

			<p ><input value="Submit" class="btn btn-sm btn-danger" type="submit"></p>
		</form>
		<div class="clear"></div>
	</div>
</div>
    <script src="{{asset('assets/js/jquery.multiselect.js')}}"></script>

<script>
	$(document).ready(function () {
		$('#skills').multiSelect();
		$('#expert').multiSelect();
	});

	function Upload_image(input) {
		if (input && input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#preview')
				.attr('src', e.target.result)
				.attr('class', 'col-sm-12');
			};

			reader.readAsDataURL(input.files[0]);
		}
	}
</script>

@include("layout.footer")
