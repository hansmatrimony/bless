@include('layout.header')

<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.dataTables.min.css')}}">
<script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
		$('#data1').DataTable();
	});
</script>
<style>
	.button{
		border: 0px;
		background-color: #D4401D;
		color: white;
		padding: 5px 15px;
		margin-left: 10px;
	}

	a {
		text-decoration: none;
	}
</style>

<div class="section mini dashboardscreen"><div class="wdth">
	<div class="col15 left">
		@include('layout.leftside')
	</div>
	<div class="col85 right contentside" style="padding:0px 15px;">

		<h2 class="title left">All Super Users</h2>
		<h2 class="right">
			<a class="button" href="/laravel/createsuperuser"><i class="fa fa-plus" aria-hidden="true"></i> Create New</a> &nbsp;&nbsp;
			<a class="button" href="#" onclick='popup()'><i class="fa fa-arrows-h" aria-hidden="true"></i> Convert User</a>
		</h2>

		@if(sizeof($users) == 0){
		<div class="textcenter nothingelse">
			<img src="img/noorder.png" alt="" />
			<h3>No Record Found</h3>
		</div>

		@else
		<table id='data1' class='display' style='width:100%'>
			<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Username</th>
					<th>Category</th>
					<th>Skill(s)</th>
					<th>Expertise</th>
					<th>Rating</th>
					<th>Call Price</th>
					<th>Created</th>
				</tr>
			</thead>
			<tbody id='myTable_row'>

				@foreach( $users as $user )
				<tr style=" text-align: center;">
					<td>
						{{$user['basic']['fb_id']}}
					</td>
					<td style="line-height: 20px;">
						{{$user['basic']['first_name']." ".$user['basic']['last_name']}}
					</td>
					<td>
						{{$user['basic']['username']}}
					</td>
					<td>
						{{$user['category']}}
					</td>
					<td>
						{{implode(', ', $user['skills'])}}
					</td>
					<td>
						{{implode(', ', $user['expertise'])}}
					</td>

					<td>
						<div id="rating_{{$user['id']}}" data-id="{{$user['id']}}">
							{{$user['rating']}} &nbsp;
							<i class="fa fa-edit" style="cursor: pointer;" onclick="editRating('rating_{{$user['id']}}', {{$user['rating']}})"></i>
						</div>
					</td>

					<td>
						<div id="price_{{$user['id']}}" data-id="{{$user['id']}}">
							{{$user['pricing']}} &nbsp;
							<i class="fa fa-edit" style="cursor: pointer;" onclick="editPrice('price_{{$user['id']}}', {{$user['pricing']}})"></i>
						</div>
					</td>

					<td>
						{{$user['basic']['created']}}
					</td>
				</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Username</th>
					<th>Category</th>
					<th>Skill(s)</th>
					<th>Expertise</th>
					<th>Rating</th>
					<th>Call Price</th>
					<th>Created</th>
				</tr>
			</tfoot>
		</table>
		<nav><ul class='pagination pagination-sm' id='myPager'></ul></nav>
		@endif
	</div>
</div>


<script>
    function editRating(id, rating)
    {
        var x = document.getElementById(id);
        id = $('#'+id).attr('data-id');
        x.innerHTML = '<input type="number" name="rating" value="'+rating+'"><br><br>'+
        '<button onclick="rating('+id+')"><i class="fa fa-check"></i></button>';
    }

    function rating(id) {
    	var val = $('#rating_'+id+' input[name=rating]').val();
    	$.ajax({
    		type: 'POST',
    		url: "{{route('updaterating')}}",
    		data: {_token: "{{csrf_token()}}", id: id, rating: val}
    	})
    	.done(function (data) {
    		document.getElementById('rating_'+id).innerHTML = data.rating + "&nbsp; <i class='fa fa-edit' style='cursor: pointer;' onclick='editRating('rating_'"+id+", "+data.rating+")'></i>";
    	})
    	.fail (function (failure) {

    	});
    }


    function editPrice(id, price)
    {
        var x = document.getElementById(id);
        id = $('#'+id).attr('data-id');
        x.innerHTML = '<input type="number" name="price" value="'+price+'"><br><br>'+
        '<button onclick="price('+id+')"><i class="fa fa-check"></i></button>';
    }

    function price(id) {
    	var val = $('#price_'+id+' input[name=price]').val();
    	$.ajax({
    		type: 'POST',
    		url: "{{route('updateprice')}}",
    		data: {_token: "{{csrf_token()}}", id: id, price: val}
    	})
    	.done(function (data) {
    		document.getElementById('price_'+id).innerHTML = data.price + "&nbsp; <i class='fa fa-edit' style='cursor: pointer;' onclick='editPrice('price_'"+id+", "+data.price+")'></i>";
    	})
    	.fail (function (failure) {

    	});
    }

</script>
@include('layout.footer')
