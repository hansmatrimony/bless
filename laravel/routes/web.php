<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});



Route::get('/users', 'HomeController@users')->name('users');
Route::get('/activeusers', 'HomeController@activeUsers')->name('activeusers');
Route::get('/superusers', 'HomeController@superUsers')->name('superusers');
Route::get('/createsuperuser', function () {
	return view('superuser.create');
});
Route::post('/createsuperuser', 'HomeController@createSuperUser')->name('createsuperuser');
Route::post('/updaterating', 'HomeController@updateRating')->name('updaterating');
Route::post('/updateprice', 'HomeController@updateCallPrice')->name('updateprice');
Route::get('/users/activity', 'HomeController@userActivity')->name('useractivity');

Route::get('/sounds', 'HomeController@getSounds');
Route::get('/uploadSound', function () {
	return view('sounds.upload');
});
Route::post('/tempupload', 'HomeController@tempUpload');
Route::post('/sound/upload', 'HomeController@uploadSound')->name('uploadSound');
Route::post('/sound/approve', 'HomeController@approveOrDissaproveSound');
Route::post('/sound/update', 'HomeController@updateSound')->name('updatesound');
Route::post('/sound/markgood', 'HomeController@markGoodSound')->name('markgoodsound');
Route::post('/sound/delete', 'HomeController@deleteSound')->name('deletesound');
Route::get('/sections', 'HomeController@getSections');
Route::get('/createsection', function() {
	return view('sounds.createsection');
});
Route::get('/videos', 'HomeController@getVideos');
Route::post('/video/approve', 'HomeController@approveOrDissaproveVideo');
Route::post('/video/updatescore', 'HomeController@updateVideoScore')->name('updatescore');
Route::get('/stats', 'HomeController@statistics')->name('stats');
