<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuperUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('super_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 200);
            $table->string('language', 100);
            $table->text('experience');
            $table->boolean('online');
            $table->integer('rating');
            $table->integer('likes');
            $table->integer('people_served');
            $table->integer('badges');
            $table->integer('pricing');
            $table->integer('reviews');
            $table->text('bio');
            $table->text('skills');
            $table->text('expertise');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('super_user');
    }
}
