<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuperuserSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('superuser_skills', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('super_user_id');
            $table->unsignedBigInteger('skill_id');
            $table->timestamps();

            $table->foreign('super_user_id')->references('id')->on('super_user');
            $table->foreign('skill_id')->references('id')->on('skills');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('superuser_skills');
    }
}
