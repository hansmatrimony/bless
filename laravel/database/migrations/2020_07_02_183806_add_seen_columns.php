<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSeenColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	Schema::table('follow_users', function (Blueprint $table) {
            $table->tinyInteger('seen')->default(0)->nullable();
        });

        Schema::table('video_like_dislike', function (Blueprint $table) {
            $table->tinyInteger('seen')->default(0)->nullable();
        });

        Schema::table('video_comment', function (Blueprint $table) {
            $table->tinyInteger('seen')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
