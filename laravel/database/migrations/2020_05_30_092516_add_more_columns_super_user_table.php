<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMoreColumnsSuperUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('super_user', function (Blueprint $table) {
            $table->bigInteger('user_id')->unsigned()->after('id');
            $table->dropColumn(['name', 'bio']);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->bigInteger('super_user_id')->unsigned()->nullable()->after('fb_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
