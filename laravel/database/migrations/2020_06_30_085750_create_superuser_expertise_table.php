<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuperuserExpertiseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('superuser_expertise', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('super_user_id');
            $table->unsignedBigInteger('expertise_id');
            $table->timestamps();

            $table->foreign('super_user_id')->references('id')->on('super_user');
            $table->foreign('expertise_id')->references('id')->on('expertise');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('superuser_expertise');
    }
}
