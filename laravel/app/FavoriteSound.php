<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavoriteSound extends Model
{
    protected $table = 'fav_sound';

    public $timestamps = false;

    protected $fillable = ['fb_id', 'sound_id'];

    protected $attributes = [
    	'created' => date("Y-m-d H:i:s"),
    ];

    protected $casts = [
    	'created' => 'timestamps',
    ];
}
