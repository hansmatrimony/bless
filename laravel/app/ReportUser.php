<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportUser extends Model
{
    public $timestamps = false;

    protected $fillable = ['reporter_fb_id', 'reported_fb_id'];
}
