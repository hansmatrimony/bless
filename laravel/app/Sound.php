<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sound extends Model
{
	protected $table = 'sound';

    public $timestamps = false;

    protected $fillable = ['sound_name', 'description', 'thum', 'section', 'duration'];

    protected $appends = ['audio_path'];

    protected $attributes = [
        'thum' => ''
    ];

    protected function getAudioPathAttribute()
    {
    	return array(
    		"mp3" => "http://d1xlfmzv4wdngj.cloudfront.net/audios/".$this->id.".mp3",
    		"acc" => "http://d1xlfmzv4wdngj.cloudfront.net/audios/".$this->id.".aac"
    	);
    }
}
