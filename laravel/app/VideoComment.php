<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoComment extends Model
{
    protected $table = 'video_comment';

    public $timestamps = false;

    protected $fillable = ['video_id', 'fb_id', 'comments'];

    protected $appends = ['video'];

    protected function getVideoAttribute()
    {
    	try {
    		return Video::where('id', $this->video_id)->first();
    	}
    	catch (\Exception $e) {
    		return null;
    	}
    }
}
