<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Event;
use Illuminate\Support\Facades\Storage;
use FFMpeg;
use DB;

class WatermarkDownloadableVideo implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $id;
    protected $name;
    protected $data;

    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            FFMpeg::cleanupTemporaryFiles();
            $original_video = FFMpeg::fromDisk('s3')->open('videos/'.$this->name);

            $format = new FFMpeg\Format\Video\X264();
            $format->setAudioCodec("aac");

            $original_video->addFilter(function ($filters){
                $filters->watermark('/var/www/html/laravel/storage/app/public/watermark/bless.png', array(
                    'position' => 'relative',
                    'bottom' => 25,
                    'right' => 25,
                ));
            })->export()->inFormat($format)->toDisk('public')->withVisibility('public')->save('watermark/original/'.$this->name);

            $video = FFMpeg::fromDisk('public')->open('watermark/original/'.$this->name);

            $dimensions = $video->getStreams()->videos()->first()->getDimensions();
            $width = $dimensions->getWidth();
            $height = $dimensions->getHeight();

            $end = FFMpeg::fromDisk('public')->open('watermark/end.mp4');

            $commands ='scale=gte(iw/ih\,'.$width.'/'.$height.')*'.$width.'+lt(iw/ih\,'.$width.'/'.$height.')*(('.$height.'*iw)/ih):lte(iw/ih\,'.$width.'/'.$height.')*'.$height.'+gt(iw/ih\,'.$width.'/'.$height.')*(('.$width.'*ih)/iw),pad='.$width.':'.$height.':('.$width.'-gte(iw/ih\,'.$width.'/'.$height.')*'.$width.'-lt(iw/ih\,'.$width.'/'.$height.')*(('.$height.'*iw)/ih))/2:('.$height.'-lte(iw/ih\,'.$width.'/'.$height.')*'.$height.'-gt(iw/ih\,'.$width.'/'.$height.')*(('.$width.'*ih)/iw))/2:black'; 

            $end->addFilter(function ($filters) use($commands){
                $filters->custom($commands);
            })->export()->inFormat($format)->toDisk('public')->withVisibility('public')->save('watermark/end/'.$this->name);


            $video->concat(['/var/www/html/laravel/storage/app/public/watermark/original/'.$this->name, '/var/www/html/laravel/storage/app/public/watermark/end/'.$this->name])->saveFromSameCodecs('/var/www/html/laravel/storage/app/public/watermark/modified/'.$this->name, true); 

            Storage::disk('s3')->put('downloads/'.$this->name, file_get_contents('/var/www/html/laravel/storage/app/public/watermark/modified/'.$this->name),array('visibility'=>'public'));

	    DB::table('videos')->where('id', $this->id)->update(['isWatermark'=>3]);

            unlink('/var/www/html/laravel/storage/app/public/watermark/end/'.$this->name);

            unlink('/var/www/html/laravel/storage/app/public/watermark/original/'.$this->name);
            unlink('/var/www/html/laravel/storage/app/public/watermark/modified/'.$this->name);

            FFMpeg::cleanupTemporaryFiles();
	
        }catch(\Exception $e){
        
        }
    }
}
