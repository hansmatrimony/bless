<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Event;
use Illuminate\Support\Facades\Storage;
use FFMpeg;
use DB;

class ConvertToHls implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $id;
    protected $name;
    protected $data;

    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            FFMpeg::cleanupTemporaryFiles();
            $format = new FFMpeg\Format\Video\X264();
            $format->setAudioCodec("aac");

            Storage::disk('public')->put('download/original/'.$this->name, Storage::disk('s3')->get('videos/'.$this->name));
            $file = explode(".", $this->name)[0];

            $original_video = FFMpeg::fromDisk('public')->open("download/original/".$this->name);

            $dimensions = $original_video->getStreams()->videos()->first()->getDimensions();
            $width = $dimensions->getWidth();
            $height = $dimensions->getHeight();

            if($width>$height){
                $ratio = $height/$width;

                $h1 = ceil($ratio*640);
                if($h1 % 2 == 1) $h1++;

                $h2 = ceil($ratio*842);
                if($h2 % 2 == 1) $h2++;

                $h3 = ceil($ratio*1280);
                if($h3 % 2 == 1) $h3++;

                $h4 = ceil($ratio*1920);
                if($h4 % 2 == 1) $h4++;


                echo shell_exec("/usr/bin/ffmpeg -hide_banner -y -i /var/www/html/laravel/storage/app/public/download/original/".$this->name." \
          -vf scale=w=640:h=".$h1." -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod  -b:v 800k -maxrate 856k -bufsize 1200k -b:a 96k -hls_segment_filename /var/www/html/laravel/storage/app/public/download/hls/".$file."_360p_%03d.ts /var/www/html/laravel/storage/app/public/download/hls/".$file."_360p.m3u8 \
          -vf scale=w=842:h=".$h2." -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 1400k -maxrate 1498k -bufsize 2100k -b:a 128k -hls_segment_filename /var/www/html/laravel/storage/app/public/download/hls/".$file."_480p_%03d.ts /var/www/html/laravel/storage/app/public/download/hls/".$file."_480p.m3u8 \
          -vf scale=w=1280:h=".$h3." -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 2800k -maxrate 2996k -bufsize 4200k -b:a 128k -hls_segment_filename /var/www/html/laravel/storage/app/public/download/hls/".$file."_720p_%03d.ts /var/www/html/laravel/storage/app/public/download/hls/".$file."_720p.m3u8 \
          -vf scale=w=1920:h=".$h4." -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 5000k -maxrate 5350k -bufsize 7500k -b:a 192k -hls_segment_filename /var/www/html/laravel/storage/app/public/download/hls/".$file."_1080p_%03d.ts /var/www/html/laravel/storage/app/public/download/hls/".$file."_1080p.m3u8");

            }else{
                $ratio = $width/$height;

                $w1 = ceil($ratio*360);
                if($w1 % 2 == 1) $w1++;

                $w2 = ceil($ratio*480);
                if($w2 % 2 == 1) $w2++;

                $w3 = ceil($ratio*720);
                if($w3 % 2 == 1) $w3++;

                $w4 = ceil($ratio*1080);
                if($w4 % 2 == 1) $w4++;


                echo shell_exec("/usr/bin/ffmpeg -hide_banner -y -i /var/www/html/laravel/storage/app/public/download/original/".$this->name." \
          -vf scale=w=".$w1.":h=360 -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod  -b:v 800k -maxrate 856k -bufsize 1200k -b:a 96k -hls_segment_filename /var/www/html/laravel/storage/app/public/download/hls/".$file."_360p_%03d.ts /var/www/html/laravel/storage/app/public/download/hls/".$file."_360p.m3u8 \
          -vf scale=w=".$w2.":h=480 -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 1400k -maxrate 1498k -bufsize 2100k -b:a 128k -hls_segment_filename /var/www/html/laravel/storage/app/public/download/hls/".$file."_480p_%03d.ts /var/www/html/laravel/storage/app/public/download/hls/".$file."_480p.m3u8 \
          -vf scale=w=".$w3.":h=720 -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 2800k -maxrate 2996k -bufsize 4200k -b:a 128k -hls_segment_filename /var/www/html/laravel/storage/app/public/download/hls/".$file."_720p_%03d.ts /var/www/html/laravel/storage/app/public/download/hls/".$file."_720p.m3u8 \
          -vf scale=w=".$w4.":h=1080 -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 5000k -maxrate 5350k -bufsize 7500k -b:a 192k -hls_segment_filename /var/www/html/laravel/storage/app/public/download/hls/".$file."_1080p_%03d.ts /var/www/html/laravel/storage/app/public/download/hls/".$file."_1080p.m3u8");
            }

            $myfile = fopen('/var/www/html/laravel/storage/app/public/download/hls/'.$file.'.m3u8', "w") or die("Unable to open file!");
            
            $txt = "#EXTM3U\n#EXT-X-VERSION:3\n#EXT-X-STREAM-INF:BANDWIDTH=800000\n".$file."_360p.m3u8\n#EXT-X-STREAM-INF:BANDWIDTH=1400000\n".$file."_480p.m3u8\n#EXT-X-STREAM-INF:BANDWIDTH=2800000\n".$file."_720p.m3u8\n#EXT-X-STREAM-INF:BANDWIDTH=5000000\n".$file."_1080p.m3u8";
            fwrite($myfile, $txt);
            fclose($myfile);

            foreach(glob('/var/www/html/laravel/storage/app/public/download/hls/*.*') as $file){
                $array = explode('/',$file);
                Storage::disk('s3')->put('videos/'.end($array), file_get_contents($file),array('visibility'=>'public'));
                unlink($file);
            }

            unlink('/var/www/html/laravel/storage/app/public/download/original/'.$this->name);

            DB::table('videos')->where('id', $this->id)->update(['video' => explode('.',$this->name)[0].'.m3u8','isHls'=>2]);
            FFMpeg::cleanupTemporaryFiles();
        }catch(\Exception $e){

        }
    }
}
