<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Event;
use Illuminate\Support\Facades\Storage;
use FFMpeg;
use DB;

class OptimizeVideo implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $id;
    protected $name;
    protected $data;

    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            FFMpeg::cleanupTemporaryFiles();

         
            $original_video = FFMpeg::fromDisk('s3')->open('videos/'.$this->name);
            $dimensions = $original_video->getStreams()->videos()->first()->getDimensions();
            $width = $dimensions->getWidth();
            $height = $dimensions->getHeight();
            $duration = (int)$original_video->getDurationInSeconds();
            $bitrate = (int)$original_video->getStreams()->videos()->first()->get('bit_rate');
            $format = new FFMpeg\Format\Video\X264();
            $format->setAudioCodec("aac");

            if(!Storage::disk('s3')->has('originals/'.$this->name)){
                Storage::disk('s3')->copy('videos/'.$this->name, 'originals/'.$this->name);
                Storage::disk('s3')->setVisibility('originals/'.$this->name, 'public');
            }

            $size = Storage::disk('s3')->size('videos/'.$this->name);

       //    $max_size = 10000000;
            echo "original size\n";
            echo $size;
            echo "max new size to be";
            $max_size = (($duration/30 -1)*2.5 + 5)*1000;
            echo "duration \n";
            echo $duration;
            echo "original bitrate \n";
            echo $bitrate;
            echo "new bitrate \n";
            echo ceil($max_size/($duration));
            $max_size = (($duration/30 -1)*2.5 + 5)*1000000;
            if($size > $max_size){
                $ratio = $max_size/$size;
                $new_bitrate = ceil($ratio*$bitrate/1000);
             //   $new_bitrate = ceil($max_size/($duration));
                $format->setKiloBitrate($new_bitrate);
                if($new_bitrate % 2 == 1) $new_bitrate++;
                $original_video->export()->inFormat($format)->toDisk('s3')->withVisibility('public')->save('/videos/'.$this->name);
            }

            // if($size>$max_size){
            //     $ratio = $max_size/$size;
            //     $new_bitrate = ceil($ratio*$bitrate/1000);
            //     $format->setKiloBitrate($new_bitrate);
            //     if($new_bitrate % 2 == 1) $new_bitrate++;
            //     $original_video->export()->inFormat($format)->toDisk('s3')->withVisibility('public')->save('/videos/'.$this->name);
            // }

            DB::table('videos')->where('id', $this->id)->update(['isResized'=>2]);
            FFMpeg::cleanupTemporaryFiles();
        }catch(\Exception $e){

        }
    }
}
