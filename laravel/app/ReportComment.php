<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportComment extends Model
{
    public $timestamps = false;

    protected $fillable = ['reporter_fb_id', 'reported_comment_id'];
}
