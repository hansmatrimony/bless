<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuperuserExpertise extends Model
{
    protected $table = 'superuser_expertise';

    protected $fillable = ['super_user_id', 'expertise_id'];

}
