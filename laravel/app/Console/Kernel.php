<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Test::class,
        Commands\WatermarkVideo::class,
        Commands\ConvertVideo::class,
        Commands\RetryFailedJobs::class,
        Commands\RestartQueueListener::class,
        Commands\ResizeVideo::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //$schedule->command('retry:failedJobs')->dailyAt('5:30');
        //$schedule->command('restart:queueListener')->hourly();
        $schedule->command('convert:video')->everyMinute();
        $schedule->command('watermark:video')->everyFiveMinutes();
        $schedule->command('resize:video')->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
