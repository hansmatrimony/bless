<?php
namespace App\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\OptimizeVideo;
use FFMpeg;
use DB;

class ResizeVideo extends Command
{
    use DispatchesJobs;
        
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resize:video';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resize Video';
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    
    public function handle()
    {
     
        $v = DB::table('videos')->where('isResized', 0)->get();
        for($i=0; $i<=sizeof($v); $i++){
            try{
               
               $this->dispatch(new OptimizeVideo($v[$i]->id, $v[$i]->original_video));
               DB::table('videos')->where('id', $v[$i]->id)->update(['isResized'=>1]);
            }catch(\Exception $e){

            }
        }
    }
}