<?php
namespace App\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\WatermarkDownloadableVideo;
use FFMpeg;
use DB;

class WatermarkVideo extends Command
{
    use DispatchesJobs;
        
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'watermark:video';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Watermark Video';
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    
    public function handle()
    {
        $v = DB::table('videos')
		->where('isWatermark', '!=', 3)
		->where('id', '>', 16)
		->where('isResized', 2)
->take(1)->get();
//->where('isResized', 2)->where('isWatermark', 0)->get();
        for($i=0; $i<=sizeof($v); $i++){
            try{
echo $v[$i]->id;
               $this->dispatch(new WatermarkDownloadableVideo($v[$i]->id, $v[$i]->original_video));
               DB::table('videos')->where('id', $v[$i]->id)->update(['isWatermark'=>1]);
            }catch(\Exception $e){

            }
        }
    }
}
