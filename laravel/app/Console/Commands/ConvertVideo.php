<?php
namespace App\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\ConvertToHls;
use FFMpeg;
use DB;

class ConvertVideo extends Command
{
    use DispatchesJobs;
        
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert:video';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload Video';
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    
    public function handle()
    {
        $v = DB::table('videos')->where('isResized', 2)->where('isHls', 0)->get();
        for($i=0; $i<=sizeof($v); $i++){
            try{
               $this->dispatch(new ConvertToHls($v[$i]->id, $v[$i]->original_video));
               DB::table('videos')->where('id', $v[$i]->id)->update(['isHls'=>1]);
            }catch(\Exception $e){

            }
        }
    }
}