<?php
namespace App\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use FFMpeg;
use DB;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload Video';
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    
    public function handle()
    {
        
        $this->name = '1.mp4';
        $this->id = '2';
        

        Storage::disk('public')->put('download/original/'.$this->name, Storage::disk('s3')->get('videos/'.$this->name));
        //unlink('/var/www/html/laravel/storage/app/public/watermark/modified/1.mp4');
        //echo shell_exec("/usr/bin/ffmpeg' '-y' '-i' '/var/www/html/laravel/storage/app/public/watermark/end/1.mp4' '-threads' '12' '-vcodec' 'libx264' '-acodec' 'aac' '-b:v' '1000k' '-refs' '6' '-coder' '1' '-sc_threshold' '40' '-flags' '+loop' '-me_range' '16' '-subq' '7' '-i_qfactor' '0.71' '-qcomp' '0.6' '-qdiff' '4' '-trellis' '1' '-b:a' '128k' '-vf' '[in]scale=1920:1090:force_original_aspect_ratio=increase,pad=1920:1090:x=(1920-iw)/2:y=(1090-ih)/2:color=red[out]' '-pass' '1' '-passlogfile' '/tmp/ffmpeg-passes5df25088dc9dd9v8fe/pass-5df25088dca2f' '/var/www/html/laravel/storage/app/public/watermark/end/t/1.mp4'");
        // echo shell_exec('ffmpeg -f concat -safe 0 -i file /var/www/html/laravel/storage/app/public/watermark/original/1.mp4 file /var/www/html/laravel/storage/app/public/watermark/end/1.mp4 -c copy /var/www/html/laravel/storage/app/public/watermark/modified/1.mp4');
        // $original_video = FFMpeg::fromDisk('s3')->open('videos/'.$this->name);

            // $original_video = FFMpeg::fromDisk('public')->open('watermark/original/'.$this->name);

            // $format = new FFMpeg\Format\Video\X264();
            // $format->setAudioCodec("aac");

            // $original_video->addFilter(function ($filters){
            //     $filters->watermark('/var/www/html/laravel/storage/app/public/watermark/bless.gif', array(
            //         'position' => 'relative',
            //         'bottom' => 25,
            //         'right' => 25,
            //     ));
            // })->export()->inFormat($format)->toDisk('public')->withVisibility('public')->save('watermark/between/'.$this->name);

            // $video = FFMpeg::fromDisk('public')->open('watermark/original/'.$this->name);

            // $dimensions = $video->getStreams()->videos()->first()->getDimensions();
            // $width = $dimensions->getWidth();
            // $height = $dimensions->getHeight();
            
            // $end = FFMpeg::fromDisk('public')->open('watermark/end.mp4');
            
            
            // $commands ='scale=gte(iw/ih\,'.$width.'/'.$height.')*'.$width.'+lt(iw/ih\,'.$width.'/'.$height.')*(('.$height.'*iw)/ih):lte(iw/ih\,'.$width.'/'.$height.')*'.$height.'+gt(iw/ih\,'.$width.'/'.$height.')*(('.$width.'*ih)/iw),pad='.$width.':'.$height.':('.$width.'-gte(iw/ih\,'.$width.'/'.$height.')*'.$width.'-lt(iw/ih\,'.$width.'/'.$height.')*(('.$height.'*iw)/ih))/2:('.$height.'-lte(iw/ih\,'.$width.'/'.$height.')*'.$height.'-gt(iw/ih\,'.$width.'/'.$height.')*(('.$width.'*ih)/iw))/2:black';

            // $end->addFilter(function ($filters) use($commands){
            //     $filters->custom($commands);
            // })->export()->inFormat($format)->toDisk('public')->withVisibility('public')->save('watermark/end/'.$this->name);

            // $video->concat(['/var/www/html/laravel/storage/app/public/watermark/original/'.$this->name, '/var/www/html/laravel/storage/app/public/watermark/end/'.$this->name])->saveFromSameCodecs('/var/www/html/laravel/storage/app/public/watermark/modified/'.$this->name, true); 

            // Storage::disk('s3')->put('downloads/'.$this->name, file_get_contents('/var/www/html/laravel/storage/app/public/watermark/modified/'.$this->name),array('visibility'=>'public'));
         
        $file = explode(".", $this->name)[0];

        $original_video = FFMpeg::fromDisk('public')->open("download/original/".$this->name);

        $dimensions = $original_video->getStreams()->videos()->first()->getDimensions();
        $width = $dimensions->getWidth();
        $height = $dimensions->getHeight();

        if($width>$height){
            $ratio = $height/$width;

            $h1 = ceil($ratio*640);
            if($h1 % 2 == 1) $h1++;

            $h2 = ceil($ratio*842);
            if($h2 % 2 == 1) $h2++;

            $h3 = ceil($ratio*1280);
            if($h3 % 2 == 1) $h3++;

            $h4 = ceil($ratio*1920);
            if($h4 % 2 == 1) $h4++;


            echo shell_exec("/usr/bin/ffmpeg -hide_banner -y -i /var/www/html/laravel/storage/app/public/download/original/".$this->name." \
      -vf scale=w=640:h=".$h1." -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod  -b:v 800k -maxrate 856k -bufsize 1200k -b:a 96k -hls_segment_filename /var/www/html/laravel/storage/app/public/download/hls/".$file."_360p_%03d.ts /var/www/html/laravel/storage/app/public/download/hls/".$file."_360p.m3u8 \
      -vf scale=w=842:h=".$h2." -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 1400k -maxrate 1498k -bufsize 2100k -b:a 128k -hls_segment_filename /var/www/html/laravel/storage/app/public/download/hls/".$file."_480p_%03d.ts /var/www/html/laravel/storage/app/public/download/hls/".$file."_480p.m3u8 \
      -vf scale=w=1280:h=".$h3." -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 2800k -maxrate 2996k -bufsize 4200k -b:a 128k -hls_segment_filename /var/www/html/laravel/storage/app/public/download/hls/".$file."_720p_%03d.ts /var/www/html/laravel/storage/app/public/download/hls/".$file."_720p.m3u8 \
      -vf scale=w=1920:h=".$h4." -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 5000k -maxrate 5350k -bufsize 7500k -b:a 192k -hls_segment_filename /var/www/html/laravel/storage/app/public/download/hls/".$file."_1080p_%03d.ts /var/www/html/laravel/storage/app/public/download/hls/".$file."_1080p.m3u8");

        }else{
            $ratio = $width/$height;

            $w1 = ceil($ratio*360);
            if($w1 % 2 == 1) $w1++;

            $w2 = ceil($ratio*480);
            if($w2 % 2 == 1) $w2++;

            $w3 = ceil($ratio*720);
            if($w3 % 2 == 1) $w3++;

            $w4 = ceil($ratio*1080);
            if($w4 % 2 == 1) $w4++;


            echo shell_exec("/usr/bin/ffmpeg -hide_banner -y -i /var/www/html/laravel/storage/app/public/download/original/".$this->name." \
      -vf scale=w=".$w1.":h=360 -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod  -b:v 800k -maxrate 856k -bufsize 1200k -b:a 96k -hls_segment_filename /var/www/html/laravel/storage/app/public/download/hls/".$file."_360p_%03d.ts /var/www/html/laravel/storage/app/public/download/hls/".$file."_360p.m3u8 \
      -vf scale=w=".$w2.":h=480 -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 1400k -maxrate 1498k -bufsize 2100k -b:a 128k -hls_segment_filename /var/www/html/laravel/storage/app/public/download/hls/".$file."_480p_%03d.ts /var/www/html/laravel/storage/app/public/download/hls/".$file."_480p.m3u8 \
      -vf scale=w=".$w3.":h=720 -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 2800k -maxrate 2996k -bufsize 4200k -b:a 128k -hls_segment_filename /var/www/html/laravel/storage/app/public/download/hls/".$file."_720p_%03d.ts /var/www/html/laravel/storage/app/public/download/hls/".$file."_720p.m3u8 \
      -vf scale=w=".$w4.":h=1080 -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v 5000k -maxrate 5350k -bufsize 7500k -b:a 192k -hls_segment_filename /var/www/html/laravel/storage/app/public/download/hls/".$file."_1080p_%03d.ts /var/www/html/laravel/storage/app/public/download/hls/".$file."_1080p.m3u8");
        }

        $myfile = fopen('/var/www/html/laravel/storage/app/public/download/hls/'.$file.'.m3u8', "w") or die("Unable to open file!");
        
        $txt = "#EXTM3U\n#EXT-X-VERSION:3\n#EXT-X-STREAM-INF:BANDWIDTH=800000\n".$file."_360p.m3u8\n#EXT-X-STREAM-INF:BANDWIDTH=1400000\n".$file."_480p.m3u8\n#EXT-X-STREAM-INF:BANDWIDTH=2800000\n".$file."_720p.m3u8\n#EXT-X-STREAM-INF:BANDWIDTH=5000000\n".$file."_1080p.m3u8";
        fwrite($myfile, $txt);
        fclose($myfile);

        foreach(glob('/var/www/html/laravel/storage/app/public/download/hls/*.*') as $file){
            $array = explode('/',$file);
            Storage::disk('s3')->put('videos/'.end($array), file_get_contents($file),array('visibility'=>'public'));
            unlink($file);
        }

        // $original_video = FFMpeg::fromDisk('s3')->open('videos/'.$this->name);
        // $dimensions = $original_video->getStreams()->videos()->first()->getDimensions();
        // $width = $dimensions->getWidth();
        // $height = $dimensions->getHeight();

        // $bitrate = (int)$original_video->getStreams()->videos()->first()->get('bit_rate');
        // $format = new FFMpeg\Format\Video\X264();
        // $format->setAudioCodec("aac");

        // if(!Storage::disk('s3')->has('originals/'.$this->name)){
        //     Storage::disk('s3')->copy('videos/'.$this->name, 'originals/'.$this->name);
        //     Storage::disk('s3')->setVisibility('originals/'.$this->name, 'public');
        // }

        // $size = Storage::disk('s3')->size('videos/'.$this->name);

        // $max_size = 10000000;

        // if($size>$max_size){
        //     $ratio = $max_size/$size;
        //     $new_bitrate = ceil($ratio*$bitrate/1000);
        //     $format->setKiloBitrate($new_bitrate);
        //     if($new_bitrate % 2 == 1) $new_bitrate++;
        //     $original_video->export()->inFormat($format)->toDisk('s3')->withVisibility('public')->save('/videos/'.$this->name);
        // }
        

        //system('convert /var/www/html/laravel/storage/app/public/watermark/bless.gif -coalesce -repage 0x0 -resize '.(int)$width_.'x'.(int)$height_.' -layers Optimize /var/www/html/laravel/storage/app/public/watermark/gif/'.explode('.', $this->name)[0].'.gif');
        

        //echo shell_exec('/usr/bin/ffmpeg -hide_banner -v warning -i /var/www/html/laravel/storage/app/public/watermark/bless.gif -filter_complex "[0:v] scale=320:-1:flags=lanczos,split [a][b]; [a] palettegen=reserve_transparent=on:transparency_color=ffffff [p]; [b][p] paletteuse" /var/www/html/laravel/storage/app/public/watermark/gif/'.explode('.', $this->name)[0].'.gif');
        //
        // $format = new FFMpeg\Format\Video\X264();
        // $format->setAudioCodec("aac");

        // $original_video->addFilter(function ($filters){
        //     $filters->watermark('/var/www/html/laravel/storage/app/public/watermark/bless.gif', array(
        //         'position' => 'relative',
        //         'bottom' => 25,
        //         'right' => 25,
        //     ));
        // })->export()->inFormat($format)->toDisk('public')->withVisibility('public')->save('watermark/original/'.$this->name);

        // $video = FFMpeg::fromDisk('public')->open('watermark/original/'.$this->name);

        // $dimensions = $video->getStreams()->videos()->first()->getDimensions();
        // $width = $dimensions->getWidth();
        // $height = $dimensions->getHeight();
        
        // $end = FFMpeg::fromDisk('public')->open('watermark/end.mp4');
        
        
        // $commands ='scale=gte(iw/ih\,'.$width.'/'.$height.')*'.$width.'+lt(iw/ih\,'.$width.'/'.$height.')*(('.$height.'*iw)/ih):lte(iw/ih\,'.$width.'/'.$height.')*'.$height.'+gt(iw/ih\,'.$width.'/'.$height.')*(('.$width.'*ih)/iw),pad='.$width.':'.$height.':('.$width.'-gte(iw/ih\,'.$width.'/'.$height.')*'.$width.'-lt(iw/ih\,'.$width.'/'.$height.')*(('.$height.'*iw)/ih))/2:('.$height.'-lte(iw/ih\,'.$width.'/'.$height.')*'.$height.'-gt(iw/ih\,'.$width.'/'.$height.')*(('.$width.'*ih)/iw))/2:black';

        // $end->addFilter(function ($filters) use($commands){
        //     $filters->custom($commands);
        // })->export()->inFormat($format)->toDisk('public')->withVisibility('public')->save('watermark/end/'.$this->name);

        // $video->concat(['/var/www/html/laravel/storage/app/public/watermark/original/'.$this->name, '/var/www/html/laravel/storage/app/public/watermark/end/'.$this->name])->saveFromSameCodecs('/var/www/html/laravel/storage/app/public/watermark/modified/'.$this->name, true); 

        // FFMpeg::cleanupTemporaryFiles();
        //     $video = FFMpeg::fromDisk('s3')->open('videos/'.$this->name);

        //     $original_video = FFMpeg::fromDisk('s3')->open('videos/'.$this->name);

        //     $original_video->addFilter(function ($filters) {
        //         $filters->watermark('/var/www/html/laravel/storage/app/public/watermark/bless.gif', array(
        //             'position' => 'relative',
        //             'bottom' => 100,
        //             'right' => 100,
        //         ));
        //     })->export()->inFormat(new \FFMpeg\Format\Video\X264)->toDisk('public')->save('/watermark/original/'.$this->name);
        //     // 

        //     $video = FFMpeg::fromDisk('public')->open('/watermark/original/'.$this->name);

        //     $video->getFrameFromSeconds((int)($video->getDurationInSeconds()/2), true)->export()->toDisk('public')->save('watermark/'.$this->name.'.png');

        //     $dimensions = $video->getStreams()->videos()->first()->getDimensions();

        //     $end = FFMpeg::fromDisk('public')->open('/watermark/end.mp4');
        //     echo $dimensions->getWidth();
        //     echo $dimensions->getHeight();

        //     if($dimensions->getWidth()>$dimensions->getHeight()){
        //         $end->addFilter(function ($filters) use($dimensions){
        //             $filters->resize(new \FFMpeg\Coordinate\Dimension($dimensions->getWidth(), $dimensions->getHeight()), \FFMpeg\Filters\Video\ResizeFilter::RESIZEMODE_SCALE_WIDTH, true);
        //         })->export()->inFormat(new \FFMpeg\Format\Video\X264)->toDisk('public')->save('/watermark/end/'.$this->name);
        //     }else{
        //         $end->addFilter(function ($filters) use($dimensions){
        //             $filters->resize(new \FFMpeg\Coordinate\Dimension($dimensions->getWidth(), $dimensions->getHeight()), \FFMpeg\Filters\Video\ResizeFilter::RESIZEMODE_SCALE_HEIGHT, true);
        //         })->export()->inFormat(new \FFMpeg\Format\Video\X264)->toDisk('public')->save('/watermark/end/'.$this->name);
        //     }


        //     $video->concat(['/var/www/html/laravel/storage/app/public/watermark/original/'.$this->name, '/var/www/html/laravel/storage/app/public/watermark/end/'.$this->name])->saveFromSameCodecs('/var/www/html/laravel/storage/app/public/watermark/modified/'.$this->name, true);

        //     Storage::disk('s3')->put('downloads/'.$this->name, file_get_contents('/var/www/html/laravel/storage/app/public/watermark/modified/'.$this->name),array('visibility'=>'public'));

        //     unlink('/var/www/html/laravel/storage/app/public/watermark/end/'.$this->name);

        //     unlink('/var/www/html/laravel/storage/app/public/watermark/original/'.$this->name);
        //     unlink('/var/www/html/laravel/storage/app/public/watermark/modified/'.$this->name);
            
        //     DB::table('videos')->where('id', $this->id)->update(['isWatermark'=>2]);
        //     FFMpeg::cleanupTemporaryFiles();
        //}
        // //234
        //
        //for($i=1; $i<=1; $i++){
            //try{
                // $v = DB::table('videos')->where('id', $i)->get();
                // if(sizeof($v)){
                //     echo '1';
                //     FFMpeg::cleanupTemporaryFiles();
                    // $original_video = FFMpeg::fromDisk('s3')->open('videos/'.$v[0]->original_video);

                    // $original_video->addFilter(function ($filters) {
                    //     $filters->watermark('/var/www/html/laravel/storage/app/public/watermark/bless.gif', array(
                    //         'position' => 'relative',
                    //         'bottom' => 50,
                    //         'right' => 50,
                    //     ));
                    // })->export()->inFormat(new \FFMpeg\Format\Video\X264)->toDisk('public')->withVisibility('public')->save('/watermark/original/'.$v[0]->original_video);
                    // // 

                    // $video = FFMpeg::fromDisk('public')->open('/watermark/original/'.$v[0]->original_video);

                    // $dimensions = $video->getStreams()->videos()->first()->getDimensions();

                    // $end = FFMpeg::fromDisk('public')->open('/watermark/end.mp4');

                    // $end->addFilter(function ($filters) use($dimensions){
                    //     $filters->resize(new \FFMpeg\Coordinate\Dimension($dimensions->getWidth(), $dimensions->getHeight()), \FFMpeg\Filters\Video\ResizeFilter::RESIZEMODE_INSET, true);
                    // })->export()->inFormat(new \FFMpeg\Format\Video\X264)->toDisk('public')->withVisibility('public')->save('/watermark/end/'.$v[0]->original_video);


                    // $video->
                    // concat(['/var/www/html/laravel/storage/app/public/watermark/original/'.$v[0]->original_video, '/var/www/html/laravel/storage/app/public/watermark/end/'.$v[0]->original_video])
                    // ->saveFromSameCodecs('/var/www/html/laravel/storage/app/public/watermark/modified/'.$v[0]->original_video, true);

                    // Storage::disk('s3')->put('downloads/'.$v[0]->original_video, file_get_contents('/var/www/html/laravel/storage/app/public/watermark/modified/'.$v[0]->original_video),array('visibility'=>'public'));

                    // unlink('/var/www/html/laravel/storage/app/public/watermark/end/'.$v[0]->original_video);

                    // unlink('/var/www/html/laravel/storage/app/public/watermark/original/'.$v[0]->original_video);


                    //->export()->inFormat(new \FFMpeg\Format\Video\X264)->toDisk('s3')->withVisibility('public')->save('downloads/'.$v[0]->original_video);
                    // $lowBitrate = (new \FFMpeg\Format\Video\X264)->setKiloBitrate(250);
                    // $midBitrate = (new \FFMpeg\Format\Video\X264)->setKiloBitrate(500);
                    // $highBitrate = (new \FFMpeg\Format\Video\X264)->setKiloBitrate(1000);

                    // $video->exportForHLS()
                    //     ->onProgress(function ($percentage) {
                    //         echo "$percentage % transcoded";
                    //     })
                    //     ->addFormat($lowBitrate)
                    //     ->addFormat($midBitrate)
                    //     ->addFormat($highBitrate)
                    //     ->inFormat(new \FFMpeg\Format\Video\X264)
                    //     ->toDisk('public')->withVisibility('public')->save('/download/hls/'.$i.'.m3u8');

                    // foreach(glob('/var/www/html/laravel/storage/app/public/download/hls/*.*') as $file){
                    //     $array = explode('/',$file);
                    //     Storage::disk('s3')->put('videos/'.end($array), file_get_contents($file),array('visibility'=>'public'));
                    //     unlink($file);
                    // }

                    //DB::table('videos')->where('id', $v[0]->id)->update(['video' => $v[0]->id.'.m3u8']);
    

                    // $dimensions = $video->getStreams()->videos()->first()->getDimensions();
                    // if($dimensions->getWidth()>640 || $dimensions->getHeight()>480){
                    //     $video->addFilter(function ($filters) {
                    //         $filters->resize(new \FFMpeg\Coordinate\Dimension(640, 480), \FFMpeg\Filters\Video\ResizeFilter::RESIZEMODE_INSET, true);
                    //     })->export()->inFormat(new \FFMpeg\Format\Video\X264)->toDisk('s3')->withVisibility('public')->save('videos/'.$v[0]->video);
                    // }
                    // $audio = str_replace(".mp4",".aac",$video[0]->video);
                    // $audio = str_replace(".m4v",".aac",$audio);
                    // $link = FFMpeg::fromDisk('s3')->open('audios/'.$audio);
                    // $time = $link->getDurationInSeconds();
                    // $time = $time<10 ? '0'.$time.':00' : $time.':00';
                    // DB::insert('insert into sound (id, sound_name, description, thum, section, duration, created) values (?, ?, ?, ?, ?, ?, ?)', [$i, '','','','Remix', $time, date('Y-m-d H:i:s')]);
                    //FFMpeg::cleanupTemporaryFiles();
                //}
            //}catch(\Exception $e){

            //}
        //}
        //     // $rand = rand(2, 38);
        //     // $numbers = array_rand(range(2, 39), $rand);
        //     $tags = explode('#', str_replace(",","",$video[0]->description));
                // $numbers = array_rand(range(2, 39), sizeof($tags));
                // for($j=0;$j<sizeof($numbers);$j++){
                //     DB::insert('insert into video_comment (video_id, fb_id, comments, created) values (?, ?, ?, ?)', [$i, $numbers[$j], $tags[$j], date('Y-m-d H:i:s')]);
                // }
            

        // }
        // $flysystem = Storage::disk();
        // $s = Storage::disk('s3')->files('temp');
        // for($i=0;$i<sizeof($s);$i++){
        //     try{
        //         $name = $i+1;
        //         Storage::disk('s3')->copy($s[$i], 'videos/'.$name.'.'.substr($s[$i], strpos($s[$i], ".") + 1));
        //         $video = FFMpeg::fromDisk('s3')->open($s[$i]);
        //         $video->export()->toDisk('s3')->inFormat(new \FFMpeg\Format\Audio\Aac)->withVisibility('public')->save('audios/'.$name.'.aac');
        //         $video->export()->toDisk('s3')->inFormat(new \FFMpeg\Format\Audio\Mp3)->withVisibility('public')->save('audios/'.$name.'.mp3');
        //         $video->getFrameFromSeconds((int)($video->getDurationInSeconds()/2), true)->export()->toDisk('s3')->withVisibility('public')->save('thumbnails/'.$name.'.png');
        //         $file = rand();

        //         $video->gif(FFMpeg\Coordinate\TimeCode::fromSeconds(1), $video->getStreams()->videos()->first()->getDimensions(), (int)($video->getDurationInSeconds()/10))->save('/var/www/html/laravel/storage/app/public/download/'.$file.'.gif');
        //         Storage::disk('s3')->put('gifs/'.$name.'.gif', file_get_contents('/var/www/html/laravel/storage/app/public/download/'.$file.'.gif'),array('visibility'=>'public'));
        //         unlink('/var/www/html/laravel/storage/app/public/download/'.$file.'.gif');

        //         FFMpeg::cleanupTemporaryFiles();
                
        //         DB::insert('insert into videos (fb_id, description, video, thum, gif, view, section, sound_id, created) values (?, ?, ?, ?, ?, ?, ?, ?, ?)', [rand(1,40), '', $name.'.'.substr($s[$i], strpos($s[$i], ".") + 1), $name.'.png', $name.'.gif', rand(20, 400), '', $name, date('Y-m-d H:i:s')]);
        //     }catch(\Exception $e){

        //     }
        // }
        echo 'hi';
    }
}