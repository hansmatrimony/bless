<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RestartQueueListener extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'restart:queueListener';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restart Queue Listener';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('queue:restart');
    }
}
