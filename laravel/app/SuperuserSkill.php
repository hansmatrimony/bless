<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuperuserSkill extends Model
{
    protected $fillable = ['super_user_id', 'skill_id'];
}
