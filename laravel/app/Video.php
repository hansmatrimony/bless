<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    public $timestamps = false;

    protected $fillable = ['fb_id', 'description', 'video', 'gif', 'thum', 'view', 'section', 'sound_id', 'isHLS', 'isWatermark', 'original_video', 'isResized', 'tags', 'location', 'video_score', 'video_language', 'approved'];

    protected $appends = ['sound', 'user_info'];

    protected function getVideoAttribute($video)
    {
    	return "http://d1xlfmzv4wdngj.cloudfront.net/videos/".$video;
    }

    protected function getOriginalVideoAttribute($original_video)
    {
    	return "http://d1xlfmzv4wdngj.cloudfront.net/videos/".$original_video;
    }

    protected function getThumAttribute($thum)
    {
    	if ($thum)
	    	return "http://d1xlfmzv4wdngj.cloudfront.net/thumbnails/".$thum;

	    return null;
    }

    protected function getGifAttribute($gif)
    {
    	if ($gif)
	    	return "http://d1xlfmzv4wdngj.cloudfront.net/gifs/".$gif;

	    return null;
    }

    protected function getSoundAttribute()
    {
    	if ($this->sound_id) {
	    	return Sound::where('id', $this->sound_id)->first();
    	}

	    return null;
    }

    protected function getUserInfoAttribute()
    {
        $user = AppUser::where('fb_id', $this->fb_id)
        ->select(['first_name', 'last_name', 'username', 'profile_pic'])
        ->first();
        
        return $user;
    }
}
