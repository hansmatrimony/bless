<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expertise extends Model
{
    protected $table = 'expertise';

    public $timestamps = false;

    protected $fillable = ['field'];
}
