<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;
use App\AppUser;
use App\SuperUser;
use App\Sound;
use App\SoundSection;
use App\Video;
use App\VideoView;
use App\VideoLike;
use App\VideoComment;
use App\SuperuserSkill;
use App\SuperuserExpertise;

class HomeController extends Controller
{
	public function users (Request $request)
	{
		$users = AppUser::orderBy('id', 'desc')->get()->toArray();
		return view('users', compact('users'));
	}

	public function activeUsers (Request $request)
	{
		$users = AppUser::where('login_time' , '>=', date("Y-m-d"))
		->orderBy('login_time', 'desc')
		->get()
		->toArray();
		return view('activeusers', compact('users'));
	}

	public function superUsers (Request $request)
	{
		$users = SuperUser::orderBy('id', 'desc')->get()->toArray();
		return view('superuser.list', compact('users'));
	}

	public function createSuperUser(Request $request)
	{
		if(isset($request->phone) && isset($request->first_name) && isset($request->last_name)) {

			$fb_id = $request->phone;
			$skills = (array)$request->skills;
			$expertise = (array)$request->expertise;
			$language = (array)$request->language;
			
			$username = $request->first_name.rand();
			$filename = $request->first_name.'_'.rand().'_'.rand().".jpg";

			if ($language)
				$language = implode(', ', $language);

			$user = AppUser::where('fb_id', 'LIKE', '%'.$fb_id)->get();

			if (!$user) {
				return back()->with('msg', 'User already exist!');
			}
			else {
				$user = AppUser::create([
					'fb_id' => $fb_id,
					'username' => $username,
					'first_name' => $request->first_name,
					'last_name' => $request->last_name,
					'device' => 'android',
					'signup_type' => 'OTP',
					'bio' => $request->bio,
					'dob' => $request->dob,
					'gender' => $request->gender,
					'created' => date("Y-m-d H:i:s"),
					'profile_pic' => '',
					'tokon' => ''
				]);

				if ($user) {
					$superuser = SuperUser::create([
						'user_id' => $user->id,
						'language' => $language,
						'experience' => $request->experience
					]);

					foreach ($skills as $val) {
						SuperuserSkill::create([
							"super_user_id" => $superuser->id,
							'skill_id' => (int)$val
						]);
					}

					foreach ($expertise as $val) {
						SuperuserExpertise::create([
							"super_user_id" => $superuser->id,
							'expertise_id' => (int)$val
						]);
					}

					if ($request->hasfile('pic')) {
						$profile_pic = $request->pic;
						Storage::disk('s3')->put('photos/'.$filename, file_get_contents($profile_pic), 'public');
						$user->profile_pic = $filename;
						$user->save();
					}
				}
				else {
					return back()->with('msg', 'Some error occured. Try again!');
				}
			}
		}
		else {
			return back()->with('msg', 'Please fill all details.');
		}
	}

	public function getSounds(Request $request)
	{
		$approved = $request->approved ? $request->approved : 0;
		if ($approved == 2) {
			$sounds = Sound::where('approved', 0)
			->where('good', 0)
			->orderBy('id', 'desc')->get()->toArray();
		}
		else {
			$sounds = Sound::where('approved', $approved)
			->orderBy('id', 'desc')->get()->toArray();
		}

		return view('sounds.list', compact('sounds'));
	}

	public function updateSound(Request $request)
	{
		$sound = Sound::where('id', $request->id)->first();
		if ($sound) {
			$tag = implode(" ", $request->tag);
			$sound->sound_name = $request->sound_name ? $request->sound_name : "";
			$sound->description = $request->description ? $request->description : "";
			$sound->section = $request->section ? $request->section : $sound->section;
			$sound->tags = $tag;
			$sound->save();
		}
		return back()->with('msg', 'Succesfully updated!');
	}
	
	public function markGoodSound(Request $request)
	{
		$sound = Sound::where('id', $request->id)->first();
		if ($sound)
		{
			if ($request->good == 0) {
				$sound->good = 0;
				$sound->approved = 0;
				$sound->save();
			}
		}
		return back();
	}

	public function tempUpload()
	{
		/* Getting file name */
		$filename = $_FILES['file']['name'];

		$expldd = explode(".",$filename);

		$filerandName = rand().rand();
		$extensionn = $expldd[1];

		$finalName = $filerandName.'.'.$extensionn;

		/* Location */
		$location = "upload/".$filename;
		$uploadOk = 1;
		$imageFileType = pathinfo($location,PATHINFO_EXTENSION);

		/* Valid Extensions */
		$valid_extensions = array("jpg", "aac","mp3");
		/* Check file extension */
		if( !in_array(strtolower($imageFileType),$valid_extensions) ) {
			$uploadOk = 0;
		}

		if($uploadOk == 0){
			return 0;
		} else{
			/* Upload file */
			if(move_uploaded_file($_FILES['file']['tmp_name'], "upload/".$finalName)){
				return $finalName;
			} else{
				return 0;
			}
		}
	}

	public function uploadSound(Request $request)
	{
		try {
		$url = $request->url;
			$sound = Sound::create([
				'sound_name' => $request->title,
				'description' => $request->tagss,
				'section' => $request->section_name,
				'tags' => implode(" ", $request->hashtags)
			]);

			Storage::disk('s3')->put('audios/'.$sound->id.'.aac', $url);
			$filename = explode('/', $url);
			@unlink(end($filename));
		}
		catch (\Exception $e) {
			return back()->with('msg', 'Try Again! Failed to upload');
		}

		return back()->with('msg', 'Succesfully uploaded!');
	}

	public function deleteSound(Request $request)
	{
		$id = $request->id;
		if ($id) {
			Sound::where('id', $id)->delete();
			return back()->with('msg', 'Succesfully Deleted.');
		}

		return back()->with('msg', 'Error: Can\'t delete.');
	}

	public function getSections()
	{
		$sections = SoundSection::orderBy('id', 'desc')->get()->toArray();
		return view('sounds.sections', compact('sections'));
	}

	public function getVideos(Request $request)
	{
		$approved = $request->approved ? 1 : 0;
		$videos = Video::where('approved', $approved)->orderBy('video_score', 'desc')->orderBy('id', 'desc')->get()->toArray();
		return view('videos.list', compact('videos'));
	}

	public function approveOrDissaproveVideo(Request $request)
	{
		$video = Video::where('id', $request->id)->first();
		if ($video) {
			$video->approved = $video->approved == 1 ? 0 : 1;
			$video->save();
			return response()->json(['msg' => 'ok'], 200);
		}
		else
			return response()->json(['msg' => 'Video does not exist'], 404);	
	}

	public function updateVideoScore(Request $request)
	{
		$video = Video::where('id', $request->id)->first();
		if ($video) {
			$video->video_score = $request->score;
			$video->save();
			return back()->with('msg', 'Video Score Updated.');
		}
		else
			return back()->with('msg', 'Video does not exist');	
	}

	public function approveOrDissaproveSound(Request $request)
	{
		$sound = Sound::where('id', $request->id)->first();
		if ($sound) {
			$sound->approved = $sound->approved == 1 ? 0 : 1;
			if ($sound->approved == 1)
				$sound->good = 1;
			$sound->save();
			return response()->json(['msg' => 'ok'], 200);
		}
		else
			return response()->json(['msg' => 'Sound does not exist'], 404);	
	}

	public function userActivity(Request $request)
	{
		$fb_id = $request->fb_id;
		if ($fb_id) {
			$user = AppUser::where('fb_id', $fb_id)->first();
			$videos_watched = VideoView::where('fb_id', $fb_id)->orderBy('id', 'desc')->paginate(10);
			$liked_videos = VideoLike::where('fb_id', $fb_id)->orderBy('id', 'desc')->paginate(10);
			$commented_videos = VideoComment::where('fb_id', $fb_id)->orderBy('id', 'desc')->paginate(10);

			return view('userActivity', compact('user', 'videos_watched', 'liked_videos', 'commented_videos'));
		}
		else
			return back();
	}

	public function statistics(Request $request)
	{
		$videos_watched = DB::table('video_view')
		->select('video_id', DB::raw('count(*) as count'))
		->groupBy('video_id')
		->orderBy(DB::raw('count(*)'), "desc")
		->paginate(20);

		$liked_videos = DB::table('video_like_dislike')
		->select('video_id', DB::raw('count(*) as count'))
		->groupBy('video_id')
		->orderBy(DB::raw('count(*)'), "desc")
		->paginate(20);

		$commented_videos = DB::table('video_comment')
		->select('video_id', DB::raw('count(*) as count'))
		->groupBy('video_id')
		->orderBy(DB::raw('count(*)'), "desc")
		->paginate(20);

		return view('stats', compact('videos_watched', 'liked_videos', 'commented_videos'));
	}

	public function updateRating(Request $request)
	{
		$superuser = SuperUser::find($request->id);
		if ($superuser) {
			$superuser->rating = (int)$request->rating;
			$superuser->save();
			return response()->json(['rating' => $superuser->rating], 200);
		}
		else {
			return response()->json(['msg' => "some error occured"], 200);
		}
	}

	public function updateCallPrice(Request $request)
	{
		$superuser = SuperUser::find($request->id);
		if ($superuser) {
			$superuser->pricing = (int)$request->price;
			$superuser->save();
			return response()->json(['price' => $superuser->pricing], 200);
		}
		else {
			return response()->json(['msg' => "some error occured"], 200);
		}
	}
}
