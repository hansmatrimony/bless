<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppUser extends Model
{
	protected $table = 'users';

    public $timestamps = false;

    protected $fillable = ['fb_id', 'username', 'first_name', 'last_name', 'gender', 'bio', 'profile_pic', 'block', 'version', 'device', 'signup_type', 'dob', 'tokon', 'login_time', 'location', 'email', 'birth_time'];

    protected function getProfilePicAttribute($profile_pic)
    {
    	if(strpos($profile_pic, "http") !== false){
			return $profile_pic;
		}else{
			return 'http://d1xlfmzv4wdngj.cloudfront.net/photos/'.$profile_pic;
		}
    }
}
