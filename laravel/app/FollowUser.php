<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FollowUser extends Model
{
    public $timestamps = false;

    protected $fillable = [
    	'fb_id',
    	'followed_fb_id'
    ];
}
