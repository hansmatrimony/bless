<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    public $timestamps = false;

    protected $fillable = ['image'];

    protected function getImageAttribute($image)
    {
    	return "http://d1xlfmzv4wdngj.cloudfront.net/likes/".$image;
    }
}
