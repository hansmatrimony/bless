<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoundSection extends Model
{
	protected $table = 'sound_section';

    public $timestamps = false;

    protected $fillable = ['section_name'];
}
