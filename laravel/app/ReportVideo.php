<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportVideo extends Model
{
    public $timestamps = false;

    protected $fillable = ['reporter_fb_id', 'reported_video_id'];
}
