<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoLike extends Model
{
	protected $table = 'video_like_dislike';

    public $timestamps = false;

    protected $fillable = ['video_id', 'fb_id', 'action', 'like_id'];

    protected $appends = ['video'];

    protected function getVideoAttribute()
    {
    	try {
    		return Video::where('id', $this->video_id)->first();
    	}
    	catch (\Exception $e) {
    		return null;
    	}
    }
}
