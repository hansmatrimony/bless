<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuperUser extends Model
{
	protected $table = 'super_user';
	
    protected $fillable = ['user_id', 'language', 'experience', 'online', 'rating', 'likes', 'people_served', 'badges', 'pricing', 'reviews', 'category'];

    protected $appends = ['basic', 'skills', 'expertise'];

    public function getBasicAttribute()
    {
    	return AppUser::where('id', $this->user_id)->first();
    }

    public function getSkillsAttribute($skills)
    {
        $skills = SuperuserSkill::where('super_user_id', $this->id)->pluck('skill_id');
        if ($skills)
            return Skill::whereIn('id', $skills)->pluck('skill');

        return null;
    }

    public function getExpertiseAttribute($expertise)
    {
        $expertise = SuperuserExpertise::where('super_user_id', $this->id)->pluck('expertise_id');
    	if ($expertise)
    		return Expertise::whereIn('id', $expertise)->pluck('field');
    	
    	return null;
    }

    public function getCategoryAttribute($category)
    {
        if ($category == 1)
            return 'Astrologers';
    }
}
