<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoView extends Model
{
	protected $table = 'video_view';

    public $timestamps = false;

    protected $fillable = ['video_id', 'fb_id'];

    protected $appends = ['video'];

    protected function getVideoAttribute()
    {
    	try {
    		return Video::where('id', $this->video_id)->first();
    	}
    	catch (\Exception $e) {
    		return null;
    	}
    }
}
