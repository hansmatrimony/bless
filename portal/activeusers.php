<?php 
require_once("config.php"); 
if( isset($_SESSION['id']))
{ 
	
	?>

	<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">
	<!--<script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script>-->
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#data1').DataTable();
		} );
	</script>

	<h2 class="title left">Daily Active Users</h2>
	<h2 class="right">
		<a class="button" href="dashboard.php?p=users" style="text-decoration: none"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
	</h2>
	<?php 

	$query = mysqli_query($conn,"SELECT * from users where login_time >= '".date("Y-m-d")."' order by login_time desc");

	if(mysqli_num_rows($query) == 0){
		?>
		<div class="textcenter nothingelse">
			<img src="img/noorder.png" alt="" />
			<h3>No Record Found</h3>
		</div>
		<?php

	} else {
		echo "<script>$('#count').html('(".mysqli_num_rows($query).")')</script>";
		echo "<table id='data1' class='display' style='width:100%''>
		<thead>
		<tr>
		<th>ID</th>
		<th>Name</th>
		<th>Username</th>
		<th>Videos Watched</th>
		<th>Likes</th>
		<th>Comments</th>
		<th>Created</th>
		</tr>
		</thead>
		<tbody id='myTable_row'>";

		while( $row = mysqli_fetch_array($query) ) {
			$q1 = mysqli_query($conn, "SELECT count(*) as count from video_view where fb_id='".$row['id']."' && created >= '".date("Y-m-d")."'");
			$videos_watched = ($q1) ? mysqli_fetch_assoc($q1)['count'] : 0;

			$q2 = mysqli_query($conn, "SELECT count(*) as count from video_like_dislike where fb_id='".$row['id']."' && created >= '".date("Y-m-d")."'");
			$likes = ($q2) ? mysqli_fetch_assoc($q2)['count'] : 0;

			$q3 = mysqli_query($conn, "SELECT count(*) as count from video_comment where fb_id='".$row['id']."' && created >= '".date("Y-m-d")."'");
			$comments = ($q3) ? mysqli_fetch_assoc($q3)['count'] : 0;
			?>

			<tr style="text-align: center;">
				<td>
					<?php 
					echo $row['fb_id']; 
					?>
				</td>
				<td style="line-height: 20px;">
					<?php 
					echo $row['first_name']." ".$row['last_name'];
					?>		
				</td>
				<td>
					<?php echo $row['username'];  ?>
				</td>

				<td>
					<?php 
					echo $videos_watched; 
					?>
				</td>

				<td>
					<?php 
					echo $likes; 
					?>
				</td>

				<td>
					<?php 
					echo $comments; 
					?>
				</td>

				<td>
					<?php 
					echo $row['created']; 
					?>
				</td>
			</tr>
			<?php
		}
		echo "</tbody>
		</table> <nav><ul class='pagination pagination-sm' id='myPager'></ul></nav>";
	}
	?>

<?php } else {

	@header("Location: index.php");
	echo "<script>window.location='index.php'</script>";
	die;

} ?>
