<?php 
require_once("header.php");
require_once("config.php");

if(!isset($_SESSION['id'])){ 
	@header("Location: index.php");
	echo "<script>window.location='index.php'</script>";
} 

?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<style>

	body{
		margin:0px; 
		padding: 0px;
		font-family: Arial;
	}

	.container{

		display: block;
		width: 90%;
		background: #fafbfd;
		border-radius: 6px;
		line-height: normal;
		margin:20px auto;
		padding: 40px 20px;
	}

	.button{
		border: 0px;
		background-color: deepskyblue;
		color: white;
		padding: 5px 15px;
		margin-left: 10px;
	}
</style>

<?php

if (isset($_GET['action'])) {
	if ($_GET['action'] == 'submit') {

		if(isset($_POST['phone']) && isset($_POST['first_name']) && isset($_POST['last_name'])) {

			$fb_id = htmlspecialchars(strip_tags($_POST['phone'] , ENT_QUOTES));
			$first_name = htmlspecialchars(strip_tags($_POST['first_name'] , ENT_QUOTES));
			$last_name = htmlspecialchars(strip_tags($_POST['last_name'] , ENT_QUOTES));
			$gender = htmlspecialchars(strip_tags($_POST['gender'] , ENT_QUOTES));
			$bio = htmlspecialchars(strip_tags($_POST['bio'] , ENT_QUOTES));
			$skills = (array)$_POST['skills'];
			$experience = htmlspecialchars(strip_tags($_POST['experience'] , ENT_QUOTES));
			$expertise = (array)$_POST['expertise'];
			$dob = htmlspecialchars(strip_tags($_POST['dob'] , ENT_QUOTES));
			$language = htmlspecialchars(strip_tags($_POST['language'] , ENT_QUOTES));
			$profile_pic = file_get_contents($_FILES['pic']['tmp_name']);

			if ($_FILES['pic']['tmp_name'])
				$profile_pic = file_get_contents($_FILES['pic']['tmp_name']);
			else
				$profile_pic = null;

			$username = $first_name.rand();
			$filename = $first_name.'_'.rand().'_'.rand().".jpg";

			$query1 = "select * from users where fb_id='".$fb_id."'";
			$already_exist = mysqli_query($conn, $query1);

			if (mysqli_num_rows($already_exist)) {
				echo "<script>window.location='dashboard.php?p=superusers'</script>";
			}
			else {
				if ($profile_pic) {
					require_once("../API/index.php");
					uploadToS3("photos/".$filename, $profile_pic);
				}

				$qrry_1="insert into users(fb_id,username,first_name,last_name,profile_pic,device,signup_type,bio,dob,gender,created)values(";
    			$qrry_1.="'".$fb_id."',";
    			$qrry_1.="'".$username."',";
    			$qrry_1.="'".$first_name."',";
    			$qrry_1.="'".$last_name."',";
    			$qrry_1.="'".$filename."',";
    			// $qrry_1.="'".$version."',";
    			$qrry_1.="'android',";
    			$qrry_1.="'OTP',";
    			$qrry_1.="'".$bio."',";
    			$qrry_1.="'".$dob."',";
    			$qrry_1.="'".$gender."',";
			$qrry_1.="'".date("Y-m-d H:i:s")."'";
   			$qrry_1.=")";

    			if (mysqli_query($conn, $qrry_1)) {
    				$last_insert_fb_id = mysqli_insert_id($conn);

    				$qr2 = "insert into super_user(user_id, language, experience, skills, expertise, created_at, updated_at) values(";
    				$qr2 .= "'".$last_insert_fb_id."',";
    				$qr2 .= "'".$language."',";
    				$qr2 .= "'".$experience."',";
    				$qr2 .= "'".$skills."',";
    				$qr2 .= "'".$expertise."',";
				$qr2 .= "'".date("Y-m-d H:i:s")."',";
				$qr2 .= "'".date("Y-m-d H:i:s")."'";
    				$qr2 .= ")";

    				if (mysqli_query($conn, $qr2)) {
    					$last_insert_id = mysqli_insert_id($conn);
    					foreach ($skills as $val) {
    						$q1 = "insert into superuser_skills(super_user_id, skill_id, created_at, updated_at) values(";
    						$q1 .= "'".$last_insert_id."',";
    						$q1 .= "'".(int)$val."',";
    						$q1 .= "'".date("Y-m-d H:i:s")."',";
    						$q1 .= "'".date("Y-m-d H:i:s")."'";
    						$q1 .= ")";
    						$res = mysqli_query($conn, $q1);
    					}


    					foreach ($expertise as $val) {
    						$q1 = "insert into superuser_expertise(super_user_id, expertise_id, created_at, updated_at) values(";
    						$q1 .= "'".$last_insert_id."',";
    						$q1 .= "'".(int)$val."',";
    						$q1 .= "'".date("Y-m-d H:i:s")."',";
    						$q1 .= "'".date("Y-m-d H:i:s")."'";
    						$q1 .= ")";
    						mysqli_query($conn, $q1);
    					}

    					echo "<script>window.location='dashboard.php?p=superusers'</script>";
    				}
    				else {
    					echo "<script>window.location='createsuperuser.php?action=error1'</script>";
    				}
    			}
    			else {
    				echo "<script>window.location='createsuperuser.php?action=error2'</script>";
    			}
			}
		}
		else {
			echo "<script>window.location='createsuperuser.php?action=error3'</script>";
		}
	}
}
?>

<div class="container dashboard">
	<h2 class="title" style="text-align: center;">Add Super User</h2>

	<div class="form">
		<form action="createsuperuser.php?action=submit" enctype="multipart/form-data" method="post" >
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label for="first_name"><b>First Name</b></label>
						<input id="first_name" class="form-control" name="first_name" required type="text" value="<?php echo @$user['first_name']; ?>">
					</div>
				</div>

				<div class="col-sm-4">
					<div class="form-group">
						<label for="last_name"><b>Last Name</b></label>
						<input id="last_name" class="form-control" name="last_name" required type="text" value="<?php echo @$user['last_name']; ?>">
					</div>
				</div>

				<div class="col-sm-4">
					<div class="form-group">
						<label for="phone"><b>Phone Number</b></label>
						<input id="phone" name="phone" class="form-control" required type="text" maxlength="10" value="<?php echo @$user['fb_id']; ?>">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label for="gender"><b>Gender</b></label>
						<select id="gender" name="gender" class="form-control" required type="text" value="<?php echo @$user['gender']; ?>" style="height: 51px;">
							<option value="">-- Choose --</option>
							<option value="Male">Male</option>
							<option value="Female">Female</option>
						</select>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="form-group">
						<label for="dob"><b>Date of Birth</b></label>
						<input id="dob" name="dob" class="form-control" required type="date" value="<?php echo @$user['dob']; ?>">
					</div>
				</div>

				<div class="col-sm-4">
					<div class="form-group">
						<label for="language" placeholder="Last Name"><b>Language</b></label>
						<select id="language" name="language" class="form-control" required type="text" style="height: 51px;">
							<option value="">-- Choose --</option>
							<option value="English">English</option>
							<option value="Hindi">Hindi</option>
						</select>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label for="category" placeholder="Category"><b>Category</b></label>
						<select id="category" name="category" class="form-control" required type="text" style="height: 51px;">
							<option value="1">Astrologers</option>
						</select>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="form-group">
						<label for="experience"><b>Experience (Yrs.)</b></label>
						<input id="experience" class="form-control" name="experience" type="number" min="0" required>
					</div>
				</div>
			</div>

			<hr>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="bio"><b>Bio</b></label>
						<textarea id="bio" class="form-control" name="bio" required></textarea>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="form-group">
						<label for="skills"><b>Skills</b></label>
						<select id="skills" name="skills[]" class="form-control" multiple required>
							<option value="" disabled><b>Choose Skill(s)</b></option>
							<?php 
							$skill_q = mysqli_query($conn, "select * from skills");
							while ($skill = mysqli_fetch_array($skill_q)) {
							echo '<option value="'.$skill['id'].'">'.$skill['skill'].'</option>';
							}
							?>
						</select>
					</div>
				</div>
			</div>

			<hr>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="expert"><b>Expertise</b></label>
						<select id="expert" name="expertise[]" class="form-control" multiple required>
							<option value="" disabled><b>Choose Field(s)</b></option>
							<?php 
							$q = mysqli_query($conn, "select * from expertise");
							while ($row = mysqli_fetch_array($q)) {
							echo '<option value="'.$row['id'].'">'.$row['field'].'</option>';
							}
							?>
						</select>
					</div>
				</div>
			</div>

			<br><hr>
			<div class="row">
				<div class="col-sm-12" align="center">
					<h2 style="text-align: center;">Profile Picture</h2>
					<label for="pic" style="cursor: pointer;" class="uploadbtn">
						<div class='preview' align="center" style="margin-top: 40px;">
							<img id="preview" src="uploadSound/upload.png" width="100" >
							<h2 style="color: #80808099; font-weight: 200; margin:10px; font-family: Arial;">Select Image</h2>
						</div>
						<div >
							<input type="file" id="pic" name="pic" onchange="return Upload_image(this)" style="display: none;" accept="image/png, image/jpeg, image/jpg" />
						</div>
					</label>
				</div>
			</div>

			<p ><input value="Submit" class="btn btn-sm btn-danger" type="submit"></p>

		</form>
		<div class="clear"></div>
	</div>
</div>

<script>
	function Upload_image(input) {
		if (input && input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#preview')
				.attr('src', e.target.result)
				.attr('class', 'col-sm-12');
			};

			reader.readAsDataURL(input.files[0]);
		}
	}
</script>

<?php require_once("footer.php"); ?>
