<?php 
require_once("config.php");
if( isset($_SESSION['id']))
{
	?>

	<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">
	<!--<script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script>-->
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#data1').DataTable();
		} );
	</script>

	<style>
		.button{
			border: 0px;
			background-color: #D4401D;
			color: white;
			padding: 5px 15px;
			margin-left: 10px;
		}

		a {
			text-decoration: none;
		}
	</style>

	
	<h2 class="title left">All Super Users</h2>
	<h2 class="right">
		<a class="button" href="createsuperuser.php"><i class="fa fa-plus" aria-hidden="true"></i> Create New</a> &nbsp;&nbsp;
		<a class="button" href="#"><i class="fa fa-arrows-h" aria-hidden="true"></i> Convert User</a>
	</h2>
	
	
	<?php

	$query = mysqli_query($conn,"select * from super_user order by id DESC");

	if(mysqli_num_rows($query) == 0){
			//echo "<div class='alert alert-danger'>Error in fetching order history, try again later..</div>";
		?>
		<div class="textcenter nothingelse">
			<img src="img/noorder.png" alt="" />
			<h3>No Record Found</h3>
		</div>
		<?php

	} else {
		echo "<table id='data1' class='display' style='width:100%''>
		<thead>
		<tr>
		<th>ID</th>
		<th>Name</th>
		<th>Username</th>
		<th>Category</th>
		<th>Skill(s)</th>
		<th>Expertise</th>
		<th>Created</th>
		</tr>
		</thead>
		<tbody id='myTable_row'>";

		while( $row = mysqli_fetch_array($query) ) {

			$query2 = mysqli_query($conn, "select * from users where id = '".$row['user_id']."'");

			if ($val = mysqli_fetch_array($query2)) {
				?>
				<tr style=" text-align: center;">
					<td>
						<?php 
						echo $val['fb_id']; 
						?>
					</td>
					<td style="line-height: 20px;">
						<?php 
						echo $val['first_name']." ".$val['last_name'];
						?>		
					</td>
					<td>
						<?php echo $val['username'];  ?>
					</td>
					<td>
						<?php 
						if ($row['category'] == 1)
							echo "Astrologers";
						?>
					</td>

					<td>
						<?php 
						$q1 = mysqli_query($conn, "select skill_id from superuser_skills where super_user_id = '".$row['id']."'");
						$row['skills'] = [];
						while ($r = mysqli_fetch_array($q1)) {
							$row['skills'][] = $r[0];
						}

						if ($row['skills']) {
							$q = mysqli_query($conn, "SELECT GROUP_CONCAT(`skill`) as skills from `skills` where `id` in (".implode(', ', $row['skills']).")");
							$skills = mysqli_fetch_assoc($q);
							echo $skills['skills'];
						}
						?>
					</td>

					<td>
						<?php 
						$q1 = mysqli_query($conn, "select expertise_id from superuser_expertise where super_user_id = '".$row['id']."'");
						$row['expertise'] = [];
						while ($r = mysqli_fetch_array($q1)) {
							$row['expertise'][] = $r[0];
						}

						if ($row['expertise']) {
							$q = mysqli_query($conn, "SELECT GROUP_CONCAT(`field`) as fields from `expertise` where `id` in (".implode(', ', $row['expertise']).")");
							$expertise = mysqli_fetch_assoc($q);
							echo $expertise['fields'];
						}
						?>
					</td>

					<td>
						<?php 
						echo $val['created']; 
						?>
					</td>
				</tr>
				<?php
			}
		}
		echo "</tbody>
		</table> <nav><ul class='pagination pagination-sm' id='myPager'></ul></nav>";
			///
	}


	?>

<?php } else {
	
	@header("Location: index.php");
	echo "<script>window.location='index.php'</script>";
	die;

} ?>
